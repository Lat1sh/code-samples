﻿using System.Collections.Generic;
using UnityEngine;

namespace CodeSamples
{
    /// <summary>
    /// Finds a path between a sheep's start position and escape object.
    /// </summary>
    public class Pathfinder
    {
        #region Properties

        /// <summary>
        /// Reference to the object that holds data about placement grid.
        /// </summary>
        public PlacementGrid ActiveGrid { get; set; }

        #endregion

        #region Fields

        /// <summary>
        /// Reference to the object that stores cells and their relations when A* pathfinding algorithm is performing its task.
        /// </summary>
        private PathfindingHeap openSet;

        /// <summary>
        /// Set of closed cells for the A* pathfinding algorithm.
        /// </summary>
        private HashSet<PlacementGridCell> closedSet;

        /// <summary>
        /// Result of <see cref="FindPath(PlacementGridCell, PlacementGridCell)"/> in the reverse order.
        /// </summary>
        private List<PlacementGridCell> retracedPath;

        #endregion

        #region Init

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Pathfinder()
        {            
            closedSet = new HashSet<PlacementGridCell>();
            retracedPath = new List<PlacementGridCell>();
        }

        /// <summary>
        /// Initializes the pathfinding heap.
        /// </summary>
        public void InitializeHeap()
        {
            openSet = new PathfindingHeap(ActiveGrid.GridWidth * ActiveGrid.GridHeight);
        }

        #endregion

        /// <summary>
        /// Finds a path between two cells.
        /// </summary>
        /// <param name="startCell">Start cell.</param>
        /// <param name="targetCell">Target cell.</param>
        /// <returns></returns>
        public PlacementGridCell[] FindPath(PlacementGridCell startCell, PlacementGridCell targetCell)
        {
            openSet.ResetHeap();
            closedSet.Clear();

            openSet.AddCell(startCell);

            while (openSet.CellCount > 0)
            {
                PlacementGridCell currentCell = openSet.RemoveFirstCellFromHeap();
                closedSet.Add(currentCell);

                if (currentCell == targetCell)
                {
                    return RetracePath(startCell, targetCell);
                }

                foreach (PlacementGridCell neighbor in currentCell.Neighbors)
                {
                    if (neighbor.IsOccupiedByEscapeBuilding || closedSet.Contains(neighbor))
                    {
                        continue;
                    }

                    int updatedMovementCost = currentCell.GCost + GetPathfindingDistance(currentCell, neighbor);

                    if (updatedMovementCost < neighbor.GCost || !openSet.Contains(neighbor))
                    {
                        neighbor.GCost = updatedMovementCost;
                        neighbor.HCost = GetPathfindingDistance(neighbor, targetCell);
                        neighbor.HeapParent = currentCell;

                        if (!openSet.Contains(neighbor))
                        {
                            openSet.AddCell(neighbor);
                        }
                        else
                        {
                            openSet.UpdateCell(neighbor);
                        }
                    }
                }
            }

            Debug.LogErrorFormat("Returning a null path from cell {0} {1} to cell {2} {3}", startCell.XCoordinate, startCell.YCoordinate,
                                 targetCell.XCoordinate, targetCell.YCoordinate);

            return null;
        }

        /// <summary>
        /// Gets pathfinding distance for the A* algorithm.
        /// </summary>
        /// <param name="firstCell"></param>
        /// <param name="secondCell"></param>
        /// <returns>
        /// An integer.
        /// </returns>
        private int GetPathfindingDistance(PlacementGridCell firstCell, PlacementGridCell secondCell)
        {
            int horizontalDistance = Mathf.Abs(firstCell.XCoordinate - secondCell.XCoordinate);
            int verticalDistance = Mathf.Abs(firstCell.YCoordinate - secondCell.YCoordinate);

            return 14 * Mathf.Min(horizontalDistance, verticalDistance) + 10 * Mathf.Abs(horizontalDistance - verticalDistance);
        }

        /// <summary>
        /// Retraces the path found by <see cref="FindPath(PlacementGridCell, PlacementGridCell)"/>.
        /// </summary>
        /// <param name="startCell">Start cell.</param>
        /// <param name="targetCell">Target cell.</param>
        /// <returns></returns>
        private PlacementGridCell[] RetracePath(PlacementGridCell startCell, PlacementGridCell targetCell)
        {
            retracedPath.Clear();

            PlacementGridCell currentCell = targetCell;

            while (currentCell != startCell)
            {
                retracedPath.Add(currentCell);
                currentCell = currentCell.HeapParent;
            }

            retracedPath.Reverse();

            return retracedPath.ToArray();
        }

    }
}