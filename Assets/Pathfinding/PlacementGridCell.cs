﻿using System.Collections.Generic;
using UnityEngine;

namespace CodeSamples
{
    /// <summary>
    /// Holds data about a single cell on the <see cref="PlacementGrid"/>.
    /// </summary>
    public class PlacementGridCell
    {
        #region Properties

        /// <summary>
        /// Is this cell currently occupied by an escape object like a rope or catapult?
        /// </summary>
        public bool IsOccupiedByEscapeObject { get; set; } = false;

        /// <summary>
        /// Is this cell currently occupied by an escape building?
        /// </summary>
        public bool IsOccupiedByEscapeBuilding { get; set; } = false;

        /// <summary>
        /// Can this cell be used for building placement?
        /// </summary>
        public bool CanBeUsedForBuildingPlacement { get; set; } = true;

        /// <summary>
        /// Cell's x coordinate.
        /// </summary>
        public int XCoordinate { get; }

        /// <summary>
        /// Cell's y coordinate.
        /// </summary>
        public int YCoordinate { get; }

        /// <summary>
        /// Cell's world position.
        /// </summary>
        public (float, float) WorldPosition { get; private set; }

        /// <summary>
        /// Cell's index in the <see cref="PathfindingHeap"/>.
        /// </summary>
        public int HeapIndex { get; set; }

        /// <summary>
        /// Cell's parent in the <see cref="PathfindingHeap"/>.
        /// </summary>
        public PlacementGridCell HeapParent { get; set; }

        /// <summary>
        /// Cell's GCost in for the A* pathfinding algorithm.
        /// </summary>
        public int GCost { get; set; }

        /// <summary>
        /// Cell's HCost in for the A* pathfinding algorithm.
        /// </summary>
        public int HCost { get; set; }

        /// <summary>
        /// Cell's FCost in for the A* pathfinding algorithm.
        /// </summary>
        public int FCost => GCost + HCost;

        /// <summary>
        /// References to cell's neighbors on the grid.
        /// </summary>
        public PlacementGridCell[] Neighbors { get; private set; }

        #endregion

        #region Constants

        /// <summary>
        /// Half of cell's size in world units.
        /// </summary>
        private const float CellHalfSize = 0.5f;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="x">Cell's x coordinate.</param>
        /// <param name="y">Cell's y coordinate.</param>
        public PlacementGridCell(int x, int y, Vector3 gridOriginPosition)
        {
            XCoordinate = x;
            YCoordinate = y;
            WorldPosition = (gridOriginPosition.x + XCoordinate - CellHalfSize, gridOriginPosition.z + YCoordinate - CellHalfSize);
        }

        /// <summary>
        /// Initializes cell's neighbor array,
        /// </summary>
        public void InitializeNeigborArray(PlacementGrid placementGrid) => Neighbors = GetNeighbors(placementGrid).ToArray();

        /// <summary>
        /// Updates cell's grid position based on passed grid origin.
        /// </summary>
        /// <param name="gridOriginPosition">New grid origin.</param>
        public void UpdateWorldPosition(Vector3 gridOriginPosition)
        {
            WorldPosition = (gridOriginPosition.x + XCoordinate - CellHalfSize, gridOriginPosition.z + YCoordinate - CellHalfSize);
        }

        #endregion

        #region Neighbors

        /// <summary>
        /// Gets a list of cell's neighbors.
        /// </summary>
        /// <param name="placementGrid">Reference to the placement grid.</param>
        /// <returns>
        /// A list of <see cref="PlacementGridCell"/>.
        /// </returns>
        private List<PlacementGridCell> GetNeighbors(PlacementGrid placementGrid)
        {
            var neighbors = new List<PlacementGridCell>();

            for (int xOffset = -1; xOffset <= 1; xOffset++)
            {
                for (int yOffset = -1; yOffset <= 1; yOffset++)
                {
                    if (xOffset == 0 && yOffset == 0)
                    {
                        continue;
                    }

                    int xCoordinate = XCoordinate + xOffset;
                    int yCoordinate = YCoordinate + yOffset;

                    if (placementGrid.ContainsCoordinates(xCoordinate, yCoordinate))
                    {
                        neighbors.Add(placementGrid.GetCellAtCoordinates(xCoordinate, yCoordinate));
                    }
                }
            }

            return neighbors;
        }

        #endregion
    }
}