﻿using UnityEngine;

namespace CodeSamples
{
    /// <summary>
    /// Initializes the pathfinding heap.
    /// </summary>
    public class PathfindingHeapInitializer : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Reference to the object that finds a path between a sheep's start position and escape object.
        /// </summary>
        private Pathfinder pathfinder;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by inhecting necessary dependencies.
        /// </summary>
        /// <param name="pathfinder">Object that finds a path between a sheep's start position and escape object.</param>
        [Zenject.Inject]
        public void Construct(Pathfinder pathfinder) => this.pathfinder = pathfinder;

        /// <summary>
        /// Calls the <see cref="Pathfinder.InitializeHeap"/> method on <see cref="pathfinder"/> after the placement grid is generated.
        /// </summary>
        public void InitializePathfindingHeap() => pathfinder.InitializeHeap();

        #endregion        
    }
}