﻿using CodeSamples.ScriptableObjects;
using System.Collections.Generic;
using UnityEngine;

namespace CodeSamples
{
    /// <summary>
    /// Holds data about a grid used to place various objects: level props, escape targets, etc.
    /// </summary>
    public class PlacementGrid
    {
        #region Properties

        /// <summary>
        /// Width of the placement grid.
        /// </summary>
        public int GridWidth { get; private set; }

        /// <summary>
        /// Height of the placement grid.
        /// </summary>
        public int GridHeight { get; private set; }

        public Vector3 GridOriginPosition { private get; set; } = Vector3.zero;

        #endregion

        #region Fields

        /// <summary>
        /// Cells that constitute the grid on current level.
        /// </summary>
        private PlacementGridCell[] cells;

        /// <summary>
        /// Cells that belong to the easy zone.
        /// </summary>
        private List<PlacementGridCell> easyZoneCells;

        /// <summary>
        /// Cells that belong to the medium zone.
        /// </summary>
        private List<PlacementGridCell> mediumZoneCells;

        /// <summary>
        /// Cells that belong to the hard zone.
        /// </summary>
        private List<PlacementGridCell> hardZoneCells;

        /// <summary>
        /// Unoccupied cells in easy zone remaining during current placement process.
        /// </summary>
        private List<PlacementGridCell> remainingEasyZoneCells;

        /// <summary>
        /// Unoccupied cells in medium zone remaining during current placement process.
        /// </summary>
        private List<PlacementGridCell> remainingMediumZoneCells;

        /// <summary>
        /// Unoccupied cells in hard zone remaining during current placement process.
        /// </summary>
        private List<PlacementGridCell> remainingHardZoneCells;

        /// <summary>
        /// Reference to the scriptable object that holds the coordinates of the last disabled escape object facade.
        /// </summary>
        /// <remarks>
        /// Require to deoccupy a placement grid 
        /// </remarks>
        private Vector2Variable disabledFacadeCoordinates;

        #endregion

        #region Constants

        /// <summary>
        /// Maximum number of attempts when trying to get cells for building placement.
        /// </summary>
        public int MaxAttemptsToGetBuildingCells = 10;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting necessary dependencies.
        /// </summary>
        /// <param name="disabledFacadeCoordinates">scriptable object that holds the coordinates 
        /// of the last disabled escape object facade.</param>
        [Zenject.Inject]
        public void Construct(Vector2Variable disabledFacadeCoordinates)
        {
            this.disabledFacadeCoordinates = disabledFacadeCoordinates;
        }

        /// <summary>
        /// Generates a new grid based on passed bounds and grid origin.
        /// </summary>
        /// <param name="mainGroundBounds">Main ground collider's bounds.</param>
        /// <param name="gridOriginPosition">Grid's origin in world space.</param>
        public void Generate(Bounds mainGroundBounds, Vector3 gridOriginPosition)
        {
            GridOriginPosition = gridOriginPosition;

            GridWidth = (int)mainGroundBounds.size.x;
            GridHeight = (int)mainGroundBounds.size.z;
            cells = new PlacementGridCell[GridWidth * GridHeight];

            for (int x = 0; x < GridWidth; x++)
            {
                for (int y = 0; y < GridHeight; y++)
                {
                    cells[x + y * GridWidth] = new PlacementGridCell(x + 1, y + 1, GridOriginPosition);
                }
            }

            foreach (var cell in cells)
            {
                cell.InitializeNeigborArray(this);
            }
        }

        /// <summary>
        /// Resets flags on grid cells when a level ends before new building placement.
        /// </summary>
        public void ResetBeforeNewBuildingPlacement()
        {
            
        }

        /// <summary>
        /// Marks cells beneath and around the prison yard as invalid for building placement or occupied.
        /// </summary>
        /// <param name="bottomLeftPrisonYardCorner">Bottom left corner of the prison yard.</param>
        /// <param name="topRightPrisonYardCorner">Top right corner of the prison yard.</param>
        /// <param name="bufferAreaSize">Size of the buffer area around the prison yard.</param>
        public void AddPrisonYardToGrid(Vector3 bottomLeftPrisonYardCorner, Vector3 topRightPrisonYardCorner, float bufferAreaSize)
        {
            PlacementGridCell bottomLeftPrisonYardCell = GetCellAtWorldPosition(bottomLeftPrisonYardCorner);
            PlacementGridCell topRightPrisonYardCell = GetCellAtWorldPosition(topRightPrisonYardCorner);           

            PlacementGridCell bottomLeftBufferAreaCell = GetCellAtWorldPosition(bottomLeftPrisonYardCorner + 
                                                                                Vector3.left * bufferAreaSize);

            PlacementGridCell topRightBufferAreaCell = GetCellAtWorldPosition(topRightPrisonYardCorner +
                                                                              Vector3.right * bufferAreaSize 
                                                                              + Vector3.forward * bufferAreaSize);

            /*Debug.LogFormat("Prison yard bottom left: {0} {1}. Top right: {2} {3}. Buffer bottom left: {4} {5}. Top right: {6} {7}",
                             bottomLeftPrisonYardCell.XCoordinate, bottomLeftPrisonYardCell.YCoordinate,
                             topRightPrisonYardCell.XCoordinate, topRightPrisonYardCell.YCoordinate,
                             bottomLeftBufferAreaCell.XCoordinate, bottomLeftBufferAreaCell.YCoordinate,
                             topRightBufferAreaCell.XCoordinate, topRightBufferAreaCell.YCoordinate);*/

            for (int x = bottomLeftBufferAreaCell.XCoordinate, maxX = topRightBufferAreaCell.XCoordinate; x <= maxX; x++)
            {
                for (int y = bottomLeftBufferAreaCell.YCoordinate, maxY = topRightBufferAreaCell.YCoordinate; y <= maxY; y++)
                {
                    if (x >= bottomLeftPrisonYardCell.XCoordinate && x <= topRightPrisonYardCell.XCoordinate 
                        && y <= topRightPrisonYardCell.YCoordinate)
                    {
                        cells[(x - 1) + (y - 1) * GridWidth].IsOccupiedByEscapeBuilding = true;
                    }
                    else
                    {
                        cells[(x - 1) + (y - 1) * GridWidth].CanBeUsedForBuildingPlacement = false;
                    }
                }
            }
        }

        /// <summary>
        /// Resets flags on all cells.
        /// </summary>
        public void ResetCells()
        {
            foreach (PlacementGridCell cell in cells)
            {
                cell.IsOccupiedByEscapeBuilding = false;
                cell.IsOccupiedByEscapeObject = false;
                cell.CanBeUsedForBuildingPlacement = true;
            }
        }

        #endregion

        #region Cells

        /// <summary>
        /// Returns True when the grid contains a cell with specified coordinates, False otherwise.
        /// </summary>
        /// <param name="xCoordinate">X-coordinate to check.</param>
        /// <param name="yCoordinate">Y-coordinate to check.</param>
        /// <returns>
        /// A boolean.
        /// </returns>
        public bool ContainsCoordinates(int xCoordinate, int yCoordinate)
        {
            return (xCoordinate > 0 && xCoordinate <= GridWidth) && (yCoordinate > 0 && yCoordinate <= GridHeight);
        }

        /// <summary>
        /// Updates world positions of grid cells based on passed grid origin.
        /// </summary>
        /// <param name="gridOriginPosition">New grid origin.</param>
        public void UpdateCellWorldPositions(Vector3 gridOriginPosition)
        {
            GridOriginPosition = gridOriginPosition;

            for (int x = 0; x < GridWidth; x++)
            {
                for (int y = 0; y < GridHeight; y++)
                {
                    cells[x + y * GridWidth].UpdateWorldPosition(GridOriginPosition);
                }
            }
        }

        /// <summary>
        /// Returns True if cell at specified coordinates is occupied by an escape object, False otherwise.
        /// </summary>
        /// <param name="xCoordinate">X coordinate.</param>
        /// <param name="yCoordinate">Y coordinate.</param>
        /// <returns>
        /// A boolean.
        /// </returns>
        public bool IsCellAtCoordinatesOccupiedByEscapeObject(int xCoordinate, int yCoordinate) => GetCellAtCoordinates(xCoordinate, yCoordinate).IsOccupiedByEscapeObject;

        /// <summary>
        /// Gets cell at specified coordinates.
        /// </summary>
        /// <param name="xCoordinate">X coordinate.</param>
        /// <param name="yCoordinate">Y coordinate.</param>
        /// <returns>
        /// A <see cref="PlacementGridCell"/>
        /// </returns>        
        public PlacementGridCell GetCellAtCoordinates(int xCoordinate, int yCoordinate)
        {
            try
            {
                return cells[(xCoordinate - 1) + ((yCoordinate - 1) * GridWidth)];
            }
            catch (System.IndexOutOfRangeException)
            {
                Debug.LogErrorFormat("Invalid index at coordinates {0} {1}", xCoordinate, yCoordinate);
                return null;
            }
        }

        /// <summary>
        /// Gets cell that corresponds with the passed world position.
        /// </summary>
        /// <param name="worldPosition">World position.</param>
        /// <returns>
        /// A <see cref="PlacementGridCell"/>.
        /// </returns>
        public PlacementGridCell GetCellAtWorldPosition(Vector3 worldPosition)
        {
            (int xIndex, int yIndex) = GetCellIndicesAtWorldPosition(worldPosition);
            return cells[xIndex + yIndex * GridWidth];
        }

        /// <summary>
        /// Occupies cell at specified coordinates with an escape object.
        /// </summary>
        /// <param name="xCoordinate">X coordinate.</param>
        /// <param name="yCoordinate">Y coordinate.</param>
        public void OccupyCellWithEscapeObject(int xCoordinate, int yCoordinate)
        {
            cells[(xCoordinate - 1) + ((yCoordinate - 1) * GridWidth)].IsOccupiedByEscapeObject = true;
        }

        /// <summary>
        /// Deoccupies cell at specified coordinates.
        /// </summary>
        /// <param name="xCoordinate">X coordinate.</param>
        /// <param name="yCoordinate">Y coordinate.</param>
        public void DeoccupyCell(int xCoordinate, int yCoordinate)
        {
            cells[xCoordinate + (yCoordinate * GridWidth)].IsOccupiedByEscapeObject = false;
        }

        /// <summary>
        /// Deoccupies cell at specified world position.
        /// </summary>
        /// <param name="position">Cell's world position.</param>
        public void DeoccupyCellAtWorldPosition(Vector3 position)
        {
            (int xCoordinate, int yCoordinate) = GetCellIndicesAtWorldPosition(position);
            DeoccupyCell(xCoordinate, yCoordinate);
        }

        /// <summary>
        /// Gets array indices of a cell at the specified world position.
        /// </summary>
        /// <param name="position">World position of the cell.</param>
        /// <returns>
        /// A tuple of type (int, int).
        /// </returns>
        private (int, int) GetCellIndicesAtWorldPosition(Vector3 position)
        {
            return ((int)Mathf.Floor(position.x - GridOriginPosition.x), (int)Mathf.Floor(position.z - GridOriginPosition.z));
        }

        /// <summary>
        /// Deoccupies a cell that corresponds to the world coordinates stored in <see cref="disabledFacadeCoordinates"/>.
        /// </summary>
        public void OnEscapeObjectFacadeDisabled()
        {
            Vector2 coordinates = disabledFacadeCoordinates.Value;
            DeoccupyCellAtWorldPosition(new Vector3(coordinates.x, 0f, coordinates.y));
        }

        /// <summary>
        /// Resets list of cells that are used for current placement process.
        /// </summary>
        public void ResetCellLists()
        {
            remainingEasyZoneCells = new List<PlacementGridCell>(easyZoneCells);
            remainingMediumZoneCells = new List<PlacementGridCell>(mediumZoneCells);
            remainingHardZoneCells = new List<PlacementGridCell>(hardZoneCells);
        }

        /// <summary>
        /// Gets a list of arrays that represent a cell range where a building will be placed.
        /// </summary>
        /// <param name="numberOfCellToGet">Number of cells to get. 
        /// Corresponds with the number of buildings that neds to be placed.</param>
        /// <param name="placementGridZoneDifficulty">Difficulty of the grid zone that the cells belong to.</param>
        /// <param name="placementGridZoningRules">Current zoning rules for the grid.</param>
        /// <returns></returns>
        public List<PlacementGridCell> GetCellsWithinZone(int numberOfCellToGet,
                                                          PlacementGridZoneDifficulty placementGridZoneDifficulty,
                                                          PlacementGridZoningRules placementGridZoningRules)
        {
            //Debug.LogFormat("Trying to get {0} cells for difficulty {1}", numberOfCellToGet, placementGridZoneDifficulty);

            var cellsToReturn = new List<PlacementGridCell>();

            for (int i = 1; i <= numberOfCellToGet; i++)
            {
                PlacementGridCell cellToPlaceBuilding = GetCellToPlaceBuilding(placementGridZoneDifficulty, ref cellsToReturn);

                if (cellToPlaceBuilding != null)
                {
                    cellsToReturn.Add(cellToPlaceBuilding);
                }
            }

            return cellsToReturn;
        }

        /// <summary>
        /// Gets a 2x2 range of cells inside a grid zone of specified difficulty.
        /// </summary>
        /// <param name="placementGridZoneDifficulty"></param>
        /// <param name="placementCells">Current list of cells that will be used for placement.</param>
        /// <returns>
        /// A <see cref="PlacementGridCell"/>.
        /// </returns>
        private PlacementGridCell GetCellToPlaceBuilding(PlacementGridZoneDifficulty placementGridZoneDifficulty,
                                                         ref List<PlacementGridCell> placementCells)
        {
            PlacementGridCell cellToReturn = null;

            for (int i = 1; i <= MaxAttemptsToGetBuildingCells; i++)
            {
                switch (placementGridZoneDifficulty)
                {
                    case PlacementGridZoneDifficulty.Easy:
                        cellToReturn = remainingEasyZoneCells[Random.Range(0, remainingEasyZoneCells.Count)];
                        break;
                    case PlacementGridZoneDifficulty.Medium:
                        cellToReturn = remainingMediumZoneCells[Random.Range(0, remainingMediumZoneCells.Count)];
                        break;
                    case PlacementGridZoneDifficulty.Hard:
                        cellToReturn = remainingHardZoneCells[Random.Range(0, remainingHardZoneCells.Count)];
                        break;
                }

                if (placementCells.Contains(cellToReturn))
                {
                    continue;
                }

                int cellX = cellToReturn.XCoordinate;
                int cellY = cellToReturn.YCoordinate;

                int minGridX = Mathf.Clamp(cellX - 2, 1, GridWidth);
                int maxGridX = Mathf.Clamp(cellX + 3, 1, GridWidth);

                int minGridY = Mathf.Clamp(cellY - 2, 1, GridHeight);
                int maxGridY = Mathf.Clamp(cellY + 3, 1, GridHeight);

                if (OneOfCellsWithinRangeIsOccupiedByEscapeBuilding(minGridX, maxGridX, minGridY, maxGridY))
                {
                    continue;
                }

                //Debug.LogFormat("Found valid placement cell: {0} {1}", cellToReturn.XCoordinate, cellToReturn.YCoordinate);

                OccupyAndRemoveCellsWithinRange(minGridX, maxGridX, minGridY, maxGridY, cellX, cellY);

                return cellToReturn;
            }

            return null;
        }

        /// <summary>
        /// Checks whether any of the cells within specified range are occupied.
        /// </summary>
        /// <param name="minGridX">Minimum x index to check.</param>
        /// <param name="maxGridX">Maximum x index to check.</param>
        /// <param name="minGridY">Minimum y index to check.</param>
        /// <param name="maxGridY">Maximum y index to check.</param>
        /// <returns>
        /// A boolean.
        /// </returns>
        private bool OneOfCellsWithinRangeIsOccupiedByEscapeBuilding(int minGridX, int maxGridX, int minGridY, int maxGridY)
        {
            for (int gridXIndex = minGridX; gridXIndex <= maxGridX; gridXIndex++)
            {
                for (int gridYIndex = minGridY; gridYIndex <= maxGridY; gridYIndex++)
                {
                    PlacementGridCell currentCell = cells[(gridXIndex - 1) + (gridYIndex - 1) * GridWidth];

                    if (currentCell.IsOccupiedByEscapeBuilding)
                    {
                        /*Debug.LogFormat("Cell {0} {1} is occupied", cells[gridXIndex + gridYIndex * GridWidth].XCoordinate,
                                                        cells[gridXIndex + gridYIndex * GridWidth].YCoordinate);*/
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Occupies all cells within specified range and removes them from active cell list.
        /// </summary>
        /// <param name="minGridX">Minimum x index to check.</param>
        /// <param name="maxGridX">Maximum x index to check.</param>
        /// <param name="minGridY">Minimum y index to check.</param>
        /// <param name="maxGridY">Maximum y index to check.</param>
        /// <param name="cellX">Cell's x coordinate.</param>
        /// <param name="cellY">Cell's y coordinate.</param>
        private void OccupyAndRemoveCellsWithinRange(int minGridX, int maxGridX, int minGridY, int maxGridY, int cellX, int cellY)
        {
            //Debug.LogFormat("Occupying and removing cells within range x: {0} {1} y: {2} {3}", minGridX, maxGridX, minGridY, maxGridY);
          
            int buildingMaxX = cellX + 1;
            int buildingMaxY = cellY + 1;

            for (int gridXIndex = minGridX; gridXIndex <= maxGridX; gridXIndex++)
            {
                for (int gridYIndex = minGridY; gridYIndex <= maxGridY; gridYIndex++)
                {
                    PlacementGridCell currentCell = cells[(gridXIndex - 1) + (gridYIndex - 1) * GridWidth];

                    int xCoordinate = currentCell.XCoordinate;
                    int yCoordinate = currentCell.YCoordinate;

                    if (xCoordinate >= cellX && xCoordinate <= buildingMaxX
                        && yCoordinate >= cellY && yCoordinate <= buildingMaxY)
                    {
                        currentCell.IsOccupiedByEscapeBuilding = true;
                    }
                    else
                    {
                        currentCell.CanBeUsedForBuildingPlacement = false;
                    }

                    if (remainingEasyZoneCells.Contains(currentCell))
                    {
                        remainingEasyZoneCells.Remove(currentCell);
                    }
                    else if (remainingMediumZoneCells.Contains(currentCell))
                    {
                        remainingMediumZoneCells.Remove(currentCell);
                    }
                    else if (remainingHardZoneCells.Contains(currentCell))
                    {
                        remainingHardZoneCells.Remove(currentCell);
                    }
                }
            }
        }

        /// <summary>
        /// Updates difficulty zones on the placement grid.
        /// </summary>
        /// <param name="placementGridZoningRules"></param>
        public void UpdateDifficultyZones(PlacementGridZoningRules placementGridZoningRules)
        {
            if (placementGridZoningRules == null || placementGridZoningRules.EasyZoneWidth == 0)
            {
                placementGridZoningRules = new PlacementGridZoningRules(11, 8, 4, 3, 3, 3);
            }

            easyZoneCells = new List<PlacementGridCell>();
            mediumZoneCells = new List<PlacementGridCell>();
            hardZoneCells = new List<PlacementGridCell>();

            int easyZoneWallOffset = 1 + (GridWidth - placementGridZoningRules.EasyZoneWidth) / 2;

            int easyZoneMinX = easyZoneWallOffset;
            int easyZoneMaxX = GridWidth - easyZoneWallOffset + 1;

            int easyZoneMaxY = placementGridZoningRules.EasyZoneHeight;

            int mediumZoneMinX = easyZoneMinX - placementGridZoningRules.MediumZoneWidth;
            int mediumZoneMaxX = easyZoneMaxX + placementGridZoningRules.MediumZoneWidth;

            int mediumZoneMaxY = easyZoneMaxY + placementGridZoningRules.MediumZoneHeight;

            int hardZoneMinX = mediumZoneMinX - placementGridZoningRules.HardZoneWidth;
            int hardZoneMaxX = mediumZoneMaxX + placementGridZoningRules.HardZoneWidth;

            int hardZoneMaxY = mediumZoneMaxY + placementGridZoningRules.HardZoneHeight;

            for (int x = 0; x < GridWidth; x++)
            {
                for (int y = 0; y < GridWidth; y++)
                {
                    if ((x + 1) >= easyZoneMinX && (x + 1) <= easyZoneMaxX && (y + 1) < easyZoneMaxY)
                    {
                        if (cells[x + y * GridWidth].CanBeUsedForBuildingPlacement)
                        {
                            easyZoneCells.Add(cells[x + y * GridWidth]);
                        }
                    }
                    else if ((x + 1) >= mediumZoneMinX && (x + 1) <= mediumZoneMaxX && (y + 1) < mediumZoneMaxY)
                    {
                        if (cells[x + y * GridWidth].CanBeUsedForBuildingPlacement)
                        {
                            mediumZoneCells.Add(cells[x + y * GridWidth]);
                        }
                    }
                    else if ((x + 1) >= hardZoneMinX && (x + 1) <= hardZoneMaxX && (y + 1) < hardZoneMaxY)
                    {
                        if (cells[x + y * GridWidth].CanBeUsedForBuildingPlacement)
                        {
                            hardZoneCells.Add(cells[x + y * GridWidth]);
                        }
                    }
                }
            }

            /*var buildingCellString = "Cell occupied by buildings: ";
            var unavailableCellString = "Cells not available for placement: ";

            foreach (PlacementGridCell cell in cells)
            {
                if (cell.IsOccupiedByEscapeBuilding)
                {
                    buildingCellString += string.Format("{0} {1}; ", cell.XCoordinate, cell.YCoordinate);
                }
                else if (!cell.CanBeUsedForBuildingPlacement)
                {
                    unavailableCellString += string.Format("{0} {1}; ", cell.XCoordinate, cell.YCoordinate);
                }
            }

            Debug.Log(buildingCellString);
            Debug.Log(unavailableCellString);*/
        }

        #endregion

    }
}