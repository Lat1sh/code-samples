﻿using UnityEngine;

namespace CodeSamples
{
    /// <summary>
    /// Stores cells and their relations when A* pathfinding algorithm is performing its task.
    /// </summary>
    public class PathfindingHeap
    {
        /// <summary>
        /// Current number of cells in the pathfinding heap.
        /// </summary>
        public int CellCount { get; private set; } = 0;

        #region Fields

        /// <summary>
        /// Cells in the heap.
        /// </summary>
        private PlacementGridCell[] cells;

        /// <summary>
        /// Maximum size of the pathfinding heap.
        /// </summary>
        private int maxHeapSize;

        private int swapIndex;
        private int parentIndex;

        #endregion

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="maxHeapSize">Maximum heap size.</param>
        public PathfindingHeap(int maxHeapSize)
        {
            cells = new PlacementGridCell[maxHeapSize];
            this.maxHeapSize = maxHeapSize;
        }

        #region Heap management

        /// <summary>
        /// Resets the pathfinding heap by creating a new array.
        /// </summary>
        public void ResetHeap()
        {
            cells = new PlacementGridCell[maxHeapSize];
            CellCount = 0;
        }

        /// <summary>
        /// Adds a cell to the heap.
        /// </summary>
        /// <param name="cell">Cell to add.</param>
        public void AddCell(PlacementGridCell cell)
        {
            try
            {
                cell.HeapIndex = CellCount;
                cells[CellCount] = cell;
                SortUp(cell);
                CellCount++;
            }
            catch (System.IndexOutOfRangeException)
            {
                Debug.LogErrorFormat("Invalid index: {0}. Cells array length: {1}", CellCount, cells.Length);
            }
        }

        /// <summary>
        /// Updates cell's index in the heap.
        /// </summary>
        /// <param name="cell">Cell to update.</param>
        public void UpdateCell(PlacementGridCell cell) => SortUp(cell);

        /// <summary>
        /// Removes first cell from the heap and returns it.
        /// </summary>
        /// <returns>
        /// A <see cref="PlacementGridCell"/>.
        /// </returns>
        public PlacementGridCell RemoveFirstCellFromHeap()
        {
            var firstCell = cells[0];
            CellCount--;
            cells[0] = cells[CellCount];
            cells[0].HeapIndex = 0;
            SortDown(cells[0]);
            return firstCell;
        }

        /// <summary>
        /// Sorts the cell up the heap.
        /// </summary>
        /// <param name="cell">Cell to sort.</param>
        private void SortUp(PlacementGridCell cell)
        {
            SetParentIndex(cell);

            while (true)
            {
                var parentCell = cells[parentIndex];

                if (CompareCellCosts(cell, parentCell) > 0)
                {
                    SwapCells(cell, parentCell);
                }
                else
                {
                    break;
                }

                SetParentIndex(cell);
            }
        }

        /// <summary>
        /// Set's cell's parent index in the heap.
        /// </summary>
        /// <param name="cell">Target cell.</param>
        private void SetParentIndex(PlacementGridCell cell) => parentIndex = (cell.HeapIndex - 1) / 2;

        /// <summary>
        /// Sorts a cell down the heap.
        /// </summary>
        /// <param name="cell">Cell to sort.</param>
        private void SortDown(PlacementGridCell cell)
        {
            while (true)
            {
                int childIndexLeft = cell.HeapIndex * 2 + 1;
                int childIndexRight = cell.HeapIndex * 2 + 2;
                swapIndex = 0;

                if (childIndexLeft < CellCount)
                {
                    swapIndex = childIndexLeft;

                    if (childIndexRight < CellCount)
                    {
                        if (CompareCellCosts(cells[childIndexLeft], cells[childIndexRight]) < 0)
                        {
                            swapIndex = childIndexRight;
                        }
                    }

                    if (CompareCellCosts(cell, cells[swapIndex]) < 0)
                    {
                        SwapCells(cell, cells[swapIndex]);
                    }
                    else
                    {
                        return;
                    }

                }
                else
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Spaps twi cells in the heap.
        /// </summary>
        /// <param name="firstCell">First cell.</param>
        /// <param name="secondCell">Second cell.</param>
        private void SwapCells(PlacementGridCell firstCell, PlacementGridCell secondCell)
        {
            cells[firstCell.HeapIndex] = secondCell;
            cells[secondCell.HeapIndex] = firstCell;

            int tempIndex = firstCell.HeapIndex;
            firstCell.HeapIndex = secondCell.HeapIndex;
            secondCell.HeapIndex = tempIndex;
        }

        /// <summary>
        /// Returns True when the heap contains the passed cell.
        /// </summary>
        /// <param name="cell">Cell to check.</param>
        /// <returns>
        /// A boolean.
        /// </returns>
        public bool Contains(PlacementGridCell cell)
        {
            try
            {
                return Equals(cells[cell.HeapIndex], cell);
            }
            catch (System.IndexOutOfRangeException)
            {
                Debug.Log("Invalid heap index: " + cell.HeapIndex + " for cell with X: " + cell.XCoordinate + " and Y: " + cell.YCoordinate);
                return false;
            }
        }

        /// <summary>
        /// Compares A* costs of two cells and returns the result.
        /// </summary>
        /// <param name="firstCell">First cell.</param>
        /// <param name="secondCell">Second cell.</param>
        /// <returns>
        /// An integer.
        /// </returns>
        private int CompareCellCosts(PlacementGridCell firstCell, PlacementGridCell secondCell)
        {
            int compareResult = firstCell.FCost.CompareTo(secondCell.FCost);

            if (compareResult == 0)
            {
                compareResult = firstCell.HCost.CompareTo(secondCell.HCost);
            }               

            return -compareResult;
        }

        #endregion
    }
}