﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Handles showing/hiding and changing the color of an object's outline.
    /// </summary>
    public class OutlineController
    {

#if UNITY_EDITOR

        #region Properties

        public bool RenderersAreValid
        {
            get
            {
                if (renderers == null)
                {
                    return false;
                }

                foreach (SpriteRenderer renderer in renderers)
                {
                    if (renderer == null)
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        #endregion

#endif

        #region Fields

        /// <summary>
        /// Reference to the sprite renderers that this controller draws the outline around.
        /// </summary>
        private readonly SpriteRenderer[] renderers;

        /// <summary>
        /// A delegate that shows outline depending on how we want
        ///  to draw it: by using the shader or a separate sprite.
        /// </summary>
        /// <param name="targetColor"></param>
        private delegate void ShowOutlineDelegate(Color targetColor);
        private ShowOutlineDelegate showOutlineDelegate;

        /// <summary>
        /// A delegate that hides the outline depending on whether
        /// it was drawn using a shader or a separate sprite.
        /// </summary>
        private delegate void HideOutlineDelegate();
        private HideOutlineDelegate hideOutlineDelegate;

        private static Color32 gravityHighlightColor = new Color32(58, 57, 227, 255);
        private static Color32 spectralHighlightColor = new Color32(39, 171, 100, 255);
        private static Color32 polymorphHighlightColor = new Color32(195, 20, 41, 255);
        private static Color32 immunityHighlightColor = new Color32(207, 189, 28, 255);

        private static MaterialPropertyBlock propertyBlock;

        #endregion

        #region Constructor
      
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="renderers">Game object's sprite renderers.</param>
        /// <param name="usePropertyBlock">Should a property block be used for drawing outline or a separate sprite?</param>
        public OutlineController(SpriteRenderer[] renderers, bool usePropertyBlock)
        {
            this.renderers = renderers;

            if (usePropertyBlock)
            {
                showOutlineDelegate += ShowOutlineShader;
                hideOutlineDelegate += HideOutlineShader;
            }
            else
            {
                showOutlineDelegate += ShowOutlineSprite;
                hideOutlineDelegate += HideOutlineSprite;
            }

            if (propertyBlock == null)
            {
                propertyBlock = new MaterialPropertyBlock();
            }            
        }

        #endregion

        #region Outline

        /// <summary>
        /// Shows outline of the color that corresponds with the specified outline type.
        /// </summary>
        /// <param name="type">Outline type.</param>
        public void ShowOutline(SpellType type)
        {
            Color targetColor = new Color();

            switch (type)
            {
                case SpellType.Gravity:
                    targetColor = gravityHighlightColor;
                    break;
                case SpellType.Spectral:
                    targetColor = spectralHighlightColor;
                    break;
                case SpellType.Polymorph:
                    targetColor = polymorphHighlightColor;
                    break;
                case SpellType.Immunity:
                    targetColor = immunityHighlightColor;
                    break;
            }

            showOutlineDelegate(targetColor);
        }

        /// <summary>
        /// Hides outline.
        /// </summary>
        public void HideOutline() => hideOutlineDelegate();

        /// <summary>
        /// Shows outline using the target renderer's shader.
        /// </summary>
        private void ShowOutlineShader(Color targetColor)
        {
            foreach (SpriteRenderer renderer in renderers)
            {
                renderer.GetPropertyBlock(propertyBlock);
                propertyBlock.SetColor("_OutlineColor", targetColor);
                propertyBlock.SetInt("_DrawOutline", 1);
                renderer.SetPropertyBlock(propertyBlock);
            }            
        }

        /// <summary>
        /// Shows outline by enabling a separate sprite that represents the outline.
        /// </summary>
        private void ShowOutlineSprite(Color targetColor)
        {
            renderers[0].color = targetColor;
            renderers[0].enabled = true;
        }

        /// <summary>
        /// Disables the outline on the renderer's shader.
        /// </summary>
        private void HideOutlineShader()
        {
            foreach (SpriteRenderer renderer in renderers)
            {
                renderer.GetPropertyBlock(propertyBlock);
                propertyBlock.SetInt("_DrawOutline", 0);
                renderer.SetPropertyBlock(propertyBlock);
            }            
        }

        /// <summary>
        /// Hides the sprite that represents the outline.
        /// </summary>
        private void HideOutlineSprite() => renderers[0].enabled = false;

        #endregion
    }
}