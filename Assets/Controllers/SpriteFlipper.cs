﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Flips a sprite around the x or y axis.
    /// </summary>
    public class SpriteFlipper
    {
        /// <summary>
        /// Flips character's sprite horizontally based on where character is facing.
        /// </summary>
        /// <param name="spriteRenderer">Sprite renderer to flip.</param>
        /// <param name="facingRight">Is character currently facing right?</param>
        public void FlipX(SpriteRenderer spriteRenderer, bool facingRight)
        {
            spriteRenderer.flipX = !facingRight;
        }
    }
}