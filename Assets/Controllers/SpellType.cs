﻿namespace MetraReborn
{
    /// <summary>
    /// Lists base spell types.
    /// </summary>
    public enum SpellType
    {
        Gravity,
        Spectral,
        Polymorph,
        Immunity
    }
}