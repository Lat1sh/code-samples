﻿namespace MetraReborn
{
    /// <summary>
    /// Holds current gravity direction for objects that can move in any of the four directions.
    /// </summary>
    public enum GravityDirection 
    {
        Down,
        Up,        
        Left,
        Right
    }
}