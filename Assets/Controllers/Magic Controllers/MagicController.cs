﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Applies magic effects to an object.
    /// </summary>
    [ExecuteAlways]
    public class MagicController : MonoBehaviour
    {
        #region Properties

        /// <summary>
        /// Connected object's current gravity direction.
        /// </summary>
        /// <value>
        /// Gets and sets the value of the enum field gravityDirection.
        /// </value>
        public GravityDirection GravityDirection { get => gravityDirection; set => gravityDirection = value; }

        /// <summary>
        /// Is the object this controller belongs to in spectral state?
        /// </summary>
        /// <value>
        /// Gets and sets the value of the bool field isSpectral.
        /// </value>
        public bool IsSpectral { get => isSpectral; private set => isSpectral = value; }

        /// <summary>
        /// Is the object this controller belongs to immune to magic?
        /// </summary>
        /// <value>
        /// Gets tand sets the value of the bool field isImmuneToMagic.
        /// </value>
        public bool IsImmuneToMagic { get => isImmuneToMagic; private set => isImmuneToMagic = value; }

        #endregion

        #region Fields

        [HideInInspector]
        [SerializeField]
        private GravityDirection gravityDirection;

        [HideInInspector]
        [SerializeField]
        private bool isSpectral;

        [HideInInspector]
        [SerializeField]
        private bool isImmuneToMagic;

        /// <summary>
        /// Component that handles gravity magic.
        /// </summary>
        private GravityMagicController gravityController;

        /// <summary>
        /// Component that handles polymorph magic.
        /// </summary>
        private PolymorphMagicController polymorphController;

        /// <summary>
        /// Component that handles spectral magic.
        /// </summary>
        private SpectralMagicController spectralController;

        /// <summary>
        /// Component that handles immunity magic.
        /// </summary>
        private ImmunityMagicController immunityController;

        /// <summary>
        /// Object's main sprite renderer.
        /// </summary>
        [Inject(Id = "MainSpriteRenderer")]
        private SpriteRenderer spriteRenderer;

#if UNITY_EDITOR
        [SerializeField]
        private bool usePropertyBlockForOutlineController = true;
#endif

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting the dependencies.
        /// </summary>
        /// <param name="gravityController">Gravity controller.</param>
        /// <param name="polymorphController">Polymorph controller.</param>
        /// <param name="spectralController">Spectral controller.</param>
        /// <param name="immunityController">Immunity controller.</param>
        [Inject]
        public void Construct(GravityMagicController gravityController, SpectralMagicController spectralController,
                             PolymorphMagicController polymorphController, ImmunityMagicController immunityController)
        {
            this.gravityController = gravityController;
            this.polymorphController = polymorphController;
            this.spectralController = spectralController;
            this.immunityController = immunityController;
        }

        /// <summary>
        /// Initializes magic controller's state when an object is polymorphed.
        /// </summary>
        /// <param name="magicControllerState">Magic controller state.</param>
        public void InitState(MagicControllerState magicControllerState)
        {
            GravityDirection = magicControllerState.GravityDirection;

            switch (GravityDirection)
            {
                case GravityDirection.Down:
                    transform.rotation = Quaternion.Euler(Vector3.zero);
                    break;
                case GravityDirection.Up:
                    transform.rotation = Quaternion.Euler(0f, 0f, 180f);
                    break;
                case GravityDirection.Left:
                    transform.rotation = Quaternion.Euler(0f, 0f, 90f);
                    break;
                case GravityDirection.Right:
                    transform.rotation = Quaternion.Euler(0f, 0f, -90f);
                    break;
                default:
                    break;
            }

            IsSpectral = magicControllerState.IsSpectral;
            spectralController.UseSpectralMagic(isSpectral);

            IsImmuneToMagic = magicControllerState.IsImmuneToMagic;
            immunityController.UseImmunityMagic(isImmuneToMagic);
        }

        /// <summary>
        /// Gets magic controller's current state.
        /// </summary>
        /// <returns>
        /// A <see cref="MagicControllerState"/>.
        /// </returns>
        public MagicControllerState GetState()
        {
            return new MagicControllerState(gravityDirection, isSpectral, isImmuneToMagic);
        }

        private void Start()
        {
            if (IsSpectral)
            {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    spectralController = new SpectralMagicController(GetComponentsInChildren<SpriteRenderer>());
                }
#endif
                spectralController.UseSpectralMagic(IsSpectral);
            }

            if (isImmuneToMagic)
            {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    SpriteRenderer[] spriteRenderers = usePropertyBlockForOutlineController ?
                                               GetComponentsInChildren<SpriteRenderer>() :
                                               new SpriteRenderer[1] { transform.Find("OutlineRenderer").GetComponent<SpriteRenderer>() };

                    immunityController = new ImmunityMagicController(new OutlineController(spriteRenderers, usePropertyBlockForOutlineController));
                }
#endif
                immunityController.UseImmunityMagic(IsImmuneToMagic);
            }
        }

        #endregion

        #region Magic

        /// <summary>
        /// Tells the <see cref="gravityController"/> to use gravity magic 
        /// if the object is not immune to magic. If it is, removes the immunity and returns.
        /// </summary>
        /// <param name="gravityMagicType">Type of gravity magic to use.</param>
        public void UseGravityMagic()
        {
            if (RemovedImmunityFromAnImmuneObject())
            {
                return;
            }

#if UNITY_EDITOR
            
            if (!Application.isPlaying)
            {
                gravityController = new GravityMagicController(new TransformRotator(), new SpriteFlipper(),
                                                               new GravityDirectionCalculator());

                gravityController.InitializeFields(transform, transform.Find("GravityPivot"));

                spriteRenderer = GetComponent<SpriteRenderer>();

                Undo.RecordObject(this, string.Format("Cast gravity magic on {0}", gameObject.name));
                EditorUtility.SetDirty(this);
            }
#endif
            gravityDirection = gravityController.UseGravityMagic(gravityDirection, spriteRenderer);
        }

        /// <summary>
        /// Tells the <see cref="spectralController"/> to use spectral magic 
        /// if the object is not immune to magic. If it is, removes the immunity and returns.
        /// </summary>
        public void UseSpectralMagic()
        {
            if (RemovedImmunityFromAnImmuneObject())
            {
                return;
            }

#if UNITY_EDITOR

            if (!Application.isPlaying)
            {
                spectralController = new SpectralMagicController(GetComponentsInChildren<SpriteRenderer>());
                Undo.RecordObject(this, string.Format("Cast spectral magic on {0}", gameObject.name));
                EditorUtility.SetDirty(this);
            }
#endif
            isSpectral = !isSpectral;
            spectralController.UseSpectralMagic(isSpectral);
        }

        /// <summary>
        /// Tells the <see cref="polymorphController"/> to use polymorph magic
        /// if the object is not immune to magic. If its is, removes the immunity and returns.
        /// </summary>
        public void UsePolymorphMagic()
        {
            if (RemovedImmunityFromAnImmuneObject())
            {
                return;
            }

#if UNITY_EDITOR  
            if (!Application.isPlaying)
            {
                // TO-DO: Need to set up the polymorph process in the editor with pools and scriptable objects

                Undo.RecordObject(this, string.Format("Cast polymorph magic on {0}", gameObject.name));
                EditorUtility.SetDirty(this);
            }
#endif
            polymorphController.UsePolymorphMagic();
        }

        /// <summary>
        /// Tells the <see cref="immunityController"/> to use immunity magic.
        /// </summary>
        public void UseImmunityMagic()
        {
#if UNITY_EDITOR 
            if (!Application.isPlaying)
            {
                SpriteRenderer[] spriteRenderers = usePropertyBlockForOutlineController ?
                                               GetComponentsInChildren<SpriteRenderer>() :
                                               new SpriteRenderer[1] { transform.Find("OutlineRenderer").GetComponent<SpriteRenderer>() };

                immunityController = new ImmunityMagicController(new OutlineController(spriteRenderers, usePropertyBlockForOutlineController));

                Undo.RecordObject(this, string.Format("Cast immunity magic on {0}", gameObject.name));
                EditorUtility.SetDirty(this);
            }
#endif
            isImmuneToMagic = !isImmuneToMagic;
            immunityController.UseImmunityMagic(isImmuneToMagic);
        }

        /// <summary>
        /// Returns True when an object that a magic is applied to is immune to magic and has its immunity removed, False otherwise.
        /// </summary>
        /// <returns>
        /// A boolean.
        /// </returns>
        private bool RemovedImmunityFromAnImmuneObject()
        {
            if (IsImmuneToMagic)
            {
                UseImmunityMagic();
                return true;
            }

            return false;
        }

        #endregion
    }
}