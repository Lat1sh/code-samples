﻿namespace MetraReborn
{
    /// <summary>
    /// Interface for object that can be polymorphed.
    /// </summary>
    public interface IPolymorphableObject
    {
        PolymorphableObjectType PolymorphableObjectType { get; }

        void InitPolymorphableState(PolymorphableState polymorphableState);

        PolymorphableState GetPolymorphableState();
    }
}