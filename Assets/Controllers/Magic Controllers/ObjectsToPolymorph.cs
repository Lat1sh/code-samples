﻿using System.Collections.ObjectModel;
using UnityEngine;

namespace MetraReborn.ScriptableObjects
{
    /// <summary>
    /// Scriptable object that holds a batch of object that need to be polymorphed.
    /// </summary>
    [CreateAssetMenu(fileName = "ObjectsToPolymorph", menuName = "Scriptable Objects/Magic/ObjectsToPolymorph", order = 0)]
    public class ObjectsToPolymorph : ScriptableObject
    {
        #region Properties

        /// <summary>
        /// Objects to polymotph.
        /// </summary>
        public Collection<IPolymorphableObject> Value
        {
            get => value;

            set
            {
                this.value = value;                
                onObjectsToPolymorphReceived.Raise();
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Event that is raised when a new batch of object to polymorph is received.
        /// </summary>
        [Tooltip("Event that is raised when a new batch of object to polymorph is received.")]
        [SerializeField]
        private Events.GameEvent onObjectsToPolymorphReceived;

        private Collection<IPolymorphableObject> value;

        #endregion
    }
}