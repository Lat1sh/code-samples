﻿namespace MetraReborn
{
    /// <summary>
    /// Contains transferable data to be used when the character changes between alternative forms.
    /// </summary>
    public class CharacterPolymorphableState : PolymorphableState
    {
        #region Properties

        /// <summary>
        /// Was character facing right when they got polymorphed?
        /// </summary>
        public bool WasFacingRight { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="magicControllerState">Character's magic controller state on polymorph.</param>
        public CharacterPolymorphableState(bool wasFacingRight, 
                                           MagicControllerState magicControllerState) : base(magicControllerState)
        {
            WasFacingRight = wasFacingRight;
        }

        #endregion
    }
}