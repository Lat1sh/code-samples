﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Toggles an object between spectral and material states.
    /// </summary>
    public class SpectralMagicController
    {
        #region Fields

        /// <summary>
        /// Reference to object's sprite renderers.
        /// </summary>
        private SpriteRenderer[] spriteRenderers;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="spriteRenderers">Object's sprite renderers.</param>
        public SpectralMagicController(SpriteRenderer[] spriteRenderers) => this.spriteRenderers = spriteRenderers;

        #endregion

        #region Magic

        /// <summary>
        /// Applies spectral magic to this object.
        /// </summary>
        /// <param name="isSpectral">Is this object in spectral state?</param>
        public void UseSpectralMagic(bool isSpectral)
        {
            var propertyBlock = new MaterialPropertyBlock();

            foreach (SpriteRenderer spriteRenderer in spriteRenderers)
            {
                if (spriteRenderer.CompareTag("NotAffectedBySpectralMagic"))
                {
                    continue;
                }

                spriteRenderer.GetPropertyBlock(propertyBlock);
                propertyBlock.SetFloat("_DefaultState", isSpectral ? 0f : 1f);
                spriteRenderer.SetPropertyBlock(propertyBlock);
            }
        }

#endregion
    }
}