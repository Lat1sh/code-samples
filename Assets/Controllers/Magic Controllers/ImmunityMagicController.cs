﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Toggles object's immunity to magic.
    /// </summary>
    public class ImmunityMagicController
    {
        #region Fields

        /// <summary>
        /// Component that draws object's outline.
        /// </summary>
        private OutlineController outlineController;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="outlineController"></param>       
        public ImmunityMagicController(OutlineController outlineController) => this.outlineController = outlineController;

        #endregion

        #region Magic effect

        /// <summary>
        /// Toggles object's immunity to magic.
        /// </summary>
        /// <param name="isImmuneToMagic">Is character currently immune to magic?</param>
        public void UseImmunityMagic(bool isImmuneToMagic)
        {
            if (isImmuneToMagic)
            {
                outlineController.ShowOutline(SpellType.Immunity);
            }
            else
            {
                outlineController.HideOutline();
            }
        }

        #endregion
    }
}