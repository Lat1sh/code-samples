﻿namespace MetraReborn
{
    /// <summary>
    /// Holds current state of a magic controller: gravity direction, spectral state, immunity, etc.
    /// Used to pass data when objects are polymorphed.
    /// </summary>
    public class MagicControllerState
    {
        #region Properties

        /// <summary>
        /// Polymorphed object's current gravity direction.
        /// </summary>
        public GravityDirection GravityDirection { get; }

        /// <summary>
        /// Is polymorphed object currently in spectral state?
        /// </summary>
        public bool IsSpectral { get; }

        /// <summary>
        /// Is polymorphed object currently immune to magic?
        /// </summary>
        public bool IsImmuneToMagic { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="gravityDirection">Polymorphed object's current gravity direction.</param>
        /// <param name="isSpectral">Is polymorphed object currently in spectral state?</param>
        /// <param name="isImmuneToMagic">Is polymorphed object currently immune to magic?</param>
        public MagicControllerState(GravityDirection gravityDirection, bool isSpectral, bool isImmuneToMagic)
        {
            GravityDirection = gravityDirection;
            IsSpectral = isSpectral;
            IsImmuneToMagic = isImmuneToMagic;
        }

        #endregion
    }
}