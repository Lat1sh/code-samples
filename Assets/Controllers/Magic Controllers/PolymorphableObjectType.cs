﻿namespace MetraReborn
{
    /// <summary>
    /// Type of object that can be polymorphed.
    /// </summary>
    public enum PolymorphableObjectType
    {
        Player,
        Enemy,
        Interactable,
        Trap
    }
}