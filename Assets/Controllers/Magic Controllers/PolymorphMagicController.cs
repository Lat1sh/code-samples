﻿using MetraReborn.ScriptableObjects;

namespace MetraReborn
{
    /// <summary>
    /// Toggles an object between alternative forms when polymorph magic is used.
    /// </summary>
    public class PolymorphMagicController
    {
        #region Fields

        /// <summary>
        /// Polymorphable component on parent object.
        /// </summary>
        private IPolymorphableObject polymorphableObject;

        /// <summary>
        /// Scriptable object that holds objects that need to be polymorphed.
        /// </summary>
        private ObjectsToPolymorph objectsToPolymorph;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="polymorphableObject">Polymorphable component on parent object.</param>
        /// <param name="objectsToPolymorph">Scriptable object that holds objects that need to be polymorphed.</param>
        public PolymorphMagicController(IPolymorphableObject polymorphableObject, ObjectsToPolymorph objectsToPolymorph)
        {
            this.polymorphableObject = polymorphableObject;
            this.objectsToPolymorph = objectsToPolymorph;
        }

        #endregion

        #region Toggle

        /// <summary>
        /// Sets the value of <see cref="ObjectsToPolymorph.Value"/> property on <see cref="objectsToPolymorph"/> 
        /// to <see cref="polymorphableObject"/>.
        /// </summary>
        public void UsePolymorphMagic()
        {
            objectsToPolymorph.Value = new System.Collections.ObjectModel.Collection<IPolymorphableObject> { polymorphableObject };
        }
        
        #endregion
    }
}