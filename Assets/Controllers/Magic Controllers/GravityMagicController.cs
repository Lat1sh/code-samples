﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Gravity controller for non-ground objects.
    /// </summary>
    public class GravityMagicController
    {
        #region Fields 

        /// <summary>
        /// Object's root transform.
        /// </summary>
        [Zenject.Inject(Id = "RootTransform")]
        private Transform rootTransform;

        /// <summary>
        /// Transform to use a pivt for gravity magic.
        /// </summary>
        [Zenject.Inject(Id = "GravityPivot")]
        private Transform gravityPivot;

        /// <summary>
        /// Object that rotates a transform.
        /// </summary>
        private TransformRotator transformRotator;

        /// <summary>
        /// Object that flips a sprite around x or y axis.
        /// </summary>
        private SpriteFlipper spriteFlipper;

        /// <summary>
        /// Object that calculates new gravity direction based on type of gravity magic that was used.
        /// </summary>
        private GravityDirectionCalculator gravityDirectionCalculator;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="transformRotator">Object that rotates a transform.</param>
        /// <param name="spriteFlipper">Object that flips a sprite.</param>
        /// <param name="gravityDirectionCalculator">bject that calculates new gravity direction based on 
        /// type of gravity magic that was used.</param>
        public GravityMagicController(TransformRotator transformRotator, SpriteFlipper spriteFlipper, 
                                      GravityDirectionCalculator gravityDirectionCalculator)
        {
            this.transformRotator = transformRotator;
            this.spriteFlipper = spriteFlipper;
            this.gravityDirectionCalculator = gravityDirectionCalculator;
        }

        #endregion

        #region Magic effect

        /// <summary>
        /// Casts gravity magic on this object.
        /// </summary>
        /// <param name="currentDirection">Current gravity direction.</param>
        /// <param name="spriteRenderer">Object's main sprite renderer.</param>.
        /// <returns>
        /// The new <see cref="GravityDirection"/>.
        /// </returns>
        public GravityDirection UseGravityMagic(GravityDirection currentDirection, SpriteRenderer spriteRenderer)
        {
            transformRotator.Rotate(rootTransform, rotateClockwise: true, gravityPivot.position);
            transformRotator.Rotate(rootTransform, rotateClockwise: true, gravityPivot.position);
            spriteFlipper.FlipX(spriteRenderer, spriteRenderer.flipX);

            GravityDirection newDirection = gravityDirectionCalculator.GetNewDirection(currentDirection);

            return newDirection;
        }

        #endregion

#if UNITY_EDITOR

        #region Editor

        /// <summary>
        /// Initializes fields. Used instead of Zenject injection in the edtior.
        /// </summary>
        /// <param name="rootTransform">Object's root transform.</param>
        /// <param name="gravityPivot">Transform to use a pivt for gravity magic.</param>
        public void InitializeFields(Transform rootTransform, Transform gravityPivot)
        {
            this.rootTransform = rootTransform;
            this.gravityPivot = gravityPivot;
        }

        #endregion

#endif
    }
}