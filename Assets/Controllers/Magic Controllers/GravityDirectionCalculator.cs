﻿namespace MetraReborn
{
    /// <summary>
    /// Calculates <see cref="GravityDirection"/> based on current gravity direction of an object and a <see cref="GravityMagicType"/>.
    /// </summary>

    public class GravityDirectionCalculator
    {
        /// <summary>
        /// Gets new gravity direction based on current one and type of gravity magic that was used.
        /// </summary>
        /// <param name="currentDirection">Current gravity direction.</param>
        /// <param name="gravityMagicType">Type of gravity magic that was used.</param>
        /// <returns></returns>
        public GravityDirection GetNewDirection(GravityDirection currentDirection)
        {
            GravityDirection newDirection = GravityDirection.Down;

            switch (currentDirection)
            {                
                case GravityDirection.Down:
                    newDirection = GravityDirection.Up;
                    break;
                case GravityDirection.Up:
                    newDirection = GravityDirection.Down;
                    break;
                case GravityDirection.Left:
                    newDirection = GravityDirection.Right;
                    break;
                case GravityDirection.Right:
                    newDirection = GravityDirection.Left;
                    break;                
            }

            return newDirection;
        }
    }
}