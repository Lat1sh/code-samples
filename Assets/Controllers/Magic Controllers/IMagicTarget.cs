﻿namespace MetraReborn
{
    /// <summary>
    /// Base interface for all objects that can be targeted by magic: characters, platforms, traps, projectiles, etc.
    /// </summary>
    public interface IMagicTarget : IHighlightable
    {
        string Tag { get; }
        MagicController MagicController { get; }
        void UseGravityMagic();
        void UseSpectralMagic();
        void UsePolymorphMagic();
        void UseImmunityMagic();
    }
}