﻿namespace MetraReborn
{
    /// <summary>
    /// Contains transferable data to be used when an object is polymorphed into its counterpart.
    /// </summary>
    public class PolymorphableState
    {
        #region Properties

        /// <summary>
        /// Object's magic controller state.
        /// </summary>
        public MagicControllerState MagicControllerState { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="magicControllerState">Object's magic controller state.</param>
        public PolymorphableState(MagicControllerState magicControllerState)
        {
            MagicControllerState = magicControllerState;
        }

        #endregion
    }
}