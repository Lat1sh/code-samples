﻿using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Inverts gravity value for main character when gravity magic is used.
    /// </summary>
    public class GravityInverter : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that serves as a facade for main character,
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Scriptable object that holds current gravity value.
        /// </summary>
        [Inject(Id = "Gravity")]
        private FloatVariable gravity;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="characterFacade"></param>
        [Inject]
        public void Construct(CharacterFacade characterFacade) => this.characterFacade = characterFacade;

        #endregion

        #region Invert

        /// <summary>
        /// Inverts gravity value when gravity magic is used.
        /// </summary>
        public void InvertGravity()
        {
            GravityDirection currentGravityDirection = characterFacade.GravityDirection;

            if (currentGravityDirection == GravityDirection.Up || currentGravityDirection == GravityDirection.Right)
            {
                if (gravity.Value < 0f)
                {
                    gravity.Value *= -1f;
                }
            }
            else
            {
                if (gravity.Value > 0f)
                {
                    gravity.Value *= -1f;
                }
            }
        }

        #endregion
    }
}