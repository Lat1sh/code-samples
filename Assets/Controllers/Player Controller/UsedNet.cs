﻿using MetraReborn.Events;
using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Scriptable object that holds the net the character is grabbing.
    /// </summary>
    [CreateAssetMenu(fileName = "UsedNet", menuName = "Scriptable Objects/Character/Used Net", order = 6)]
    public class UsedNet : ScriptableObject
    {
        #region Properties

        /// <summary>
        /// Net that the character is grabbing.
        /// </summary>
        /// <value>
        /// Gets and sets the value of the field netFacade. Raises the <see cref="onResetJumpState"/> when setting.
        /// </value>
        public NetFacade NetFacade
        {
            get => netFacade;

            set
            {
                netFacade = value;

                if (netFacade != null)
                {
                    onCharacterGrabbedNet.Raise();
                }
                else
                {
                    onCharacterReleasedNet.Raise();
                }
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Event that is raised when character grabs a net.
        /// </summary>
        [Tooltip("Event that is raised when character grabs a net.")]
        [SerializeField]
        private GameEvent onCharacterGrabbedNet;

        /// <summary>
        /// Event that is raised when character releases a net.
        /// </summary>
        [Tooltip("Event that is raised when character releases a net.")]
        [SerializeField]
        private GameEvent onCharacterReleasedNet;

        [ReadOnly]
        [SerializeField]
        private NetFacade netFacade = null;

        #endregion
    }
}