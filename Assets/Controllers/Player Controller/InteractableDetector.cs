﻿using MetraReborn.ScriptableObjects;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Detects interactable objects that the player is overlapping with.
    /// </summary>
    public class InteractableDetector : MonoBehaviour, IPausable
    {
        #region Properties

        /// <summary>
        /// Is this component currently paused?
        /// </summary>
        public bool IsPaused { get; private set; } = false;

        #endregion

        #region Fields

        /// <summary>
        /// Scriptable object that holds the interactable item character is currently colliding with.
        /// </summary>
        private CurrentInteractable currentInteractable;

        /// <summary>
        /// Collisions mask for interactable objects.
        /// </summary>
        [Inject(Id = "InteractableCollisionMask")]
        private LayerMask interactableCollisionMask;

        /// <summary>
        /// Contact filter to use when checking for overlapping interactable objects.
        /// </summary>
        private ContactFilter2D interactableContactFilter;

        /// <summary>
        /// Reference to character's main collider.
        /// </summary>
        [Inject(Id = "MainCollider")]
        private BoxCollider2D mainCollider;

        /// <summary>
        /// Component that handles spells cast on this object.
        /// </summary>
        private MagicController magicController;

        private List<Collider2D> hitColliders = new List<Collider2D>();

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="currentInteractable">Scriptable object that holds the interactable item 
        /// character is currently colliding with.</param>
        /// <param name="magicController">Component that handles spells cast on this object.</param>
        [Inject]
        public void Construct(CurrentInteractable currentInteractable, MagicController magicController)
        {            
            this.currentInteractable = currentInteractable;
            this.magicController = magicController;

            interactableContactFilter = new ContactFilter2D();
            interactableContactFilter.SetLayerMask(interactableCollisionMask);
        }

        #endregion

        #region Update

        private void Update()
        {
            if (IsPaused)
            {
                return;
            }

            InteractableFacade currentInteractable = InteractableCheck();

            if (this.currentInteractable.Current != currentInteractable)
            {
                this.currentInteractable.Current = currentInteractable;
            }
        }

        #endregion

        #region Check

        /// <summary>
        /// Checks whether character is ovelapping with an interactable object.
        /// </summary>
        /// <returns>
        /// An <see cref="InteractableFacade"/>.
        /// </returns>
        private InteractableFacade InteractableCheck()
        {
            int numberOfCollidingInteractables = mainCollider.OverlapCollider(interactableContactFilter, hitColliders);

            if (numberOfCollidingInteractables > 0)
            {
                var interactable = hitColliders[0].GetComponent<IMagicTarget>();

                if (interactable.Tag == "DashCrystal")
                {
                    return (InteractableFacade)interactable;
                }

                return magicController.IsSpectral == interactable.MagicController.IsSpectral ? (InteractableFacade)interactable : null;
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region  Pause

        /// <summary>
        /// Sets the value of <see cref="IsPaused"/> to True.
        /// </summary>
        public void Pause() => IsPaused = true;

        /// <summary>
        /// Sets the value of <see cref="IsPaused"/> to False.
        /// </summary>
        public void Unpause() => IsPaused = false;

        #endregion
    }
}