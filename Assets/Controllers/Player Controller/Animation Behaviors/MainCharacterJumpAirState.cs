﻿using UnityEngine;

namespace MetraReborn
{
    public class MainCharacterJumpAirState : StateMachineBehaviour
    {
        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (animator.GetComponent<CharacterFacade>().IsGrounded)
            {
                animator.SetTrigger("StartIdling");
                animator.GetComponent<AnimationController>().EnterDefaultState();
            }                
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetBool("IsJumping", false);
        }
    }
}