﻿using UnityEngine;

namespace MetraReborn
{
    public class MainCharacterTeleportState : StateMachineBehaviour
    {
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetBool("IsTeleporting", true);
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetBool("IsTeleporting", false);
        }
    }
}