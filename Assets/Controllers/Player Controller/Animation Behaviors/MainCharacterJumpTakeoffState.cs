﻿using UnityEngine;

namespace MetraReborn
{
    public class MainCharacterJumpTakeoffState : StateMachineBehaviour
    {
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetBool("IsJumping", true);
        }
    }
}