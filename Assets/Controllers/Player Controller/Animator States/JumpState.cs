﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Animator state used when character is jumping.
    /// </summary>
    public class JumpState : CharacterAnimatorState
    {
        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="animator">Animator component.</param>
        public JumpState(Animator animator) : base(animator)
        {
        }

        #endregion

        /// <summary>
        /// Called when animator enters this state.
        /// </summary>
        public override void EnterState() => animator.SetTrigger("StartJumping");
    }
}