﻿using MetraReborn.ScriptableObjects;
using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Animator state used when character is climbing a net.
    /// </summary>
    public class ClimbNetState : CharacterAnimatorState
    {
        #region Fields

        /// <summary>
        /// Reference to the <see cref="FloatVariable"/> that holds current horizontal input.
        /// </summary>      
        private FloatVariable horizontalInput;
        /// <summary>
        /// Reference to the <see cref="FloatVariable"/> that holds current vertical input.
        /// </summary>
        private FloatVariable verticalInput;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="animator">Animator component.</param>
        public ClimbNetState(Animator animator, FloatVariable horizontalInput, FloatVariable verticalInput) : base(animator)
        {
            this.horizontalInput = horizontalInput;
            this.verticalInput = verticalInput;
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when animator enters this state.
        /// </summary>
        public override void EnterState()
        {
            animator.SetBool("IsJumping", false);
            animator.SetTrigger("StartClimbing");
        }

        /// <summary>
        /// State update. Executed every frame as long as the character is in this state.
        /// </summary>
        public override void Update()
        {
            animator.speed = (horizontalInput.Value != 0 || verticalInput.Value != 0) ? 1f : 0f;
        }

        #endregion
    }
}