﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Animator state used when character is using a dash pad.
    /// </summary>
    public class DashPadState : CharacterAnimatorState
    {
        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="animator">Animator component.</param>
        public DashPadState(Animator animator) : base(animator) {}

        #endregion

        #region Update

        /// <summary>
        /// Called when animator enters this state.
        /// </summary>
        public override void EnterState()
        {
            animator.SetBool("IsJumping", false);
            if (!animator.GetBool("IsDashing"))
            {
                animator.SetTrigger("StartDashing");
            }
        }

        /// <summary>
        /// State update. Executed every frame as long as the character is in this state.
        /// </summary>
        public override void Update() {}

        #endregion
    }
}