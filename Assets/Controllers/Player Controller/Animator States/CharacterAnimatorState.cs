﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Base class for character animator states.
    /// </summary>
    public abstract class CharacterAnimatorState
    {
        /// <summary>
        /// Reference to character's animator component.
        /// </summary>
        protected Animator animator;

        /// <summary>
        /// Base constructor.
        /// </summary>
        /// <param name="animator">Animator component.</param>
        public CharacterAnimatorState(Animator animator)
        {
            this.animator = animator;
        }

        public abstract void EnterState();
        public virtual void Update() { }
    }
}