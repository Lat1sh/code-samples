﻿using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Animator state used during default character movement.
    /// </summary>
    public class DefaultMovementState : CharacterAnimatorState
    {
        #region Fields

        /// <summary>
        /// Scriptable object that serves as a facade for playable character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        private FloatVariable horizontalInput;

        /// <summary>
        /// Scriptable object that holds current vertical input.
        /// </summary>
        private FloatVariable verticalInput;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary>
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Scriptable object that holds a bollean with character's current grounded status.
        /// </summary>
        private BoolVariable isGrounded;

        /// <summary>
        /// Scriptable object that holds reference to current ground object.
        /// </summary>
        private CurrentGround currentGround;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="animator">Animator component.</param>
        /// <param name="characterFacade">Scriptable object that serves as a facade for playable character.</param>
        /// <param name="currentVelocity">Reference to the the scriptable object that holds character's current velocity.</param>
        /// <param name="isGrounded">Scriptable object that holds grounde flag.</param>
        /// <param name="currentGround">Scriptable object that holds reference to current ground object.</param>
        public DefaultMovementState(Animator animator, CharacterFacade characterFacade, Vector2Variable currentVelocity, 
                                    BoolVariable isGrounded, CurrentGround currentGround,
                                    FloatVariable horizontalInput, FloatVariable verticalInput) : base(animator)
        {
            this.characterFacade = characterFacade;
            this.currentVelocity = currentVelocity;
            this.isGrounded = isGrounded;
            this.currentGround = currentGround;
            this.horizontalInput = horizontalInput;
            this.verticalInput = verticalInput;
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when animator enters this state.
        /// </summary>
        public override void EnterState() => Update();

        /// <summary>
        /// State update. Executed every frame as long as the character is in this state.
        /// </summary>
        public override void Update()
        {
            if (isGrounded.Value)
            {
                if (currentGround.Current.Tag == "ConveyorGround")
                {
                    float inputValue = 0f;

                    switch (characterFacade.GravityDirection)
                    {
                        case GravityDirection.Down:
                        case GravityDirection.Up:
                            inputValue = horizontalInput.Value;
                            break;
                        case GravityDirection.Left:
                        case GravityDirection.Right:
                            inputValue = verticalInput.Value;
                            break;
                    }

                    if (inputValue != 0f && !animator.GetBool("IsRunning"))
                    {
                        animator.SetTrigger("StartRunning");
                    }
                    else if (inputValue == 0f && !animator.GetBool("IsIdling"))
                    {
                        animator.SetTrigger("StartIdling");
                    }
                }
                else
                {
                    float scaledHorizontalSpeed = Mathf.Abs(currentVelocity.Value.x * Time.deltaTime);

                    if (scaledHorizontalSpeed > 0.005f && !animator.GetBool("IsRunning"))
                    {
                        animator.SetTrigger("StartRunning");
                    }
                    else if (scaledHorizontalSpeed < 0.005f && !animator.GetBool("IsIdling"))
                    {
                        animator.SetTrigger("StartIdling");
                    }
                }
            }
            else
            {
                float scaledVerticalSpeed = currentVelocity.Value.y * Time.deltaTime;

                if (scaledVerticalSpeed > 0f && !animator.GetBool("IsJumping"))
                {
                    animator.SetTrigger("StartJumping");
                }
                else if (scaledVerticalSpeed < 0f && !animator.GetBool("IsFalling"))
                {
                    animator.SetTrigger("StartFalling");
                }
            }
        }

        #endregion
    }
}