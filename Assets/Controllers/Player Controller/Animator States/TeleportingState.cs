﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Animator state used when character is teleporting.
    /// </summary>
    public class TeleportingState : CharacterAnimatorState
    {
        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="animator">Animator component.</param>
        public TeleportingState(Animator animator) : base(animator) {}
        
        #endregion

        #region Update

        /// <summary>
        /// Called when animator enters this state.
        /// </summary>
        public override void EnterState()
        {
            animator.SetBool("IsJumping", false);
            if (!animator.GetBool("IsTeleporting"))
            {
                animator.SetTrigger("StartTeleporting");
            }            
        }

        #endregion
    }
}