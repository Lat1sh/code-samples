﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Animator state used when character is inside an acceleration zone.
    /// </summary>
    public class AccelerationZoneState : CharacterAnimatorState
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="animator">Animator component.</param>
        public AccelerationZoneState(Animator animator) : base(animator) {}

        /// <summary>
        /// Called when animator enters this state.
        /// </summary>
        public override void EnterState()
        {
            animator.SetBool("IsJumping", false);
            animator.SetTrigger("StartFloating");
        }
    }
}