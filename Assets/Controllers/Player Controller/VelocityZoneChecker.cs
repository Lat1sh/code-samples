﻿using MetraReborn.Events;
using MetraReborn.ScriptableObjects;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Checks whether player is colliding with any velocity zones and adjusts velocity accordingly.
    /// </summary>
    public class VelocityZoneChecker : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary>
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Scriptable object that holds the is grounded flag.
        /// </summary>        
        [Inject(Id = "IsGrounded")]
        private BoolVariable isGrounded;

        /// <summary>
        /// Scriptable object that holds current gravity value.
        /// </summary>
        [Inject(Id = "Gravity")]
        private FloatVariable gravity;

        /// <summary>
        /// Scriptable object holds the isInsideFanZone flag.
        /// </summary>
        [Inject(Id = "IsInsideFanZone")]
        private BoolVariable isInsideFanZone;

        /// <summary>
        /// Event that is raised when jump state needs to be reset.
        /// </summary>
        [Inject(Id = "OnResetJumpState")]
        private GameEvent onResetJumpState;

        /// <summary>
        /// Reference to character's main collider.
        /// </summary>
        [Inject(Id = "MainCollider")]
        private BoxCollider2D mainCollider;

        /// <summary>
        /// Component that serves as a facade for the character object.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Contact filter to use when checking for overlapping velocity zones.
        /// </summary>
        private ContactFilter2D velocityZoneContactFilter;        

        /// <summary>
        /// Was player inside a fan zone on previous frame?
        /// </summary>
        private bool wasInsideFanZoneOnPreviousFrame = false;

        private List<Collider2D> hitColliders = new List<Collider2D>();

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="gravityDirection">Scriptable object that holds character's current gravity direction.</param>
        /// <param name="characterFacade">Component that serves as a facade for the character object.</param>
        [Inject]
        public void Construct(Vector2Variable currentVelocity, CharacterFacade characterFacade)
        {
            this.currentVelocity = currentVelocity;
            this.characterFacade = characterFacade;
        }

        private void Start()
        {
            velocityZoneContactFilter = new ContactFilter2D();
            velocityZoneContactFilter.SetLayerMask(1 << LayerMask.NameToLayer("VelocityZones"));
        }

        #endregion

        #region Update

        /// <summary>
        /// Checks whether player is colliding with any velocity zones and 
        /// sets up respective fields. Throws an event to reset jump state when first entering such a zone.
        /// </summary>
        public void Update()
        {
            if (CharacterMoveController.Active.Type != CharacterMoveControllerType.Default)
            {
                return;
            }

            isInsideFanZone.Value = false;

            Vector2 currentVelocity = this.currentVelocity.Value;

            VelocityZone[] velocityZones = GetVelocityZones();

            if (velocityZones != null)
            {
                GravityDirection gravityDirection = this.characterFacade.GravityDirection;

                foreach (var zone in velocityZones)
                {
                    switch (zone.VelocityZoneType)
                    {
                        case VelocityZoneType.Wind:
                        case VelocityZoneType.Fan:
                            if ((zone.IsHorizontal && isGrounded.Value &&
                                (gravityDirection == GravityDirection.Up || gravityDirection == GravityDirection.Down))
                                ||
                                (!zone.IsHorizontal && isGrounded.Value &&
                                (gravityDirection == GravityDirection.Right || gravityDirection == GravityDirection.Left)))
                            {
                                if (zone.VelocityZoneType == VelocityZoneType.Fan)
                                {
                                    if (!wasInsideFanZoneOnPreviousFrame)
                                    {
                                        wasInsideFanZoneOnPreviousFrame = true;
                                        onResetJumpState.Raise();
                                    }

                                    isInsideFanZone.Value = true;
                                    currentVelocity += (zone.VelocityToAdd / characterFacade.CurrentGround.FrictionModifier)
                                                       * zone.GetFanVelocityFactor(currentVelocity, Mathf.Abs(gravity.Value * Time.deltaTime));
                                }
                                else
                                {
                                    currentVelocity += zone.VelocityToAdd / characterFacade.CurrentGround.FrictionModifier;
                                }
                            }
                            else
                            {
                                if (zone.VelocityZoneType == VelocityZoneType.Fan)
                                {
                                    if (!wasInsideFanZoneOnPreviousFrame)
                                    {
                                        wasInsideFanZoneOnPreviousFrame = true;
                                        onResetJumpState.Raise();
                                    }

                                    isInsideFanZone.Value = true;
                                    currentVelocity += zone.VelocityToAdd * zone.GetFanVelocityFactor(currentVelocity, Mathf.Abs(gravity.Value));
                                }
                                else
                                {
                                    currentVelocity += zone.VelocityToAdd;
                                }
                            }
                            break;
                        case VelocityZoneType.ConveyorBelt:
                            if (isGrounded.Value)
                            {         
                                if (characterFacade.CurrentGround.Tag == "ConveyorGround")
                                {                                    
                                    currentVelocity += zone.VelocityToAdd;
                                }
                            }
                            break;
                    }
                }
            }
            else
            {
                wasInsideFanZoneOnPreviousFrame = false;
            }

            this.currentVelocity.Value = currentVelocity;
        }

        /// <summary>
        /// Gets an array of velocity zones that the character is currently overlapping with.
        /// </summary>
        /// <returns></returns>
        private VelocityZone[] GetVelocityZones()
        {
            int numberOfCollidingVelocityZones = mainCollider.OverlapCollider(velocityZoneContactFilter, hitColliders);

            if (numberOfCollidingVelocityZones > 0)
            {
                var velocityZones = new VelocityZone[numberOfCollidingVelocityZones];

                for (int i = 0; i < numberOfCollidingVelocityZones; i++)
                {
                    velocityZones[i] = hitColliders[i].GetComponent<IVelocityZone>().VelocityZone;
                }

                return velocityZones;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}