﻿using UnityEngine;

namespace MetraReborn.ScriptableObjects
{
    /// <summary>
    /// A scriptable object that holds reference to the ground object that the player is currently standing on, if any.
    /// </summary>
    [CreateAssetMenu(fileName = "CurrentGround", menuName = "Scriptable Objects/Character/Current Ground", order = 8)]
    public class CurrentGround : ScriptableObject
    {
        #region Properties

        /// <summary>
        /// Reference to the current ground object.
        /// </summary>
        /// <value>
        /// Gets and sets an <see cref="IGround"/> object.
        /// </value>
        public IGround Current { get; set; } = null;

        #endregion
    }
}