﻿using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Controls character movement while they are climbing walls in their alternative forms.
    /// </summary>
    public class GroundClimbController : CharacterMoveController
    {
        #region Fields        

        /// <summary>
        /// Component that serves as a facade for main character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        [Inject(Id = "HorizontalInput")]
        private FloatVariable horizontalInput;

        /// <summary>
        /// Scriptable object that holds current vertical input.
        /// </summary>
        [Inject(Id = "VerticalInput")]
        private FloatVariable verticalInput;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary>
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Scriptable object that holds data about the ground object that the character is attached to.
        /// </summary>
        private AttachedGround attachedGroundSO;

        /// <summary>
        /// Component that flips an object.
        /// </summary>
        private FlipController flipController;

        /// <summary>
        /// Ground object character is attached to.
        /// </summary>
        private IGround attachedGround;

        /// <summary>
        /// Direction relative to the player where the ground that they are attached to is.
        /// </summary>
        private AttachedGroundDirection attachedGroundDirection;

        /// <summary>
        /// Transform of the top left raycast origin.
        /// </summary>
        [Inject(Id = "TopLeftRaycastOrigin")]
        private Transform topLeftRaycastOrigin;

        /// <summary>
        /// Transform of the top right raycast origin.
        /// </summary>
        [Inject(Id = "TopRightRaycastOrigin")]
        private Transform topRightRaycastOrigin;

        /// <summary>
        /// Transform of the bottom left raycast origin.
        /// </summary>
        [Inject(Id = "BottomLeftRaycastOrigin")]
        private Transform bottomLeftRaycastOrigin;

        /// <summary>
        /// Transform of the bottom right raycast origin.
        /// </summary>
        [Inject(Id = "BottomRightRaycastOrigin")]
        private Transform bottomRightRaycastOrigin;

        /// <summary>
        /// Collisions mask for ground.
        /// </summary>
        [Inject(Id = "GroundCollisionMask")]
        private LayerMask groundCollisionMask;

        /// <summary>
        /// Speed when character is climbing.
        /// </summary>
        [Inject(Id = "GroundClimbSpeed")]
        private float groundClimbSpeed = 3f;

        /// <summary>
        /// Acceleration time when character is climbing.
        /// </summary>
        [Inject(Id = "AccelerationTimeClimbing")]
        private float accelerationTimeClimbing = 0.15f;

        private float horizontalVelocitySmoothing;

        #endregion

        #region Init

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="characterTransform">Root transform of the character object.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="characterFacade">Component that serves as a facade for main character.</param>
        /// <param name="attachedGroundSO">Scriptable object that holds data about the ground object 
        /// that the character is attached to.</param>
        /// <param name="flipController">Component that flips an object.</param>
        public GroundClimbController(Transform characterTransform, Vector2Variable currentVelocity,
                                     CharacterFacade characterFacade, AttachedGround attachedGroundSO,
                                     FlipController flipController) : base(characterTransform)
        {
            this.currentVelocity = currentVelocity;
            this.characterFacade = characterFacade;
            this.attachedGroundSO = attachedGroundSO;
            this.flipController = flipController;
        }

        /// <summary>
        /// Sets behavior's type.
        /// </summary>
        protected override void SetType() => Type = CharacterMoveControllerType.Climb;

        public override void Enable()
        {
            var attachedGroundData = attachedGroundSO.Data;

            attachedGround = attachedGroundData.Ground;
            attachedGroundDirection = attachedGroundData.AttachedGroundDirection;
        }

        #endregion

        #region Climbing walls

        /// <summary>
        /// Updates character's velocity and handles collisions while climbing walls.
        /// </summary>
        public override void PerformUpdate()
        {
            if (!GroundWasReleased())
            {
                // TO-DO: Need to check for collisions here
                characterTransform.position += (Vector3)attachedGround.CurrentVelocity;
            }
            else
            {
                attachedGroundSO.Data = null;
                flipController.FlipObject();
                return;
            }

            UpdateVelocityWhileClimbingGround();

            Vector3 scaledVelocity = currentVelocity.Value * Time.deltaTime;

            characterTransform.Translate(scaledVelocity);
        }

        /// <summary>
        /// Checks whether the ground character is attached to has been released this frame.
        /// </summary>
        /// <returns>
        /// A boolean.
        /// </returns>
        private bool GroundWasReleased()
        {
            switch (characterFacade.GravityDirection)
            {
                case GravityDirection.Down:
                case GravityDirection.Up:
                    if ((attachedGroundDirection == AttachedGroundDirection.Right && horizontalInput.Value < 0f) ||
                        (attachedGroundDirection == AttachedGroundDirection.Left && horizontalInput.Value > 0f))
                    {
                        return true;
                    }
                    break;
                case GravityDirection.Left:
                case GravityDirection.Right:
                    if ((attachedGroundDirection == AttachedGroundDirection.Right && verticalInput.Value < 0f) ||
                        (attachedGroundDirection == AttachedGroundDirection.Left && verticalInput.Value > 0f))
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }

        /// <summary>
        /// Updates vertical velocity when character is climbing ground objects.
        /// </summary>
        private void UpdateVelocityWhileClimbingGround()
        {
            Vector2 currentVelocity = this.currentVelocity.Value;
            float verticalTargetVelocity;

            GravityDirection gravityDirection = characterFacade.GravityDirection;
            RaycastHit2D hit;

            switch (gravityDirection)
            {
                case GravityDirection.Down:
                    verticalTargetVelocity = groundClimbSpeed * verticalInput.Value;
                    currentVelocity.y = Mathf.SmoothDamp(currentVelocity.y, verticalTargetVelocity, ref horizontalVelocitySmoothing,
                                                         accelerationTimeClimbing * attachedGround.FrictionModifier);

                    
                    if (currentVelocity.y > 0f)
                    {                        
                        if (attachedGroundDirection == AttachedGroundDirection.Left)
                        {
                            hit = Physics2D.Raycast(topLeftRaycastOrigin.position, Vector2.left, 0.1f, groundCollisionMask);
                        }
                        else
                        {
                            hit = Physics2D.Raycast(topRightRaycastOrigin.position, Vector2.right, 0.1f, groundCollisionMask);
                        }

                        if (!hit)
                        {
                            currentVelocity.y = 0f;
                        }                        
                    }
                    else if (currentVelocity.y < 0f)
                    {                        
                        if (attachedGroundDirection == AttachedGroundDirection.Left)
                        {
                            hit = Physics2D.Raycast(bottomLeftRaycastOrigin.position, Vector2.left, 0.1f, groundCollisionMask);
                        }
                        else
                        {
                            hit = Physics2D.Raycast(bottomRightRaycastOrigin.position, Vector2.right, 0.1f, groundCollisionMask);
                        }

                        if (!hit)
                        {
                            currentVelocity.y = 0f;
                        }                       
                    }
                    break;
                case GravityDirection.Up:
                    verticalTargetVelocity = groundClimbSpeed * verticalInput.Value * -1f;
                    currentVelocity.y = Mathf.SmoothDamp(currentVelocity.y, verticalTargetVelocity, ref horizontalVelocitySmoothing,
                                                         accelerationTimeClimbing * attachedGround.FrictionModifier);

                    if (currentVelocity.y > 0f)
                    {                        
                        if (attachedGroundDirection == AttachedGroundDirection.Left)
                        {
                            hit = Physics2D.Raycast(topRightRaycastOrigin.position, Vector2.left, 0.1f, groundCollisionMask);
                        }
                        else
                        {
                            hit = Physics2D.Raycast(topLeftRaycastOrigin.position, Vector2.right, 0.1f, groundCollisionMask);
                        }

                        if (!hit)
                        {
                            currentVelocity.y = 0f;
                        }                                                
                    }
                    else if (currentVelocity.y < 0f)
                    {                        
                        if (attachedGroundDirection == AttachedGroundDirection.Left)
                        {
                            hit = Physics2D.Raycast(bottomRightRaycastOrigin.position, Vector2.left, 0.1f, groundCollisionMask);
                        }
                        else
                        {
                            hit = Physics2D.Raycast(bottomLeftRaycastOrigin.position, Vector2.right, 0.1f, groundCollisionMask);
                        }

                        if (!hit)
                        {
                            currentVelocity.y = 0f;
                        }                                                
                    }
                    break;
                case GravityDirection.Left:
                case GravityDirection.Right:
                    verticalTargetVelocity = groundClimbSpeed * horizontalInput.Value;
                    currentVelocity.x = Mathf.SmoothDamp(currentVelocity.x, verticalTargetVelocity, ref horizontalVelocitySmoothing,
                                                         accelerationTimeClimbing * attachedGround.FrictionModifier);

                    // TO-DO: Add raycasts like above for Up and Down.
                    break;
                default:
                    break;
            }

            this.currentVelocity.Value = currentVelocity;
        }

        #endregion walls
    }
}