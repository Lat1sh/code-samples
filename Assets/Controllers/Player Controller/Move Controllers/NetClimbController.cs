﻿using MetraReborn.Events;
using MetraReborn.ScriptableObjects;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Controls character's movemenet while attached to a net.
    /// </summary>
    public class NetClimbController : CharacterMoveController
    {
        #region Fields

        /// <summary>
        /// Component that serves as a facade for main character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        [Inject(Id = "HorizontalInput")]
        private FloatVariable horizontalInput;

        /// <summary>
        /// Scriptable object that holds current vertical input.
        /// </summary>
        [Inject(Id = "VerticalInput")]
        private FloatVariable verticalInput;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary>
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Event that is raised when jump state needs to be reset.
        /// </summary>
        [Inject(Id = "OnResetJumpState")]
        private GameEvent onResetJumpState;

        /// <summary>
        /// Character's net climb speed.
        /// </summary>
        private float netClimbSpeed;

        /// <summary>
        /// Reference to character's main collider.
        /// </summary>
        [Inject(Id = "MainCollider")]
        private BoxCollider2D mainCollider;

        /// <summary>
        /// Contact filter to use when checking for overlapping nets.
        /// </summary>
        private ContactFilter2D netContactFilter;

        /// <summary>
        /// Scriptable object that holds the net the character is grabbing.
        /// </summary>
        private UsedNet usedNetSO;

        /// <summary>
        /// Reference to the net object character is currently using.
        /// </summary>
        private NetFacade usedNet;

        private List<Collider2D> hitColliders = new List<Collider2D>();

        #endregion

        #region Init

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="characterTransform">Root transform of the character object.</param>
        /// <param name="netClimbSpeed">Character's net climb speed.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="characterFacade">Component that serves as a facade for main character.</param>
        /// <param name="usedNetSO">Scriptable object that holds the net the character is grabbing.</param>
        public NetClimbController(Transform characterTransform, float netClimbSpeed, Vector2Variable currentVelocity,
                                  CharacterFacade characterFacade, UsedNet usedNetSO) : base(characterTransform)
        {
            this.netClimbSpeed = netClimbSpeed;
            this.currentVelocity = currentVelocity;
            this.characterFacade = characterFacade;
            this.usedNetSO = usedNetSO;

            netContactFilter = new ContactFilter2D();
            netContactFilter.SetLayerMask(1 << LayerMask.NameToLayer("Nets"));
        }

        /// <summary>
        /// Sets behavior's type.
        /// </summary>
        protected override void SetType() => Type = CharacterMoveControllerType.Net;

        #endregion

        #region Enter/Exit

        /// <summary>
        /// Enables this controller.
        /// </summary>
        public override void Enable()
        {
            usedNet = usedNetSO.NetFacade;
            onResetJumpState.Raise();
        }

        /// <summary>
        /// Disables this controller.
        /// </summary>
        public override void Disable() => usedNet = null;

        #endregion

        #region Grab

        /// <summary>
        /// Checks whether the player is overlapping with a net when the grab net button is pressed.
        /// Switches current movemement mode if a net is detected.
        /// </summary>
        public void OnGrabNetButtonPressed()
        {
            int numberOfCollidingNets = mainCollider.OverlapCollider(netContactFilter, hitColliders);

            if (numberOfCollidingNets != 0)
            {
                Enable();
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates character's velocity and handles collisions while climbing nets.
        /// </summary>
        public override void PerformUpdate()
        {
            Vector3 oldPosition = characterTransform.position;
            var velocity = Vector2.zero;

            GravityDirection gravityDirection = this.characterFacade.GravityDirection;

            float distanceToEdge = usedNet.GetDistanceToEdge(mainCollider.bounds.center, gravityDirection);
            float scaledTimeSpeed = netClimbSpeed * Time.deltaTime;

            switch (gravityDirection)
            {
                case GravityDirection.Down:
                case GravityDirection.Up:
                    velocity.x = horizontalInput.Value * scaledTimeSpeed;
                    velocity.y = verticalInput.Value * scaledTimeSpeed;

                    if (velocity.y > 0f && gravityDirection == GravityDirection.Down)
                    {
                        velocity.y = Mathf.Clamp(velocity.y, 0f, distanceToEdge);
                    }
                    else if (velocity.y < 0f && gravityDirection == GravityDirection.Up)
                    {
                        velocity.y = Mathf.Clamp(velocity.y, -distanceToEdge, 0f);
                    }

                    break;
                case GravityDirection.Left:
                case GravityDirection.Right:
                    velocity.x = horizontalInput.Value * scaledTimeSpeed;
                    velocity.y = verticalInput.Value * scaledTimeSpeed;

                    if (velocity.x > 0f && gravityDirection == GravityDirection.Left)
                    {
                        velocity.x = Mathf.Clamp(velocity.x, 0f, distanceToEdge);
                    }
                    else if (velocity.x < 0f && gravityDirection == GravityDirection.Right)
                    {
                        velocity.x = Mathf.Clamp(velocity.x, -distanceToEdge, 0f);
                    }

                    break;
            }

            characterTransform.Translate(velocity, Space.World);

            currentVelocity.Value = (characterTransform.position - oldPosition) / Time.deltaTime;

            int numberOfCollidingNets = mainCollider.OverlapCollider(netContactFilter, hitColliders);

            if (numberOfCollidingNets == 0)
            {
                usedNetSO.NetFacade = null;
            }
        }

        #endregion
    }
}