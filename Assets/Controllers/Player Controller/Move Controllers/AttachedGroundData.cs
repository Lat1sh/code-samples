﻿namespace MetraReborn
{
    /// <summary>
    /// Data about a ground object that the character is attached to in alternative form.
    /// </summary>
    public class AttachedGroundData
    {
        #region Properties

        /// <summary>
        /// Ground object character is attached to.
        /// </summary>
        public IGround Ground { get; }

        /// <summary>
        ///  Direction relative to the player where the ground that they are attached to is.
        /// </summary>
        public AttachedGroundDirection AttachedGroundDirection { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="ground">Ground object character is attached to.</param>
        /// <param name="attachedGroundDirection">Direction relative to the player where the ground that they are attached to is.</param>
        public AttachedGroundData(IGround ground, AttachedGroundDirection attachedGroundDirection)
        {
            Ground = ground;
            AttachedGroundDirection = attachedGroundDirection;
        }

        #endregion
    }
}