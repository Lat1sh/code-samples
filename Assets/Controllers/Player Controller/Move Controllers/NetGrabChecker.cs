﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Checks whether character has grabbed a net when the respective button is pressed.
    /// </summary>
    public class NetGrabChecker : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that serves as a facade for main character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Scriptable object that holds the net the character is grabbing.
        /// </summary>
        private UsedNet usedNet;

        /// <summary>
        /// Character's main collider.
        /// </summary>
        [Inject(Id = "MainCollider")]
        private BoxCollider2D mainCollider;

        /// <summary>
        /// Contact filter to use when checking for overlapping nets.
        /// </summary>
        private ContactFilter2D netContactFilter;

        private List<Collider2D> hitColliders = new List<Collider2D>();

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="characterFacade">Component that serves as a facade for main character.</param>
        /// <param name="usedNet">Scriptable object that holds the net the character is grabbing.</param>
        [Inject]
        public void Construct(CharacterFacade characterFacade, UsedNet usedNet)
        {
            this.characterFacade = characterFacade;
            this.usedNet = usedNet;            
        }

        private void Start()
        {
            netContactFilter = new ContactFilter2D();
            netContactFilter.SetLayerMask(1 << LayerMask.NameToLayer("Nets"));
        }

        #endregion

        #region Check

        /// <summary>
        /// Informs the <see cref="CharacterMoveControllerSwitcher"/> to switch to 
        /// <see cref="NetClimbController"/> if character is overlapping with a net.
        /// </summary>
        public void GrabNetIfOverlapping()
        {
            if (CharacterMoveController.Active.Type == CharacterMoveControllerType.Default ||
                CharacterMoveController.Active.Type == CharacterMoveControllerType.AccelerationZone)
            {
                int numberOfCollidingNets = mainCollider.OverlapCollider(netContactFilter, hitColliders);

                if (numberOfCollidingNets != 0 && usedNet.NetFacade == null)
                {
                    NetFacade netFacade = hitColliders[0].GetComponent<NetFacade>();

                    if (netFacade.IsSpectral == characterFacade.IsSpectral)
                    {
                        usedNet.NetFacade = netFacade;
                        return;
                    }
                }
            }
        }

        #endregion
    }
}