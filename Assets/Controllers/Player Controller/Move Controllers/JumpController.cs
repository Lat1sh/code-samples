﻿using MetraReborn.Events;
using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Initiates character jump when jump input is passed and handles jump flags.
    /// </summary>
    public class JumpController : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that serves as a facade for main character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Scriptable object that holds the is grounded flag.
        /// </summary>
        [Inject(Id = "IsGrounded")]
        private BoolVariable isGrounded;

        /// <summary>
        /// Scriptable object that holds current gravity value.
        /// </summary>
        [Inject(Id = "Gravity")]
        private FloatVariable gravity;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary>
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Scriptable object that holds the jump pad that the player just used.
        /// </summary>
        private UsedJumpPad usedJumpPad;

        /// <summary>
        /// Scriptable object that holds the net character is using.
        /// </summary>
        private UsedNet usedNet;

        /// <summary>
        /// Event that is raised when character releases the ground they are climbing in alternative form.
        /// </summary>
        [Inject(Id = "OnCharacterReleasedGround")]
        private GameEvent onCharacterReleasedGround;

        /// <summary>
        /// Event that is raised when the character stops dashing in alternative form.
        /// </summary>
        [Inject(Id = "OnCharacterStoppedDashing")]
        private GameEvent onCharacterStoppedDashing;

        /// <summary>
        /// Event that is raised when character starts a jump.
        /// </summary>
        [Inject(Id = "OnCharacterStartedJumping")]
        private GameEvent onCharacterStartedJumping;

        /// <summary>
        /// Time in seconds for the character to reach maximum jump height when jumping straight up.
        /// </summary>
        [Inject(Id = "TimeToJumpMaxHeight")]
        private float timeToJumpMaxHeight = 1f;

        /// <summary>
        /// Maximum jump height in units.
        /// </summary>
        [Inject(Id = "MaxJumpHeight")]
        private float maxJumpHeight = 1f;

        /// <summary>
        /// Minimum jump height in units.
        /// </summary>
        [Inject(Id = "MinJumpHeight")]
        private float minJumpHeight = 0.5f;

        /// <summary>
        /// Character's maximum jump speed.
        /// </summary>
        [ReadOnly]
        [SerializeField]
        private float maxJumpSpeed;

        /// <summary>
        /// Character's minimum jump speed.
        /// </summary>
        [ReadOnly]
        [SerializeField]
        private float minJumpSpeed;        

        /// <summary>
        /// Is character allowed to double jump?
        /// </summary>
        private bool doubleJumpAllowed = true;

        /// <summary>
        /// Has the player used their first jump?
        /// </summary>
        [ReadOnly]
        [SerializeField]
        private bool usedFirstJump = false;

        /// <summary>
        /// Has the player used their second jump?
        /// </summary>
        [ReadOnly]
        [SerializeField]
        private bool usedSecondJump = false;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="characterFacade">Component that serves as a facade for main character.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="usedJumpPad">Scriptable object that holds the jump pad that the player just used.</param>
        /// <param name="usedNet">Scriptable object that holds the net character is using.</param>
        [Inject]
        public void Construct(CharacterFacade characterFacade, Vector2Variable currentVelocity, UsedJumpPad usedJumpPad,
                              UsedNet usedNet)
        {
            this.characterFacade = characterFacade;
            this.currentVelocity = currentVelocity;
            this.usedJumpPad = usedJumpPad;
            this.usedNet = usedNet;
        }

        private void Start()
        {
            float gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpMaxHeight, 2);
            maxJumpSpeed = Mathf.Abs(gravity * timeToJumpMaxHeight);
            minJumpSpeed = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);

            if (characterFacade.GravityDirection == GravityDirection.Up || characterFacade.GravityDirection == GravityDirection.Right)
            {
                gravity *= -1f;
            }

            this.gravity.Value = gravity;
        }

        #endregion

        #region Jump

        /// <summary>
        /// Callback for when player pressed the jump button. Initiates a jump or 
        /// a double jump if conditions for those options are met.
        /// </summary>
        public void OnJumpButtonPressed()
        {
            if (doubleJumpAllowed && usedSecondJump)
            {
                return;
            }
            else if (!doubleJumpAllowed && usedFirstJump)
            {
                return;
            }

            if (!usedFirstJump)
            {
                usedFirstJump = true;
            }
            else if (doubleJumpAllowed && !usedSecondJump)
            {
                usedSecondJump = true;
            }

            if (isGrounded.Value)
            {
                isGrounded.Value = false;
            }

            Vector2 currentVelocity = this.currentVelocity.Value;

            switch (characterFacade.GravityDirection)
            {
                case GravityDirection.Down:
                    currentVelocity.y = maxJumpSpeed;
                    break;
                case GravityDirection.Up:
                    currentVelocity.y = maxJumpSpeed * -1f;
                    break;
                case GravityDirection.Left:
                    currentVelocity.x = maxJumpSpeed;
                    break;
                case GravityDirection.Right:
                    currentVelocity.x = maxJumpSpeed * -1f;
                    break;
            }

            this.currentVelocity.Value = currentVelocity;

            CharacterMoveControllerType activeControllerType = CharacterMoveController.Active.Type;

            switch (activeControllerType)
            {
                case CharacterMoveControllerType.Net:
                    usedNet.NetFacade = null;
                    break;
                case CharacterMoveControllerType.Climb:
                    onCharacterReleasedGround.Raise();
                    break;
                case CharacterMoveControllerType.Dash:
                    onCharacterStoppedDashing.Raise();
                    break;
                default:
                    break;
            }

            onCharacterStartedJumping.Raise();
        }

        /// <summary>
        /// Callback for when player releases the jump button. Reduces vertical velocity to minimum jump speed.
        /// </summary>
        public void OnJumpButtonReleased()
        {
            Vector2 currentVelocity = this.currentVelocity.Value;

            switch (characterFacade.GravityDirection)
            {
                case GravityDirection.Down:
                    if (currentVelocity.y > minJumpSpeed)
                    {
                        currentVelocity.y = minJumpSpeed;
                    }
                    break;
                case GravityDirection.Up:
                    if (currentVelocity.y < -minJumpSpeed)
                    {
                        currentVelocity.y = -minJumpSpeed;
                    }
                    break;
                case GravityDirection.Left:
                    if (currentVelocity.x < -minJumpSpeed)
                    {
                        currentVelocity.x = -minJumpSpeed;
                    }
                    break;
                case GravityDirection.Right:
                    if (currentVelocity.x > minJumpSpeed)
                    {
                        currentVelocity.x = minJumpSpeed;
                    }
                    break;
            }

            this.currentVelocity.Value = currentVelocity;
        }

        /// <summary>
        /// Resets jump flags.
        /// </summary>
        public void ResetJumpState()
        {
            usedFirstJump = usedSecondJump = false;
        }

        #endregion

        #region Jump pad

        /// <summary>
        /// Called when the player uses a jump pad.
        /// </summary>
        public void OnJumpPadUsed()
        {
            ResetJumpState();

            Vector2 currentVelocity = this.currentVelocity.Value;     

            switch (characterFacade.GravityDirection)
            {
                case GravityDirection.Down:
                    currentVelocity.y = maxJumpSpeed * usedJumpPad.Pad.PadMagnitude * (currentVelocity.y < 0 ? 1f : -1f);
                    break;
                case GravityDirection.Up:
                    currentVelocity.y = -maxJumpSpeed * usedJumpPad.Pad.PadMagnitude * (currentVelocity.y > 0 ? 1f : -1f);
                    break;
                case GravityDirection.Left:
                    currentVelocity.x = maxJumpSpeed * usedJumpPad.Pad.PadMagnitude * (currentVelocity.x < 0 ? 1f : -1f);
                    break;
                case GravityDirection.Right:
                    currentVelocity.x = -maxJumpSpeed * usedJumpPad.Pad.PadMagnitude * (currentVelocity.x > 0 ? 1f : -1f);
                    break;
            }

            this.currentVelocity.Value = currentVelocity;
        }

        #endregion
    }
}