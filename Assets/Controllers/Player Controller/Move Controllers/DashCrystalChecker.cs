﻿using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Checks whether player is colliding with a dash crystal and raises the respective event if they are.
    /// </summary>
    public class DashCrystalChecker : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Event that is raised when character starts dashing in alternative form.
        /// </summary>
        [Inject(Id = "OnCharacterStartedDashing")]
        private Events.GameEvent onCharacterStartedDashing;

        /// <summary>
        /// Character's main collider.
        /// </summary>
        [Inject(Id = "MainCollider")]
        private BoxCollider2D mainCollider;

        /// <summary>
        /// Contact filter to use when checking for overlapping dash crystals.
        /// </summary>
        private ContactFilter2D dashCrystalContactFilter;

        private Collider2D[] hitColliders = new Collider2D[1];

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="dashCrystalLayerMask">Layer mask to use when checking for overlapping dash crystals.</param>
        [Inject]
        private void Construct(LayerMask dashCrystalLayerMask)
        {
            dashCrystalContactFilter = new ContactFilter2D();
            dashCrystalContactFilter.SetLayerMask(dashCrystalLayerMask);
        }

        #endregion

        #region Check

        /// <summary>
        /// Informs the <see cref="CharacterMoveControllerSwitcher"/> to change
        /// to <see cref="DashController"/> if character is overlapping with a dash crystal.
        /// </summary>
        public void UseDashCrystalIfOverlapping()
        {
            int numberOfCollidingDashCrystals = mainCollider.OverlapCollider(dashCrystalContactFilter, hitColliders);

            if (numberOfCollidingDashCrystals != 0)
            {
                onCharacterStartedDashing.Raise();
            }
        }

        #endregion
    }
}