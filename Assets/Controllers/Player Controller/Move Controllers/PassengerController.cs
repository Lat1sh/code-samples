﻿using MetraReborn.Events;
using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Handles character's position and collisions when they are standing on a moving platform.
    /// </summary>
    public class PassengerController : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary>
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Scriptable object that holds reference to the ground object that the player is currently standing on.
        /// </summary>
        private CurrentGround currentGround;

        /// <summary>
        /// Event that is raised when horizontal raycast data needs to be calculated.
        /// </summary>
        [Inject(Id = "OnCalculateHorizontalRaycastData")]
        private GameEvent onCalculateHorizontalRaycastData;

        /// <summary>
        /// Event that is raised when vertical raycast data needs to be calculated.
        /// </summary>
        [Inject(Id = "OnCalculateVerticalRaycastData")]
        private GameEvent onCalculateVerticalRaycastData;

        #endregion

        #region Init

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="currentGround">Scriptable object that holds reference to the ground object that the player is 
        /// currently standing on.</param>
        [Inject]
        public void Construct(Vector2Variable currentVelocity, CurrentGround currentGround)
        {
            this.currentVelocity = currentVelocity;
            this.currentGround = currentGround;
        }

        #endregion

        #region Update

        private void Update()
        {
            if (currentGround.Current == null)
            {
                return;
            }

            Vector2 oldVelocity = currentVelocity.Value;
            currentVelocity.Value = currentGround.Current.CurrentVelocity * Time.deltaTime;
            var passengerVelocity = Vector2.zero;

            onCalculateHorizontalRaycastData.Raise();

            passengerVelocity.x = currentVelocity.Value.x;

            onCalculateVerticalRaycastData.Raise();

            passengerVelocity.y = currentVelocity.Value.y;

            transform.Translate(passengerVelocity * Time.deltaTime, Space.World);

            currentVelocity.Value = oldVelocity;
        }

        #endregion
    }
}