﻿using MetraReborn.Events;
using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Controls character's behavior during teleportation.
    /// </summary>
    public class TeleportationController : CharacterMoveController
    {
        #region Fields

        /// <summary>
        /// Scriptable object that holds the teleporter currently used by the player.
        /// </summary>
        private UsedTeleporter usedTeleporterSO;

        /// <summary>
        /// Teleporter currently used by the player.
        /// </summary>
        private TeleporterFacade usedTeleporter;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary>
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        [Inject(Id = "HorizontalInput")]
        private FloatVariable horizontalInput;     

        /// <summary>
        /// Event that is raised when player reaches teleportation target.
        /// </summary>
        [Tooltip("Event that is raised when player reaches teleportation target.")]
        [SerializeField]
        private GameEvent onTeleportationFinished;

        /// <summary>
        /// Speed at which character moves while teleporting.
        /// </summary>
        private float teleportationSpeed = 5f;

        /// <summary>
        /// Reference to the main SpriteRenderer component.
        /// </summary>
        [Inject(Id = "MainSpriteRenderer")]
        private SpriteRenderer mainSpriteRenderer;

        /// <summary>
        /// Reference to the sprite renderer used during teleportation.
        /// </summary>
        [Inject(Id = "TeleportationSpriteRenderer")]
        private SpriteRenderer teleportationSpriteRenderer;

        /// <summary>
        /// Character's rotation before teleportation started.
        /// </summary>
        private Quaternion rotationBeforeTeleporting;

        /// <summary>
        /// FlipX flag on the sprite renderer before teleportation started.
        /// </summary>
        private bool flipXBeforeTeleporting;

        /// <summary>
        /// FlipY flag on the sprite renderer before teleportation started.
        /// </summary>
        private bool flipYBeforeTeleporting;

        /// <summary>
        /// Current teleportation target position.
        /// </summary>
        private Vector3 teleportationTarget;

        /// <summary>
        /// Component that serves as a facade object for the main character.
        /// </summary>
        private CharacterFacade characterFacade;

        #endregion

        #region Constants

        /// <summary>
        /// When distance to teleportation target goes below this threshold, teleporation is considered to have finished.
        /// </summary>
        private float TeleportationEndedDistance = 0.01f;

        #endregion

        #region Init

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="characterTransform">Root transform of the character object.</param>
        /// <param name="usedTeleporterSO">Scriptable object that holds the teleporter currently used by the player.</param>
        /// <param name="onTeleportationFinished">Event that is raised when player reaches teleportation target.</param>
        /// <param name="teleportationSpeed">Speed at which character moves while teleporting.</param>
        /// <param name="characterFacade">Component that serves as a facade object for the main character.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="gravityDirection">Scriptable object that holds character's current gravity direction.</param>
        public TeleportationController(Transform characterTransform, UsedTeleporter usedTeleporterSO,
                                       GameEvent onTeleportationFinished, float teleportationSpeed,
                                       CharacterFacade characterFacade, Vector2Variable currentVelocity
                                       ) : base(characterTransform)
        {
            this.usedTeleporterSO = usedTeleporterSO;
            this.onTeleportationFinished = onTeleportationFinished;
            this.teleportationSpeed = teleportationSpeed;
            this.characterFacade = characterFacade;
            this.currentVelocity = currentVelocity;
        }

        /// <summary>
        /// Sets behavior's type.
        /// </summary>
        protected override void SetType() => Type = CharacterMoveControllerType.Teleport;

        #endregion

        #region Enable/disable

        /// <summary>
        /// Enables this controller.
        /// </summary>
        public override void Enable()
        {
            usedTeleporter = usedTeleporterSO.Teleporter;

            currentVelocity.Value = Vector3.zero;
            rotationBeforeTeleporting = characterTransform.rotation;
            flipXBeforeTeleporting = mainSpriteRenderer.flipX;
            flipYBeforeTeleporting = mainSpriteRenderer.flipY;

            characterTransform.position = usedTeleporter.ConnectedTeleporter.TeleportationPosition;

            Vector3 targetPosition = usedTeleporter.TeleportationPosition;
            Vector3 targetVector = targetPosition - characterTransform.position;

            float targetAngle = Vector2.SignedAngle(targetVector, Vector2.right) * -1f;

            switch (characterFacade.GravityDirection)
            {
                case GravityDirection.Down:
                case GravityDirection.Up:
                    mainSpriteRenderer.flipY = Mathf.Abs(targetAngle) > 90f;
                    mainSpriteRenderer.flipX = Mathf.Abs(targetAngle) > 90f ? targetPosition.x > characterTransform.position.x :
                                                                  targetPosition.x < characterTransform.position.x;
                    break;
                case GravityDirection.Left:
                case GravityDirection.Right:
                    mainSpriteRenderer.flipY = Mathf.Abs(targetAngle) > 90f;

                    mainSpriteRenderer.flipX = Mathf.Sign(targetAngle) > 0f ? targetPosition.y < characterTransform.position.y :
                                                                          targetPosition.y > characterTransform.position.y;
                    break;
            }

            characterTransform.rotation = Quaternion.Euler(0f, 0f, targetAngle);

            teleportationSpriteRenderer.enabled = true;
            mainSpriteRenderer.enabled = false;
        }

        /// <summary>
        /// Disables this controller.
        /// </summary>
        public override void Disable()
        {
            characterTransform.position = teleportationTarget;
            characterTransform.rotation = rotationBeforeTeleporting;
            mainSpriteRenderer.flipX = flipXBeforeTeleporting;
            mainSpriteRenderer.flipY = flipYBeforeTeleporting;

            teleportationSpriteRenderer.enabled = false;
            mainSpriteRenderer.enabled = true;

            // TO-DO: Change character's gravity direction here.
        }

        #endregion

        #region Update

        /// <summary>
        /// Update's character's position while teleporting.
        /// </summary>
        public override void PerformUpdate()
        {
            teleportationTarget = usedTeleporter.TeleportationPosition;

            characterTransform.position =  Vector3.MoveTowards(characterTransform.position, teleportationTarget, teleportationSpeed * Time.deltaTime);

            if (Vector3.Distance(characterTransform.position, teleportationTarget) < TeleportationEndedDistance)
            {
                usedTeleporterSO.Teleporter = null;
            }
        }

        #endregion

    }
}