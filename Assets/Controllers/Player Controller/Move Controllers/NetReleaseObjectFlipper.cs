﻿using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Flips the character if necessary when they release a net
    /// </summary>
    public class NetReleaseObjectFlipper : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that servesa as a facade for playable character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Component that flips main character.
        /// </summary>
        private FlipController flipController;

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        [Inject(Id = "HorizontalInput")]
        private FloatVariable horizontalInput;

        /// <summary>
        /// Scriptable object that holds current vertical input.
        /// </summary>
        [Inject(Id = "VerticalInput")]
        private FloatVariable verticalInput;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="characterFacade">Component that servesa as a facade for playable character.</param>
        /// <param name="flipController">Component that flips main character.</param>
        [Inject]
        public void Construct(CharacterFacade characterFacade, FlipController flipController)
        {
            this.characterFacade = characterFacade;
            this.flipController = flipController;
        }

        #endregion

        #region Flip

        /// <summary>
        /// Flips the character if necessary when a net is released.
        /// </summary>
        public void FlipOnNetReleaseIfNecessary()
        {
            float input;

            switch (characterFacade.GravityDirection)
            {
                case GravityDirection.Down:
                    input = horizontalInput.Value;
                    if ((input == 1f && !flipController.IsFacingRight) ||
                        (input == -1f && flipController.IsFacingRight))
                    {
                        flipController.FlipObject();
                    }
                    break;                    
                case GravityDirection.Up:
                    input = horizontalInput.Value;
                    if ((input == 1f && flipController.IsFacingRight) ||
                        (input == -1f && !flipController.IsFacingRight))
                    {
                        flipController.FlipObject();
                    }
                    break;
                case GravityDirection.Left:
                    input = verticalInput.Value;
                    if ((input == 1f && flipController.IsFacingRight) ||
                        (input == -1f && !flipController.IsFacingRight))
                    {
                        flipController.FlipObject();
                    }
                    break;
                case GravityDirection.Right:
                    input = verticalInput.Value;
                    if ((input == 1f && !flipController.IsFacingRight) ||
                        (input == -1f && flipController.IsFacingRight))
                    {
                        flipController.FlipObject();
                    }
                    break;
            }
        }

        #endregion
    }
}