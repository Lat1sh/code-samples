﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Base class for controllers that handle various forms of character movement: running, climbing, using dash pads, etc.
    /// </summary>
    public abstract class CharacterMoveController
    {
        #region Properties

        /// <summary>
        /// Type of move controller.
        /// </summary>
        public CharacterMoveControllerType Type { get; protected set; }

        /// <summary>
        /// Active move controller.
        /// </summary>
        public static CharacterMoveController Active
        {
            get => active;

            set
            {
                active?.Disable();
                active = value;
                active?.Enable();
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Root transform of the character object.
        /// </summary>
        protected Transform characterTransform;

        private static CharacterMoveController active;

        #endregion

        #region Constructor

        /// <summary>
        /// Base constructor.
        /// </summary>
        /// <param name="characterTransform">Root transform of the character object.</param>
        public CharacterMoveController(Transform characterTransform)
        {
            this.characterTransform = characterTransform;
            SetType();
        }

        #endregion

        #region Enable/Disable

        public virtual void Enable() { }
        public virtual void Disable() { }

        #endregion

        #region Abstract

        public abstract void PerformUpdate();
        

        protected abstract void SetType();

        #endregion
    }
}