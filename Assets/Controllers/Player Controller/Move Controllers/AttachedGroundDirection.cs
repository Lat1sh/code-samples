﻿namespace MetraReborn
{
    /// <summary>
    /// Direction relative to the player where the ground that they are attached to is.
    /// </summary>
    public enum AttachedGroundDirection
    {
        Right,
        Left,
        Above,
        Below        
    }
}