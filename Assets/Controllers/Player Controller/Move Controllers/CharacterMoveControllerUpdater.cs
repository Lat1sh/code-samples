﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Tells the active <see cref="CharacterMoveController"/> to perform an update.
    /// </summary>
    public class CharacterMoveControllerUpdater : MonoBehaviour, IPausable
    {
        #region Properties

        /// <summary>
        /// Is the game currently paused?
        /// </summary>
        public bool IsPaused { get; private set; } = false;

        #endregion

        #region Update

        private void Update()
        {
            if (IsPaused)
            {
                return;
            }

            CharacterMoveController.Active.PerformUpdate();
        }

        #endregion        

        #region Pause

        /// <summary>
        /// Sets the <see cref="IsPaused"/> flag to True when game is paused.
        /// </summary>
        public void Pause() => IsPaused = true;

        /// <summary>
        /// Sets the <see cref="IsPaused"/> flag to False when game is unpaused.
        /// </summary>
        public void Unpause() => IsPaused = false;

        #endregion
    }
}