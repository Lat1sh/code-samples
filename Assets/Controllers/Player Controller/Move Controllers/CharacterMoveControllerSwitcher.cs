﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Switches active <see cref="CharacterMoveController"/> based on what's going on in the game.
    /// </summary>
    public class CharacterMoveControllerSwitcher : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Dictionary storing all move controllers for easier access to them when switching.
        /// </summary>
        private Dictionary<CharacterMoveControllerType, CharacterMoveController> moveControllers;

        /// <summary>
        /// Object that handles default movement like running and jumping.
        /// </summary>
        [Inject]
        private DefaultMoveController defaultMoveController;

        /// <summary>
        /// Object that handles movement while teleporting.
        /// </summary>
        [Inject]
        private TeleportationController teleportationController;

        /// <summary>
        /// Object that handles movement when climbing a net.
        /// </summary>
        [Inject]
        private NetClimbController netClimbController;

        /// <summary>
        /// Object that handles movement when climbing ground.
        /// </summary>
        [Inject]
        private GroundClimbController groundClimbController;

        /// <summary>
        /// Object that handles movement when using a dash pad.
        /// </summary>
        [Inject]
        private DashPadController dashPadController;

        /// <summary>
        /// Object that handles movement when character is inside an acceleration zone.
        /// </summary>
        [Inject]
        private AccelerationZoneController accelerationZoneController;

        /// <summary>
        /// Object that handles movement when character is dashing in alternative form.
        /// </summary>
        [Inject]
        private DashController dashController;

        #endregion

        #region Init

        private void Start()
        {
            moveControllers = new Dictionary<CharacterMoveControllerType, CharacterMoveController>()
            {
                { CharacterMoveControllerType.Default, defaultMoveController },
                { CharacterMoveControllerType.Teleport, teleportationController },
                { CharacterMoveControllerType.Climb, groundClimbController },
                { CharacterMoveControllerType.Net, netClimbController },
                { CharacterMoveControllerType.Dash, dashController},
                { CharacterMoveControllerType.AccelerationZone, accelerationZoneController},
                { CharacterMoveControllerType.DashPad, dashPadController }
                
            };

            Switch(CharacterMoveControllerType.Default);
        }

        private void OnEnable()
        {
            if (moveControllers == null)
            {
                return;
            }

            Switch(CharacterMoveControllerType.Default);
        }

        #endregion

        #region Switch

        // TO-DO: Is there a way to pass an enum into Unity events' inspector representation?
        // There used to be none but something may have changed in the newer versions?
        // Or see if a custom inspector of some sort is possible to avoid having virtually identical methods.

        /// <summary>
        /// Switches active <see cref="CharacterMoveController"/> to <see cref="DefaultMoveController"/>.
        /// </summary>
        public void SwitchToDefaultMoveController() => Switch(CharacterMoveControllerType.Default);

        /// <summary>
        /// Switches active <see cref="CharacterMoveController"/> to <see cref="TeleportationController"/>.
        /// </summary>
        public void SwitchToTeleportationController() => Switch(CharacterMoveControllerType.Teleport);

        /// <summary>
        /// Switches active <see cref="CharacterMoveController"/> to <see cref="GroundClimbController"/>.
        /// </summary>
        public void SwitchToGroundClimbController() => Switch(CharacterMoveControllerType.Climb);

        /// <summary>
        /// Switches active <see cref="CharacterMoveController"/> to <see cref="NetClimbController"/>.
        /// </summary>
        public void SwitchToNetClimbController() => Switch(CharacterMoveControllerType.Net);

        /// <summary>
        /// Switches active <see cref="CharacterMoveController"/> to <see cref="DashPadController"/>.
        /// </summary>
        public void SwitchToDashPadController() => Switch(CharacterMoveControllerType.DashPad);

        /// <summary>
        /// Switches active <see cref="CharacterMoveController"/> to <see cref="DashController"/>.
        /// </summary>
        public void SwitchToDashController() => Switch(CharacterMoveControllerType.Dash);

        /// <summary>
        /// Switches active <see cref="CharacterMoveController"/> to <see cref="AccelerationZoneController"/>.
        /// </summary>
        public void SwitchToAccelerationZoneController() => Switch(CharacterMoveControllerType.AccelerationZone);

        /// <summary>
        /// Switches active <see cref="CharacterMoveController"/> based on passed type.
        /// </summary>
        /// <param name="characterMoveControllerType">Type of controller to switch to.</param>
        private void Switch(CharacterMoveControllerType characterMoveControllerType)
        {
            CharacterMoveController.Active = moveControllers[characterMoveControllerType];
        }

        #endregion
    }
}