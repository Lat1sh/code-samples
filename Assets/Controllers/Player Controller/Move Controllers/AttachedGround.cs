﻿using MetraReborn.Events;
using UnityEngine;

namespace MetraReborn.ScriptableObjects
{
    /// <summary>
    /// Scriptable object that holds data about the ground object that the character is attached to.
    /// </summary>
    [CreateAssetMenu(fileName = "AttachedGround", menuName = "Scriptable Objects/Character/Attached Ground", order = 6)]
    public class AttachedGround : ScriptableObject
    {        
        #region Properties

        /// <summary>
        /// Current ground the character is attached to in alternative form, if any.
        /// </summary>
        /// <value>
        /// Gets and sets the value of the field current.
        /// </value>
        public AttachedGroundData Data
        {
            get => data;

            set
            {
                data = value;

                if (data == null)
                {
                    onCharacterReleasedGround.Raise();
                }
                else
                {
                    onCharacterGrabbedGround.Raise();
                }
            }
        }


        #endregion

        #region Fields

        /// <summary>
        /// Event that is raised when character grabs ground in alternative form.
        /// </summary>
        [Tooltip("Event that is raised when character grabs ground in alternative form.")]
        [SerializeField]
        private GameEvent onCharacterGrabbedGround;

        /// <summary>
        /// Event that is raised when character releases ground in alternative form.
        /// </summary>
        [Tooltip("Event that is raised when character releases ground in alternative form.")]
        [SerializeField]
        private GameEvent onCharacterReleasedGround;

        private AttachedGroundData data;

        #endregion
    }
}
