﻿using MetraReborn.Events;
using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Handles movement while main character is dashing in their alternative form.
    /// </summary>
    public class DashController : CharacterMoveController
    {
        #region Fields

        /// <summary>
        /// Component that serves as a facade for playable character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary> 
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Component that flips this object.
        /// </summary>
        private FlipController flipController;

        /// <summary>
        /// Character's main box collider.
        /// </summary>
        [Inject(Id = "MainCollider")]
        private BoxCollider2D characterCollider;

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        [Inject(Id = "HorizontalInput")]
        private FloatVariable horizontalInput;

        /// <summary>
        /// Scriptable object that holds current vertical input.
        /// </summary>
        [Inject(Id = "VerticalInput")]
        private FloatVariable verticalInput;

        /// <summary>
        /// Event that is raised when horizontal collisions need to be processed.
        /// </summary>
        [Inject(Id = "OnProcessHorizontalCollisions")]
        private GameEvent onProcessHorizontalCollisions;

        /// <summary>
        /// Event that is raised when vertical collisions need to be processed.
        /// </summary>
        [Inject(Id = "OnProcessVerticalCollisions")]
        private GameEvent onProcessVerticalCollisions;

        /// <summary>
        /// Event that is raised when character stops dashing in alternative form.
        /// </summary>
        [Inject(Id = "OnCharacterStoppedDashing")]
        private GameEvent onCharacterStoppedDashing;

        /// <summary>
        /// Speed at which the character dashes.
        /// </summary>
        private float dashSpeed;

        /// <summary>
        /// Time it takes to reach full dash speed.
        /// </summary>
        private float accelerationTime;

        /// <summary>
        /// Direction of current dash.
        /// </summary>
        private Vector2 dashDirection;

        /// <summary>
        /// Character's current dash speed.
        /// </summary>
        private float currentSpeed;

        /// <summary>
        /// Is character already dashing?
        /// </summary>
        private bool isAlreadyDashing = false;

        private float velocitySmoothing;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="characterTransform">Character's root transform.</param>
        /// <param name="characterFacade">Component that serves as a facade for playable character.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="flipController">Component that flips this object.</param>
        /// <param name="dashSpeed">Speed at which the character dashes.</param>
        /// <param name="accelerationTime">Time it takes to reach full dash speed.</param>
        public DashController(Transform characterTransform, CharacterFacade characterFacade,
                              Vector2Variable currentVelocity, FlipController flipController, 
                              [Inject(Id = "DashSpeed")]float dashSpeed, 
                              [Inject(Id = "DashAccelerationTime")]float accelerationTime) : base(characterTransform)
        {
            this.characterFacade = characterFacade;
            this.currentVelocity = currentVelocity;
            this.flipController = flipController;
            this.dashSpeed = dashSpeed;
            this.accelerationTime = accelerationTime;
        }

        #endregion

        #region Init

        /// <summary>
        /// Sets controller's <see cref="CharacterMoveControllerType"/>.
        /// </summary>
        protected override void SetType() => Type = CharacterMoveControllerType.Dash;

        /// <summary>
        /// Enables this controller.
        /// </summary>
        public override void Enable()
        {
            currentSpeed = currentVelocity.Value.magnitude;

            dashDirection = new Vector2(horizontalInput.Value, verticalInput.Value);

            if (dashDirection == Vector2.zero)
            {
                switch (characterFacade.GravityDirection)
                {
                    case GravityDirection.Down:
                        dashDirection = flipController.IsFacingRight ? Vector2.right : Vector2.left;
                        break;
                    case GravityDirection.Up:
                        dashDirection = flipController.IsFacingRight ? Vector2.left : Vector2.right;
                        break;
                    case GravityDirection.Right:
                        dashDirection = flipController.IsFacingRight ? Vector2.up : Vector2.down;
                        break;
                    case GravityDirection.Left:
                        dashDirection = flipController.IsFacingRight ? Vector2.down : Vector2.up;
                        break;
                }
            }
        }

        #endregion

        #region Update

        public override void PerformUpdate()
        {
            currentSpeed = Mathf.SmoothDamp(currentSpeed, dashSpeed, ref velocitySmoothing, accelerationTime);

            Vector2 velocityBeforeCollisions = currentVelocity.Value = dashDirection * currentSpeed;

            onProcessHorizontalCollisions.Raise();
            onProcessVerticalCollisions.Raise();

            characterTransform.Translate(currentVelocity.Value * Time.deltaTime);

            if (currentVelocity.Value != velocityBeforeCollisions)
            {
                onCharacterStoppedDashing.Raise();
            }
        }

        #endregion
    }
}