﻿using MetraReborn.Events;
using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Handles main character's default movement state that includes running and jumping.
    /// </summary>
    public class DefaultMoveController : CharacterMoveController
    {
        #region Fields

        /// <summary>
        /// Component that serves as a facade for main character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary> 
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        [Inject(Id = "HorizontalInput")]
        private FloatVariable horizontalInput;

        /// <summary>
        /// Scriptable object that holds current vertical input.
        /// </summary>
        [Inject(Id = "VerticalInput")]
        private FloatVariable verticalInput;

        /// <summary>
        /// Scriptable object that holds reference to the ground object that the player is currently standing on.
        /// </summary>
        private CurrentGround currentGround;

        /// <summary>
        /// Scriptable object that holds the is grounded flag.
        /// </summary>
        [Inject(Id = "IsGrounded")]
        private BoolVariable isGrounded;

        /// <summary>
        /// Scriptable object holds the isInsideFanZone flag.
        /// </summary>
        [Inject(Id = "IsInsideFanZone")]
        private BoolVariable isInsideFanZone;

        /// <summary>
        /// Scriptable object that holds current gravity value.
        /// </summary>
        [Inject(Id = "Gravity")]
        private FloatVariable gravity;

        /// <summary>
        /// Event that is raised when horizontal collisions need to be processed.
        /// </summary>
        [Inject(Id = "OnProcessHorizontalCollisions")]
        private GameEvent onProcessHorizontalCollisions;

        /// <summary>
        /// Event that is raised when vertical collisions need to be processed.
        /// </summary>
        [Inject(Id = "OnProcessVerticalCollisions")]
        private GameEvent onProcessVerticalCollisions;

        /// <summary>
        /// Character's horizontal move speed.
        /// </summary>
        [Inject(Id = "HorizontalMoveSpeed")]
        private float horizontalMoveSpeed = 6f;

        /// <summary>
        /// Acceleration time when character is grounded.
        /// </summary>
        [Inject(Id = "AccelerationTimeGrounded")]
        private float accelerationTimeGrounded = 0.2f;

        /// <summary>
        /// Acceleration time when player is jumping or falling.
        /// </summary>
        [Inject(Id = "AccelerationTimeJumping")]
        private float accelerationTimeJumping = 0.15f;

        private float horizontalVelocitySmoothing;

        #endregion

        #region Init

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="characterTransform">Root transform of the character object.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="characterFacade">Component that serves as a facade for main character.</param>
        /// <param name="currentGround">Scriptable object that holds reference to the ground object 
        /// that the player is currently standing on.</param>
        public DefaultMoveController(Transform characterTransform, Vector2Variable currentVelocity,
                                     CharacterFacade characterFacade, CurrentGround currentGround) : base(characterTransform)
        {
            this.currentVelocity = currentVelocity;
            this.characterFacade = characterFacade;
            this.currentGround = currentGround;
        }

        /// <summary>
        /// Sets behavior's type.
        /// </summary>
        protected override void SetType() => Type = CharacterMoveControllerType.Default;

        #endregion

        #region Update

        public override void PerformUpdate()
        {
            GravityDirection gravityAtStartOfUpdate = characterFacade.GravityDirection;

            if (!isInsideFanZone.Value)
            {
                AddGravity();
            }

            UpdateHorizontalVelocity();

            onProcessHorizontalCollisions.Raise();
            onProcessVerticalCollisions.Raise();

            var scaledVelocity = Vector2.zero;

            GravityDirection currentGravityDirection = characterFacade.GravityDirection;

            SetScaledVelocity(ref scaledVelocity, gravityAtStartOfUpdate, currentVelocity.Value, currentGravityDirection, 
                             swapXAndY: currentGravityDirection != GravityDirection.Down && currentGravityDirection != GravityDirection.Up);

            characterTransform.Translate(scaledVelocity, Space.World);

            currentVelocity.Value = (Active.Type != this.Type) ? Vector2.zero : scaledVelocity / Time.deltaTime;
        }

        /// <summary>
        /// Adds gravity based on character's current gravity direction.
        /// </summary>
        private void AddGravity()
        {
            Vector2 currentVelocity = this.currentVelocity.Value;

            GravityDirection currentGravityDirection = characterFacade.GravityDirection;

            CalculateVerticalVelocityLimits(out float minVerticalVelocity, out float maxVerticalVelocity, currentGravityDirection);

            switch (currentGravityDirection)
            {
                case GravityDirection.Down:
                case GravityDirection.Up:
                    ResetVelocityComponentIfGrounded(ref currentVelocity, resetY: true);
                    currentVelocity.y = Mathf.Clamp(currentVelocity.y + gravity.Value * Time.deltaTime, 
                                                    minVerticalVelocity, maxVerticalVelocity);
                    break;

                case GravityDirection.Left:
                case GravityDirection.Right:
                    ResetVelocityComponentIfGrounded(ref currentVelocity, resetY: false);
                    currentVelocity.x = Mathf.Clamp(currentVelocity.x + gravity.Value * Time.deltaTime, 
                                                    minVerticalVelocity, maxVerticalVelocity);
                    break;
            }

            this.currentVelocity.Value = currentVelocity;
        }

        /// <summary>
        /// Resets one of velocity's componet if the character is grounded. Reset component depends on passed boolean value.
        /// </summary>
        private void ResetVelocityComponentIfGrounded(ref Vector2 currentVelocity, bool resetY)
        {
            if (!isGrounded.Value)
            {
                return;
            }

            if (resetY)
            {
                currentVelocity.y = 0f;
            }
            else
            {
                currentVelocity.x = 0f;
            }
        }

        /// <summary>
        /// Calculates values to use for clamping 
        /// </summary>
        /// <param name="minVerticalVelocity">Minimum allowed vertical velocity value.</param>
        /// <param name="maxVerticalVelocity">Maximum allowed vertical velocity value.</param>
        /// <param name="currentGravityDirection"></param>
        private void CalculateVerticalVelocityLimits(out float minVerticalVelocity, out float maxVerticalVelocity, 
                                                     GravityDirection currentGravityDirection)
        {
            minVerticalVelocity = gravity.Value;
            maxVerticalVelocity = -minVerticalVelocity;

            if (currentGravityDirection == GravityDirection.Up || currentGravityDirection == GravityDirection.Right)
            {
                minVerticalVelocity *= -1f;
                maxVerticalVelocity *= -1f;
            }
        }

        /// <summary>
        /// Updates horizontal velocity based on current gravity direction.
        /// </summary>
        private void UpdateHorizontalVelocity()
        {
            Vector2 currentVelocity = this.currentVelocity.Value;

            switch (characterFacade.GravityDirection)
            {
                case GravityDirection.Down:
                case GravityDirection.Up:
                    currentVelocity.x = CalculateHorizontalVelocityValue(currentVelocity.x, horizontalMoveSpeed * horizontalInput.Value);
                    break;
                case GravityDirection.Left:
                case GravityDirection.Right:
                    currentVelocity.y = CalculateHorizontalVelocityValue(currentVelocity.y, horizontalMoveSpeed * verticalInput.Value);
                    break;
                default:
                    break;
            }

            this.currentVelocity.Value = currentVelocity;
        }

        /// <summary>
        /// Uses a smoothing method to calculate a new value for character's horizontal 
        /// velocity based on current velocity and target velocity.
        /// </summary>
        /// <param name="currentVelocity">Current horizontal velocity.</param>
        /// <param name="targetVelocity">Target horizontal velocity.</param>
        /// <returns>
        /// A float.
        /// </returns>
        private float CalculateHorizontalVelocityValue(float currentVelocity, float targetVelocity)
        {
            return Mathf.SmoothDamp(currentVelocity, targetVelocity, ref horizontalVelocitySmoothing,
                                    isGrounded.Value ? accelerationTimeGrounded / currentGround.Current.FrictionModifier : accelerationTimeJumping);
        }

        /// <summary>
        /// Sets scaled velocity x and y components based on passed parameters.
        /// </summary>
        /// <param name="scaledVelocity">Reference to varialbe that stores scaled velocity.</param>
        /// <param name="gravityAtStartOfUpdate">Gravity direction at start if current frame.</param>
        /// <param name="currentGravityDirection">Gravity direction after collisions.</param>
        /// <param name="currentVelocity">Current object's unscaled velocity.</param>
        /// <param name="swapXAndY">Should x and y components of the velocity vector be swapped?</param>
        private void SetScaledVelocity(ref Vector2 scaledVelocity, GravityDirection gravityAtStartOfUpdate, 
                                                 Vector2 currentVelocity, GravityDirection currentGravityDirection, bool swapXAndY)
        {
            float xVelocity = currentVelocity.x * Time.deltaTime;
            float yVelocity = gravityAtStartOfUpdate == currentGravityDirection ? currentVelocity.y * Time.deltaTime : 0f;

            /*Debug.LogFormat("Unscaled X: {0}. Scaled X: {1}", currentVelocity.x, xVelocity);
            *//*Debug.LogFormat("Unscaled Y: {0}. Scaled Y: {1}", currentVelocity.y, yVelocity);*/
            
            if (swapXAndY)
            {
                scaledVelocity.y = xVelocity;
                scaledVelocity.x = yVelocity;
            }
            else
            {
                scaledVelocity.x = xVelocity;
                scaledVelocity.y = yVelocity;
            }
        }

        #endregion
    }
}