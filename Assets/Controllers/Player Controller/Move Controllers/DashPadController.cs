﻿using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Controls character's behavior while using a dash pad.
    /// </summary>
    public class DashPadController : CharacterMoveController
    {
        #region Fields

        /// <summary>
        /// Component that serves as a facade for playable character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary>  
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Scriptable object that holds the dash pad currently used by the player.
        /// </summary>
        private UsedDashPad usedDashPadSO;

        /// <summary>
        /// Reference to the currently used dash pad.
        /// </summary>
        private DashPadFacade usedDashPad;

        /// <summary>
        /// Character's speed when moving between dash pads.
        /// </summary>
        [Inject(Id = "DashPadSpeed")]
        private float dashSpeed;

        /// <summary>
        /// Time it takes for the dash velocity to reach its maximum value.
        /// </summary>
        [Inject(Id = "DashPadAccelerationTime")]
        private float dashPadAccelerationTime = 0.2f;        

        /// <summary>
        /// Target position when dashing.
        /// </summary>
        private Vector3 padTargetPosition;

        /// <summary>
        /// Character's speed on current frame.
        /// </summary>
        private float currentSpeed;     

        private float velocitySmoothing;

        #endregion

        #region Constants

        /// <summary>
        /// When distance from character to dash target goes below this threshold, dash is considered to have finished.
        /// </summary>
        private const float DashEndedDistance = 0.01f;

        #endregion

        #region Init

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="characterFacade">Component that serves as a facade for playable character.</param>
        /// <param name="characterTransform">Root transform of the character object.</param>
        /// <param name="usedDashPadSO">Scriptable object that holds the dash pad currently used by the player.</param>
        /// <param name="dashPadAccelerationTime">Time it takes for the dash velocity to reach its maximum value.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        public DashPadController(CharacterFacade characterFacade, Transform characterTransform, 
                                 UsedDashPad usedDashPadSO, Vector2Variable currentVelocity) : base(characterTransform)
        {
            this.characterFacade = characterFacade;
            this.usedDashPadSO = usedDashPadSO;
            this.currentVelocity = currentVelocity;
        }

        /// <summary>
        /// Sets behavior's type.
        /// </summary>
        protected override void SetType() => Type = CharacterMoveControllerType.DashPad;

        #endregion

        #region Enable/disable

        /// <summary>
        /// Enables this controller.
        /// </summary>
        public override void Enable()
        {
            usedDashPad = usedDashPadSO.DashPad;

            GravityDirection gravityDirection = characterFacade.GravityDirection;

            if (gravityDirection == GravityDirection.Up || gravityDirection == GravityDirection.Down)
            {
                currentSpeed = Mathf.Abs(currentVelocity.Value.x);
            }
            else
            {
                currentSpeed = Mathf.Abs(currentVelocity.Value.y);
            }            
        }

        /// <summary>
        /// Disables this controller.
        /// </summary>
        public override void Disable()
        {
            characterTransform.position = padTargetPosition;            
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates character's velocity and handles collisions while using dash pads.
        /// </summary>
        public override void PerformUpdate()
        {
            // TO-DO: Add collisions.

            padTargetPosition = usedDashPad.ConnectedPad.TargetPosition;

            currentSpeed = Mathf.SmoothDamp(currentSpeed, dashSpeed, ref velocitySmoothing, dashPadAccelerationTime);

            characterTransform.position = Vector3.MoveTowards(characterTransform.position, padTargetPosition, currentSpeed * Time.deltaTime);

            if (Vector3.Distance(characterTransform.position, padTargetPosition) < DashEndedDistance)
            {
                usedDashPadSO.DashPad = null;
            }
        }

        #endregion
    }
}