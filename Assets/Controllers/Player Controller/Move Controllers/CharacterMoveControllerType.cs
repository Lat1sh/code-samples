﻿namespace MetraReborn
{
    /// <summary>
    /// Type of character's move controller.
    /// <para>Default - running and jumping.</para>
    /// <para>Teleport - moving between teleporters.</para>
    /// <para>Climb- movement while attached to ground in Metra's alternative form.</para>
    /// <para>Net - movement while attached to a net.</para>
    /// <para>DashPad - moving between dash pads.</para>
    /// <para>AccelerationZone - movement while inside an acceleration zone.</para>
    /// <para>Dash = dashing in alternative form.</para>
    /// </summary>
    public enum CharacterMoveControllerType
    {
        Default,
        Teleport,
        Climb,
        Net,
        DashPad,
        AccelerationZone,
        Dash
    }
}