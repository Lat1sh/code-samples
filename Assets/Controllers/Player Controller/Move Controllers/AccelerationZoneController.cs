﻿using MetraReborn.Events;
using MetraReborn.ScriptableObjects;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Control's character's movement while inside an acceleration zone.
    /// </summary>
    public class AccelerationZoneController : CharacterMoveController
    {
        #region Fields

        /// <summary>
        /// Scriptable object that holds the acceleration zone that the player is currently using.
        /// </summary>
        private UsedAccelerationZone usedAccelerationZone;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary> 
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        [Inject(Id = "HorizontalInput")]
        private FloatVariable horizontalInput;

        /// <summary>
        /// Scriptable object that holds current vertical input.
        /// </summary>
        [Inject(Id = "VerticalInput")]
        private FloatVariable verticalInput;

        /// <summary>
        /// Event that is raised when jump state needs to be reset.
        /// </summary>
        [Inject(Id = "OnResetJumpState")]
        private GameEvent onResetJumpState;

        /// <summary>
        /// Reference to character's main collider.
        /// </summary>
        [Inject(Id = "MainCollider")]
        private BoxCollider2D mainCollider;

        /// <summary>
        /// Velocity at which the player was moving when they entered the acceleration zone.
        /// </summary>
        private Vector2 accelerationZoneVector;

        /// <summary>
        /// Event that is raised when horizontal collisions need to be processed.
        /// </summary>
        [Inject(Id = "OnProcessHorizontalCollisions")]
        private GameEvent onProcessHorizontalCollisions;

        /// <summary>
        /// Event that is raised when vertical collisions need to be processed.
        /// </summary>
        [Inject(Id = "OnProcessVerticalCollisions")]
        private GameEvent onProcessVerticalCollisions;

        /// <summary>
        /// Contact filter to use when checking for overlapping acceleration zones.
        /// </summary>
        private ContactFilter2D accelerationZoneContactFilter;

        private List<Collider2D> hitColliders = new List<Collider2D>();

        #endregion        

        #region Init

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="characterTransform">Root transform of the character object.</param>
        /// <param name="usedAccelerationZone">Scriptable object that holds the acceleration zone that 
        /// the player is currently using.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        public AccelerationZoneController(Transform characterTransform, UsedAccelerationZone usedAccelerationZone,
                                          Vector2Variable currentVelocity) : base(characterTransform)
        {
            this.usedAccelerationZone = usedAccelerationZone;
            this.currentVelocity = currentVelocity;
        }       

        /// <summary>
        /// Sets behavior's type.
        /// </summary>
        protected override void SetType() => Type = CharacterMoveControllerType.AccelerationZone;

        #endregion

        #region Enable/Disable

        /// <summary>
        /// Enables this controller.
        /// </summary>
        public override void Enable() => Active = this;

        #endregion

        #region Update

        /// <summary>
        /// Called when player collides with an acceleration zone.
        /// </summary>
        public void StartUsingAccelerationZone()
        {
            onResetJumpState.Raise();
            accelerationZoneVector = usedAccelerationZone.Zone.GetAccelerationVector(mainCollider.bounds);
            Enable();
        }

        /// <summary>
        /// Updates character's velocity when using acceleration zones.
        /// </summary>
        public override void PerformUpdate()
        {
            int numberOfCollidingAccelerationZones = mainCollider.OverlapCollider(accelerationZoneContactFilter, hitColliders);

            if (numberOfCollidingAccelerationZones == 0)
            {
                this.usedAccelerationZone.Zone = null;

                // To-DO: Notify switcher to change to default behavior.
                return;
            }

            AccelerationZoneFacade usedAccelerationZone = this.usedAccelerationZone.Zone;

            currentVelocity.Value += accelerationZoneVector * usedAccelerationZone.AccelerationFactor;

            float accelerationZoneMaxVelocity = usedAccelerationZone.MaxVelocity;

            Vector2 velocity = currentVelocity.Value;

            if (accelerationZoneVector.x != 0f)
            {

                velocity.x = Mathf.Clamp(velocity.x, -accelerationZoneMaxVelocity, accelerationZoneMaxVelocity);
                velocity.y = verticalInput.Value * usedAccelerationZone.LateralSpeed;
                
                currentVelocity.Value = velocity;

                onProcessHorizontalCollisions.Raise();
                characterTransform.Translate(currentVelocity.Value * Time.deltaTime, Space.World);
            }
            else
            {
                velocity.y = Mathf.Clamp(velocity.y, -accelerationZoneMaxVelocity, accelerationZoneMaxVelocity);
                velocity.x = horizontalInput.Value * usedAccelerationZone.LateralSpeed;
                currentVelocity.Value = velocity;

                onProcessVerticalCollisions.Raise();
                characterTransform.Translate(currentVelocity.Value * Time.deltaTime, Space.World);
            }
        }

        #endregion
    }
}