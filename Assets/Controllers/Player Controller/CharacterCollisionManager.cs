﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Coordinates <see cref="RaycastDataCalculator"/>, <see cref="CollisionDetector"/> and <see cref="CollisionResolver"/>
    /// to handle main character's horizontal and vertical collisions.
    /// </summary>
    public class CharacterCollisionManager : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that calculates raycast data to be used by <see cref="CollisionDetector"/>.
        /// </summary>
        private RaycastDataCalculator raycastDataCalculator;

        /// <summary>
        /// Component that detects vertical or horizontal collisions.
        /// </summary>
        private CollisionDetector collisionDetector;

        /// <summary>
        /// Component that resolves collisions detected by <see cref="collisionDetector"/>
        /// </summary>
        private CollisionResolver collisionResolver;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="raycastDataCalculator">Component that calculates raycast data to be used by <see cref="CollisionDetector"/>.</param>
        /// <param name="collisionDetector">Component that detects vertical or horizontal collisions.</param>
        /// <param name="collisionResolver">Component that resolves collisions detected by <see cref="collisionDetector"/>.</param>
        [Zenject.Inject]
        public void Construct(RaycastDataCalculator raycastDataCalculator, CollisionDetector collisionDetector, 
                              CollisionResolver collisionResolver)
        {
            this.raycastDataCalculator = raycastDataCalculator;
            this.collisionDetector = collisionDetector;
            this.collisionResolver = collisionResolver;
        }

        #endregion

        #region Collisions

        /// <summary>
        /// Tells <see cref="RaycastDataCalculator"/>, <see cref="CollisionDetector"/>, 
        /// and <see cref="CollisionResolver"/> to run their operations.
        /// </summary>
        public void ProcessCollisions(bool processHorizontal)
        {
            RaycastData raycastData = processHorizontal ? 
                                      raycastDataCalculator.CalculateHorizontalRaycastData():
                                      raycastDataCalculator.CalculateVerticalRaycastData();

            if (raycastData.Velocity != 0f)
            {
                collisionResolver.ResolveCollisions(resolvingVertical: !processHorizontal,
                                                    processHorizontal ?
                                                    collisionDetector.GetHorizontalCollisions(ref raycastData) :
                                                    collisionDetector.GetVerticalCollisions(ref raycastData),
                                                    raycastData.Velocity);
            }
        }

        #endregion
    }
}