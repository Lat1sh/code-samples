﻿using MetraReborn.Events;
using UnityEngine;

namespace MetraReborn.ScriptableObjects
{
    /// <summary>
    /// A scriptable object that holds the jump pad the player has just used, if any.
    /// </summary>
    [CreateAssetMenu(fileName = "UsedJumpPad", menuName = "Scriptable Objects/Character/Used Jump Pad", order = 7)]
    public class UsedJumpPad : ScriptableObject
    {        
        #region Properties

        /// <summary>
        /// Jump pad that the character just used.
        /// </summary>
        public JumpPadFacade Pad
        {
            get => pad;

            set
            {
                pad = value;

                if (pad != null)
                {
                    onJumpPadUsed.Raise();
                }
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Event that is fired when character uses a jump pad.
        /// </summary>
        [Tooltip("Event that is fired when character uses a jump pad.")]
        [SerializeField]
        private GameEvent onJumpPadUsed;

        [ReadOnly]
        [SerializeField]
        private JumpPadFacade pad;

        #endregion
    }
}