﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Tells the <see cref="FlipController"/> to flip the character based on received horizontal input.
    /// </summary>
    public class InputBasedCharacterFlipper : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Component that serves as a facade for main character object.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Component that flips this object.
        /// </summary>
        private FlipController flipController;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="characterFacade">Component that serves as a facade for main character object.</param>
        /// <param name="flipController">Component that flips this object.</param>
        [Zenject.Inject]
        public void Construct(CharacterFacade characterFacade, FlipController flipController)
        {
            this.characterFacade = characterFacade;
            this.flipController = flipController;
        }

        #endregion

        /// <summary>
        /// Called when directional (up/down/left/right) input is received. Tells the <see cref="SpriteFlipper"/> to flip the sprite.
        /// </summary>
        /// <param name="inputDirection">Input direction. 0 - right. 1 - left. 2 - up. 3 - down.</param>
        public void OnDirectionalInputReceived(int inputDirection)
        {
            if (CharacterMoveController.Active.Type != CharacterMoveControllerType.Default &&
                CharacterMoveController.Active.Type != CharacterMoveControllerType.AccelerationZone)
            {
                return;
            }

            GravityDirection currentGravityDirection = characterFacade.GravityDirection;

            switch (inputDirection)
            {
                case 0:
                case 1:
                    if (currentGravityDirection == GravityDirection.Down)
                    {
                        flipController.FlipObject(isFacingRight: inputDirection == 0);
                    }
                    else if (currentGravityDirection == GravityDirection.Up)
                    {
                        flipController.FlipObject(isFacingRight: inputDirection != 0);
                    }
                    break;
                case 2:
                    if (currentGravityDirection == GravityDirection.Left)
                    {
                        flipController.FlipObject(isFacingRight: true);
                    }
                    else if (currentGravityDirection == GravityDirection.Right)
                    {
                        flipController.FlipObject(isFacingRight: false);
                    }
                    break;
                case 3:
                    if (currentGravityDirection == GravityDirection.Left)
                    {
                        flipController.FlipObject(isFacingRight: false);
                    }
                    else if (currentGravityDirection == GravityDirection.Right)
                    {
                        flipController.FlipObject(isFacingRight: true);
                    }
                    break;
            }
        }
    }
}