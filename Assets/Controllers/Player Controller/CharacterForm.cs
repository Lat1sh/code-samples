﻿namespace MetraReborn
{
    /// <summary>
    /// Forms that the main character can take on.
    /// </summary>
    public enum CharacterForm
    {
        Default,
        Alternative
    }
}