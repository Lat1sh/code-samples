﻿using MetraReborn.Events;
using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// A scriptable object that holds current acceleration zone that the player is using, if any.
    /// </summary>
    [CreateAssetMenu(fileName = "UsedAccelerationZone", menuName = "Scriptable Objects/Character/Used Acceleration Zone", order = 5)]
    public class UsedAccelerationZone : ScriptableObject
    {     
        #region Properties

        /// <summary>
        /// Currently used acceleration zone.
        /// </summary>
        /// <value>
        /// Gets and sets the value of the field zone.
        /// </value>
        public AccelerationZoneFacade Zone
        {
            get => zone;

            set
            {
                zone = value;

                if (zone != null)
                {
                    onEnteredAccelerationZone.Raise();
                }
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Event that is raised whgen player enters an acceleration zone.
        /// </summary>
        [SerializeField]
        private GameEvent onEnteredAccelerationZone;

#if UNITY_EDITOR
        [ReadOnly]
        [SerializeField]
#endif
        private AccelerationZoneFacade zone;

        #endregion

        #region Enable

        private void OnEnable()
        {
            Zone = null;
        }

        #endregion
    }
}