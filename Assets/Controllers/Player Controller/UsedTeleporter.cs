﻿using MetraReborn.Events;
using UnityEngine;

namespace MetraReborn.ScriptableObjects
{
    /// <summary>
    /// A scriptable object that holds a reference to the teleporter that the player is currently using, if any.
    /// </summary>
    [CreateAssetMenu(fileName = "UsedTeleporter", menuName = "Scriptable Objects/Character/Used Teleporter", order = 2)]
    public class UsedTeleporter : ScriptableObject
    {
        #region Properties

        /// <summary>
        /// Teleporter character is currently using.
        /// </summary>
        public TeleporterFacade Teleporter
        {
            get => teleporter;

            set
            {
                teleporter = value;

                if (value != null)
                {
                    onStartedTeleporting.Raise();
                }
                else
                {
                    onStoppedTeleporting.Raise();
                }
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Event that is raised when character starts teleporting.
        /// </summary>
        [Space]
        [Header("Events")]
        [Tooltip("Event that is raised when character starts teleporting.")]
        [SerializeField]
        private GameEvent onStartedTeleporting;

        /// <summary>
        /// Event that is raised when character stops teleporting.
        /// </summary>
        [Tooltip("Event that is raised when character stops teleporting.")]
        [SerializeField]
        private GameEvent onStoppedTeleporting;            

#if UNITY_EDITOR
        [Space]
        [Header("Current")]
        [SerializeField]
        [ReadOnly]
#endif
        private TeleporterFacade teleporter;

        #endregion

        #region Enable

        private void OnEnable() => teleporter = null;

        #endregion
    }
}