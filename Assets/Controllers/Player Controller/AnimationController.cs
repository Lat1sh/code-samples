﻿using MetraReborn.ScriptableObjects;
using System;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Switches playable character's animations based on current input and character state.
    /// </summary>
    public class AnimationController : MonoBehaviour
    {
        #region Fields   

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        [Inject(Id = "HorizontalInput")]
        private FloatVariable horizontalInput;

        /// <summary>
        /// Scriptable object that holds current vertical input.
        /// </summary>
        [Inject(Id = "VerticalInput")]
        private FloatVariable verticalInput;

        /// <summary>
        /// Scriptable object that holds a bollean with character's current groundeed status.
        /// </summary>
        [Inject(Id = "IsGrounded")]
        private BoolVariable isGrounded;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary> 
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Animator component.
        /// </summary>
        private Animator animator;

        /// <summary>
        /// Current animator component's state.
        /// </summary>
        private CharacterAnimatorState currentAnimatorState;

        private CharacterAnimatorState[] animatorStates;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="characterFacade">Scriptable object that serves as a facade for playable character.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="animator">Animator component.</param>
        /// <param name="currentGround">Scriptable object that holds reference to current ground object.</param>
        /// <param name="horizontalInput">Scriptable object that holds current horizontal input.</param>
        /// <param name="verticalInput">Scriptable object that holds current vertical input.</param>
        [Inject]
        public void Construct(CharacterFacade characterFacade, Vector2Variable currentVelocity, Animator animator,
                             CurrentGround currentGround, [Inject(Id="HorizontalInput")]FloatVariable horizontalInput, 
                             [Inject(Id = "VerticalInput")]FloatVariable verticalInput)
        {
            this.currentVelocity = currentVelocity;
            this.animator = animator;

            animatorStates = new CharacterAnimatorState[]
            {
                new DefaultMovementState(animator, characterFacade, currentVelocity, isGrounded, currentGround,
                                         horizontalInput, verticalInput),
                new TeleportingState(animator),
                new ClimbNetState(animator, horizontalInput, verticalInput),
                new DashPadState(animator),
                new AccelerationZoneState(animator),
                new JumpState(animator)
            };

            currentAnimatorState = GetNewState(typeof(DefaultMovementState));
        }

        #endregion

        #region Update

        private void Update() => currentAnimatorState.Update();

        #endregion

        #region Changing state

        /// <summary>
        /// Called when character controller enters <see cref="CharacterMoveControllerType.Default"/>.
        /// </summary>
        public void EnterDefaultState()
        {
            animator.SetBool("IsDashing", false);
            animator.SetBool("IsTeleporting", false);
            currentAnimatorState = GetNewState(typeof(DefaultMovementState));
        }

        /// <summary>
        /// Called when character controller enters <see cref="CharacterMoveControllerType.Teleport"/>.
        /// </summary>
        public void EnterTeleportationState() => currentAnimatorState = GetNewState(typeof(TeleportingState));

        /// <summary>
        /// Called when character controller enters <see cref="CharacterMoveControllerType.Net"/>.
        /// </summary>
        public void EnterClimbNetState() => currentAnimatorState = GetNewState(typeof(ClimbNetState));

        /// <summary>
        /// Called when character controller enters <see cref="CharacterMoveControllerType.DashPad"/>.
        /// </summary>
        public void EnterDashPadState() => currentAnimatorState = GetNewState(typeof(DashPadState));

        /// <summary>
        /// Called when character controller enters <see cref="CharacterMoveControllerType.AccelerationZone"/>.
        /// </summary>
        public void EnterAccelerationZoneState() => currentAnimatorState = GetNewState(typeof(AccelerationZoneState));

        /// <summary>
        /// Called when character jumps.
        /// </summary>
        public void EnterJumpState() => currentAnimatorState = GetNewState(typeof(JumpState));

        /// <summary>
        /// Gets state of specified type from the array. Also enters the state before returning it.
        /// </summary>
        /// <param name="stateType">State type.</param>
        /// <returns>A <see cref="CharacterAnimatorState"/>.</returns>
        private CharacterAnimatorState GetNewState(Type stateType)
        {
            foreach (CharacterAnimatorState state in animatorStates)
            {
                if (state.GetType() == stateType)
                {
                    state.EnterState();
                    return state;
                }
            }

            Debug.LogError("Failed to find state of type " + stateType.ToString());
            return currentAnimatorState;
        }

        #endregion
    }
}