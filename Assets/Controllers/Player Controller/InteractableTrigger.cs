﻿using MetraReborn.ScriptableObjects;
using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Triggers the use of an interactable object the character is overlapping with.
    /// </summary>
    public class InteractableTrigger : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Scriptable object that holds the interactabke item character is currently colliding with.
        /// </summary>
        private CurrentInteractable currentInteractable;

        /// <summary>
        /// Scriptable object that holds the teleporter currently used by the player.
        /// </summary>
        private UsedTeleporter usedTeleporter;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="currentInteractable">Scriptable object that holds the interactabke item character is currently colliding with.</param>
        /// <param name="usedTeleporter">Scriptable object that holds the teleporter currently used by the player.</param>
        [Zenject.Inject]
        public void Construct(CurrentInteractable currentInteractable, UsedTeleporter usedTeleporter)
        {
            this.currentInteractable = currentInteractable;
            this.usedTeleporter = usedTeleporter;
        }

        #endregion

        #region Trigger

        /// <summary>
        /// Uses interactbale object that the player is overlapping with, if any.
        /// </summary>
        public void UseInteractable()
        {
            InteractableFacade currentInteractable = this.currentInteractable.Current;

            if (currentInteractable == null)
            {
                return;
            }

            switch (currentInteractable.Type)
            {
                case InteractableType.Teleporter:
                    var collidingTeleporter = (TeleporterFacade)currentInteractable;

                    if (!collidingTeleporter.CanBeUsed)
                    {
                        return;
                    }

                    if (usedTeleporter.Teleporter != null)
                    {
                        if (usedTeleporter.Teleporter == collidingTeleporter)
                        {
                            return;
                        }
                    }

                    usedTeleporter.Teleporter = collidingTeleporter;
                    break;
                case InteractableType.Activator:
                    var activator = (ActivatorFacade)currentInteractable;
                    activator.TriggerEffect();
                    break;
                default:
                    break;
            }
        }

        #endregion
    }
}