﻿using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Resets current ground object and sets the isGrounded flag to false when OnResetGround even is raised.
    /// </summary>
    public class GroundResetter : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Scriptable object that holds reference to the ground object that the player is currently standing on.
        /// </summary>
        private CurrentGround currentGround;

        /// <summary>
        /// Scriptable object that holds the is grounded flag.
        /// </summary>
        [Inject(Id = "IsGrounded")]
        private BoolVariable isGrounded;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="currentGround">Scriptable object that holds reference to the ground object
        /// that the player is currently standing on.</param>
        [Inject]
        public void Construct(CurrentGround currentGround) => this.currentGround = currentGround;

        #endregion

        #region Reset

        /// <summary>
        /// Resets current ground and the isGrounded flag.
        /// </summary>
        public void Reset()
        {
            currentGround.Current = null;
            isGrounded.Value = false;
        }

        #endregion
    }
}