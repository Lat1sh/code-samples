﻿using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Calculates raycast data to be used by <see cref="CollisionDetector"/>.
    /// </summary>
    public class RaycastDataCalculator
    {
        #region Fields

        /// <summary>
        /// Transform of the top left raycast origin.
        /// </summary>
        [Inject(Id = "TopLeftRaycastOrigin")]
        private Transform topLeftRaycastOrigin;

        /// <summary>
        /// Transform of the top right raycast origin.
        /// </summary>
        [Inject(Id = "TopRightRaycastOrigin")]
        private Transform topRightRaycastOrigin;

        /// <summary>
        /// Transform of the bottom left raycast origin.
        /// </summary>
        [Inject(Id = "BottomLeftRaycastOrigin")]
        private Transform bottomLeftRaycastOrigin;

        /// <summary>
        /// Transform of the bottom right raycast origin.
        /// </summary>
        [Inject(Id = "BottomRightRaycastOrigin")]
        private Transform bottomRightRaycastOrigin;

        /// <summary>
        /// Scriptable object that holds the value for spacing between horizontal raycasts.
        /// </summary>
        [Inject(Id = "HorizontalRaySpacingSO")]
        private FloatVariable horizontalRaySpacingSO;

        /// <summary>
        /// Scriptable object that holds the value for spacing between vertical raycasts.
        /// </summary>
        [Inject(Id = "VerticalRaySpacingSO")]
        private FloatVariable verticalRaySpacingSO;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary>
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Component that serves as a facade for the character object.
        /// </summary>
        private CharacterFacade characterFacade;

        #endregion

        #region Init

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="characterFacade">Component that serves as a facade for the character object.</param>
        public RaycastDataCalculator(Vector2Variable currentVelocity, CharacterFacade characterFacade)
        {
            this.currentVelocity = currentVelocity;
            this.characterFacade = characterFacade;
        }   

        #endregion

        #region Calculate

        /// <summary>
        /// Calculates data to use when raycasting for vertical collisions.
        /// Raises an event to get vertical collisions if velocity is not equal to zero.
        /// </summary>
        /// <returns>
        /// A <see cref="RaycastData"/> object.
        /// </returns>
        public RaycastData CalculateVerticalRaycastData()
        {
            var velocity = 0f;
            var raycastDirection = Vector2.zero;
            var rayOrigin = Vector2.zero;
            var raycastStepDirection = Vector2.zero;
            var raycastSpacing = 0f;

            switch (characterFacade.GravityDirection)
            {
                case GravityDirection.Down:
                    velocity = currentVelocity.Value.y * Time.deltaTime;

                    if (Mathf.Sign(velocity) > 0)
                    {
                        raycastDirection = Vector2.up;
                        rayOrigin = topLeftRaycastOrigin.position;
                    }
                    else
                    {
                        raycastDirection = Vector2.down;
                        rayOrigin = bottomLeftRaycastOrigin.position;
                    }

                    raycastStepDirection = Vector2.right;
                    raycastSpacing = verticalRaySpacingSO.Value;
                    break;
                case GravityDirection.Up:
                    velocity = currentVelocity.Value.y * Time.deltaTime;

                    if (Mathf.Sign(velocity) > 0)
                    {
                        raycastDirection = Vector2.up;
                        rayOrigin = bottomRightRaycastOrigin.position;
                    }
                    else
                    {
                        raycastDirection = Vector2.down;
                        rayOrigin = topRightRaycastOrigin.position;
                    }

                    raycastStepDirection = Vector2.right;
                    raycastSpacing = verticalRaySpacingSO.Value;
                    break;
                case GravityDirection.Left:
                    velocity = currentVelocity.Value.x * Time.deltaTime;

                    if (Mathf.Sign(velocity) > 0)
                    {
                        raycastDirection = Vector2.right;
                        rayOrigin = topLeftRaycastOrigin.position;
                    }
                    else
                    {
                        raycastDirection = Vector2.left;
                        rayOrigin = bottomLeftRaycastOrigin.position;
                    }

                    raycastStepDirection = Vector2.down;
                    raycastSpacing = horizontalRaySpacingSO.Value;
                    break;
                case GravityDirection.Right:
                    velocity = currentVelocity.Value.x * Time.deltaTime;

                    if (Mathf.Sign(velocity) > 0)
                    {
                        raycastDirection = Vector2.right;
                        rayOrigin = bottomLeftRaycastOrigin.position;
                    }
                    else
                    {
                        raycastDirection = Vector2.left;
                        rayOrigin = topLeftRaycastOrigin.position;
                    }

                    raycastStepDirection = Vector2.up;
                    raycastSpacing = horizontalRaySpacingSO.Value;
                    break;
            }

            return new RaycastData(velocity, raycastDirection, rayOrigin, raycastStepDirection, raycastSpacing);
        }

        /// <summary>
        /// Calculates data to use when raycasting for horizontal collisions. 
        /// Raises an event to get horizontal collisions if velocity is not equal to zero.
        /// </summary>
        /// <returns>
        /// A <see cref="RaycastData"/> object.
        /// </returns>
        public RaycastData CalculateHorizontalRaycastData()
        {
            var velocity = 0f;
            var raycastDirection = Vector2.zero;
            var rayOrigin = Vector2.zero;
            var raycastStepDirection = Vector2.zero;
            var raycastSpacing = 0f;

            switch (characterFacade.GravityDirection)
            {
                case GravityDirection.Down:
                    velocity = currentVelocity.Value.x * Time.deltaTime;

                    if (Mathf.Sign(velocity) > 0)
                    {
                        raycastDirection = Vector2.right;
                        rayOrigin = bottomRightRaycastOrigin.position;
                    }
                    else
                    {
                        raycastDirection = Vector2.left;
                        rayOrigin = bottomLeftRaycastOrigin.position;
                    }

                    raycastStepDirection = Vector2.up;
                    raycastSpacing = horizontalRaySpacingSO.Value;
                    break;
                case GravityDirection.Up:
                    velocity = currentVelocity.Value.x * Time.deltaTime;

                    if (Mathf.Sign(velocity) > 0)
                    {
                        raycastDirection = Vector2.right;
                        rayOrigin = topLeftRaycastOrigin.position;
                    }
                    else
                    {
                        raycastDirection = Vector2.left;
                        rayOrigin = topRightRaycastOrigin.position;
                    }

                    raycastStepDirection = Vector2.up;
                    raycastSpacing = horizontalRaySpacingSO.Value;
                    break;
                case GravityDirection.Left:
                    velocity = currentVelocity.Value.y * Time.deltaTime;

                    if (Mathf.Sign(velocity) > 0)
                    {
                        raycastDirection = Vector2.up;
                        rayOrigin = bottomLeftRaycastOrigin.position;
                    }
                    else
                    {
                        raycastDirection = Vector2.down;
                        rayOrigin = bottomRightRaycastOrigin.position;
                    }

                    raycastStepDirection = Vector2.right;
                    raycastSpacing = verticalRaySpacingSO.Value;
                    break;
                case GravityDirection.Right:
                    velocity = currentVelocity.Value.y * Time.deltaTime;

                    if (Mathf.Sign(velocity) > 0)
                    {
                        raycastDirection = Vector2.up;
                        rayOrigin = bottomRightRaycastOrigin.position;
                    }
                    else
                    {
                        raycastDirection = Vector2.down;
                        rayOrigin = bottomLeftRaycastOrigin.position;
                    }

                    raycastStepDirection = Vector2.left;
                    raycastSpacing = verticalRaySpacingSO.Value;
                    break;
            }

            return new RaycastData(velocity, raycastDirection, rayOrigin, raycastStepDirection, raycastSpacing);          
        }

        #endregion
    }
}