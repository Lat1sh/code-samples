﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Detect's main character's collisions.
    /// </summary>
    public class CollisionDetector
    {
        #region Fields

        /// <summary>
        /// Component that serves as a facade object for main character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Character's default collision mask.
        /// </summary>
        [Inject(Id = "CollisionMask")]
        private LayerMask collisionMask;

        /// <summary>
        /// Number of horizontal raycasts for collision calculation.
        /// </summary>
        [Inject(Id = "NumberOfHorizontalRays")]
        private int numberOfHorizontalRays = 4;

        /// <summary>
        /// Number of vertical raycasts for collision calculation.
        /// </summary>
        [Inject(Id = "NumberOfVerticalRays")]
        private int numberOfVerticalRays = 4;

        private List<ICollidable> collidingObjects = new List<ICollidable>();

        #endregion

        #region Constants

        /// <summary>
        /// Offset from the edge of the collider for raycasting. 
        /// Rays start inside the character's collider to be able to detect objects like ground.
        /// </summary>
        private const float SkinWidth = 0.02f;

        #endregion

        #region Constructor

        /// <summary>
        /// Default contructor.
        /// </summary>
        /// <param name="characterFacade">Component that serves as a facade object for main character.</param>
        public CollisionDetector(CharacterFacade characterFacade)
        {
            this.characterFacade = characterFacade;
        }

        #endregion

        #region Collisions

        /// <summary>
        /// Calculates horizontal collisions based on current velocity. Passes detected colliding objects to <see cref="collidingObjectsSO"/>.
        /// </summary>
        /// <param name="raycastData">Rasycast data to use for getting collisions.</param>
        /// A list of <see cref="ICollidable"/>.
        /// </returns>
        public List<ICollidable> GetHorizontalCollisions(ref RaycastData raycastData)
        { 
            float horizontalVelocity = raycastData.Velocity;

            if (horizontalVelocity == 0f)
            {
                return null;
            }

            Vector2 rayOrigin = raycastData.RayOrigin;
            float raycastSpacing = raycastData.RaycastSpacing;
            Vector2 raycastDirection = raycastData.RaycastDirection;
            Vector2 raycastStepDirection = raycastData.RaycastStepDirection;

            collidingObjects.Clear();

            var closestColliders = new List<Collider2D>();
            var minimumHitDistance = 100f;

            for (int rayIndex = 0; rayIndex < numberOfHorizontalRays; rayIndex++)
            {
                RaycastHit2D hit = Raycast(rayOrigin, raycastDirection, Mathf.Abs(horizontalVelocity) + SkinWidth,
                    raycastSpacing, raycastStepDirection, rayIndex);

                if (hit)
                {
                    Collider2D hitCollider = hit.collider;

                    MagicController hitObjectMagicController = hitCollider.GetComponent<MagicController>();

                    if (hitObjectMagicController != null)
                    {
                        if (hitObjectMagicController.IsSpectral != characterFacade.IsSpectral)
                        {
                            continue;
                        }
                    }

                    switch (hit.collider.tag)
                    {
                        case "JumpPad":
                            continue;
                        case "GroundSecondary":
                            hitCollider = hitCollider.transform.parent.Find("DynamicGround").GetComponent<DynamicGroundFacade>().MainCollider;
                            break;                        
                        default:
                            break;
                    }

                    if (hit.distance == minimumHitDistance)
                    {
                        if (!closestColliders.Contains(hitCollider))
                        {
                            closestColliders.Add(hitCollider);
                        }
                    }
                    else if (hit.distance < minimumHitDistance)
                    {
                        closestColliders.Clear();
                        closestColliders.Add(hitCollider);
                        minimumHitDistance = hit.distance;
                    }
                }
            }

            if (closestColliders.Count > 0)
            {
                foreach (Collider2D col in closestColliders)
                {
                    var collidingObject = col.GetComponent<ICollidable>();

                    if (!collidingObjects.Contains(collidingObject))
                    {
                        collidingObjects.Add(collidingObject);
                    }
                }

                raycastData.Velocity = (minimumHitDistance - SkinWidth) * Mathf.Sign(horizontalVelocity);

                return collidingObjects;
            }

            return null;
        }

        /// <summary>
        /// Calculates vertical collisions based on current velocity and gravity direction. Passes detected colliding objects to <see cref="collidingObjectsSO"/>.
        /// </summary>
        /// <param name="raycastData">Rasycast data to use for getting collisions.</param>
        /// A list of <see cref="ICollidable"/>.
        /// </returns>
        public List<ICollidable> GetVerticalCollisions(ref RaycastData raycastData)
        {
            float verticalVelocity = raycastData.Velocity;

            if (verticalVelocity == 0f)
            {
                return null;
            }

            Vector2 rayOrigin = raycastData.RayOrigin;
            float raycastSpacing = raycastData.RaycastSpacing;
            Vector2 raycastDirection = raycastData.RaycastDirection;
            Vector2 raycastStepDirection = raycastData.RaycastStepDirection;

            collidingObjects.Clear();

            var closestColliders = new List<Collider2D>();
            var minimumHitDistance = 100f;

            for (int rayIndex = 0; rayIndex < numberOfVerticalRays; rayIndex++)
            {
                RaycastHit2D hit = Raycast(rayOrigin, raycastDirection, Mathf.Abs(verticalVelocity) + SkinWidth, 
                                           raycastSpacing, raycastStepDirection, rayIndex);

                if (hit)
                {
                    Collider2D hitCollider = hit.collider;

                    MagicController hitObjectMagicController = hitCollider.GetComponent<MagicController>();

                    if (hitObjectMagicController != null)
                    {
                        if (hitObjectMagicController.IsSpectral != characterFacade.IsSpectral)
                        {
                            continue;
                        }
                    }

                    switch (hitCollider.tag)
                    {
                        case "GroundSecondary":
                            hitCollider = hitCollider.transform.parent.Find("DynamicGround").GetComponent<DynamicGroundFacade>().MainCollider;
                            break;
                        default:
                            break;
                    }

                    if (hit.distance == minimumHitDistance)
                    {
                        if (!closestColliders.Contains(hitCollider))
                        {
                            closestColliders.Add(hitCollider);
                        }
                    }
                    else if (hit.distance < minimumHitDistance)
                    {
                        closestColliders.Clear();
                        closestColliders.Add(hitCollider);
                        minimumHitDistance = hit.distance;
                    }
                }
            }

            if (closestColliders.Count > 0)
            {
                foreach (Collider2D col in closestColliders)
                {
                    var collidingObject = col.GetComponent<ICollidable>();
                  
                    if (!collidingObjects.Contains(collidingObject))
                    {
                        collidingObjects.Add(collidingObject);
                    }
                }

                raycastData.Velocity = (minimumHitDistance - SkinWidth) * Mathf.Sign(verticalVelocity);                

                return collidingObjects;
            }

            return null;            
        }

        /// <summary>
        /// Casts a ray based on provided parameters and returns the result.
        /// </summary>
        /// <param name="rayOriginCorner">Origin corner for raycast.</param>
        /// <param name="direction">Direction of raycast.</param>
        /// <param name="rayLength">Raycast length.</param>
        /// <param name="raycastSpacing">Spce between adjacent raycasts.</param>
        /// <param name="raycastStepDirection">Direction in which to offset raycast origin.</param>
        /// <param name="rayIndex">Index of the ray.</param>
        /// <returns>A RaycastHit2D.</returns>
        private RaycastHit2D Raycast(Vector2 rayOriginCorner, Vector2 direction, float rayLength, float raycastSpacing,
                                     Vector2 raycastStepDirection, int rayIndex)
        {
            rayOriginCorner += raycastStepDirection * (raycastSpacing * rayIndex);
            Debug.DrawRay(rayOriginCorner, direction * rayLength, Color.red);
            return Physics2D.Raycast(rayOriginCorner, direction, rayLength, collisionMask);
        }

        #endregion
    }
}