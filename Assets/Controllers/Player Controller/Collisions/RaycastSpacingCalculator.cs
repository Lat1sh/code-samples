﻿using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Calculates raycast settings based on user-set parameters.
    /// </summary>
    public class RaycastSpacingCalculator : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Scriptable object that holds the value for spacing between horizontal raycasts.
        /// </summary>
        [Inject(Id = "HorizontalRaySpacingSO")]
        private FloatVariable horizontalRaySpacingSO;

        /// <summary>
        /// Scriptable object that holds the value for spacing between vertical raycasts.
        /// </summary>
        [Inject(Id = "VerticalRaySpacingSO")]
        private FloatVariable verticalRaySpacingSO;

        /// <summary>
        /// Number of horizontal raycasts for collision calculation.
        /// </summary>
        [Inject(Id = "NumberOfHorizontalRays")]
        private int numberOfHorizontalRays = 4;

        /// <summary>
        /// Number of vertical raycasts for collision calculation.
        /// </summary>
        [Inject(Id = "NumberOfVerticalRays")]
        private int numberOfVerticalRays = 4;

        #endregion

        #region Constants

        /// <summary>
        /// Offset from the edge of the collider for raycasting. 
        /// Rays start inside the character's collider to be able to detect objects like ground.
        /// </summary>
        private const float SkinWidth = 0.02f;

        #endregion

        #region Init

        private void Start()
        {
            Bounds bounds = GetComponent<BoxCollider2D>().bounds;
            bounds.Expand(SkinWidth * -2);

            numberOfHorizontalRays = Mathf.Clamp(numberOfHorizontalRays, 2, int.MaxValue);
            numberOfVerticalRays = Mathf.Clamp(numberOfVerticalRays, 2, int.MaxValue);

            horizontalRaySpacingSO.Value = bounds.size.y / (numberOfHorizontalRays - 1);
            verticalRaySpacingSO.Value = bounds.size.x / (numberOfVerticalRays - 1);
        }

        #endregion
    }
}