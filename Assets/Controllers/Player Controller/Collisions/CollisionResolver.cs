﻿using MetraReborn.Events;
using MetraReborn.ScriptableObjects;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Resolves character's collisions after colliding objects are detecetd by <see cref="CollisionDetector"/>.
    /// </summary>
    public class CollisionResolver
    {
        #region Fields

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        [Inject(Id = "HorizontalInput")]
        private FloatVariable horizontalInput;

        /// <summary>
        /// Scriptable object that holds current vertical input.
        /// </summary>
        [Inject(Id = "VerticalInput")]
        private FloatVariable verticalInput;

        /// <summary>
        /// Component that serves as a facade for main character.
        /// </summary>
        private CharacterFacade characterFacade;

        /// <summary>
        /// Scriptable object that holds reference to the ground object that the player is currently standing on.
        /// </summary>
        private CurrentGround currentGround;

        /// <summary>
        /// Scriptable object that holds the is grounded flag.
        /// </summary>
        [Inject(Id = "IsGrounded")]
        private BoolVariable isGrounded;

        /// <summary>
        /// Scriptable object that holds character's current velocity.
        /// </summary>  
        private Vector2Variable currentVelocity;

        /// <summary>
        /// Scriptable object that holds the dash pad currently used by the player.
        /// </summary>
        private UsedDashPad usedDashPad;

        /// <summary>
        /// Scriptable object thgat holds the acceleration zone that the player is currently using.
        /// </summary>
        private UsedAccelerationZone usedAccelerationZone;

        /// <summary>
        /// Scriptable object that holds the jump pad that the player just used.
        /// </summary>
        private UsedJumpPad usedJumpPad;

        /// <summary>
        /// Scriptable object that holds the ground character is attached to in alternative form.
        /// </summary>
        private AttachedGround attachedGround;

        /// <summary>
        /// Event that is raised when character needs to reset their jump state.
        /// </summary>
        [Inject(Id = "OnResetJumpState")]
        private GameEvent onResetJumpState;

        /// <summary>
        /// Event that is raised when current ground need to be set to null and grounded flag to false.
        /// </summary>
        [Inject(Id = "OnResetGround")]
        private GameEvent onResetGround;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="currentGround">Scriptable object that holds reference to the ground object
        /// that the player is currently standing on.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="usedDashPad">Scriptable object that holds the dash pad currently used by the player.</param>
        /// <param name="usedAccelerationZone">Scriptable object thgat holds the acceleration zone that the player is currently using.</param>
        /// <param name="usedJumpPad">Scriptable object that holds the jump pad that the player just used.</param>
        /// <param name="characterFacade">Component that serves as a facade for main character.</param>
        /// <param name="attachedGround">Scriptable object that holds the ground character is attached to in alternative form.</param>
        public CollisionResolver(CurrentGround currentGround, Vector2Variable currentVelocity,
                                 UsedDashPad usedDashPad, UsedAccelerationZone usedAccelerationZone,
                                 UsedJumpPad usedJumpPad, CharacterFacade characterFacade,
                                 AttachedGround attachedGround)
        {            
            this.currentGround = currentGround;
            this.currentVelocity = currentVelocity;
            this.usedDashPad = usedDashPad;
            this.usedAccelerationZone = usedAccelerationZone;
            this.usedJumpPad = usedJumpPad;
            this.characterFacade = characterFacade;
            this.attachedGround = attachedGround;
        }

        #endregion

        #region Resolve collisions

        /// <summary>
        /// Resolves current frame's collisions.
        /// </summary>
        /// <param name="resolvingVertical">Have colliding objects been detected by vertical or horizontal collision method?</param>
        /// <param name="collidingObjects">Objects character is colliding with.</param>
        /// <param name="reducedVelocity">Value of horizontal or vertical component of velocity in case it needs
        /// to be reduced because of a collision.</param>
        public void ResolveCollisions(bool resolvingVertical, List<ICollidable> collidingObjects, float reducedVelocity)
        {
            if (collidingObjects == null)
            {
                if (resolvingVertical)
                {
                    onResetGround.Raise();
                }

                return;
            }

            var endResolution = false;
            var alreadyReducedVelocity = false;
            var collidedWithGround = false;

            foreach (ICollidable collidingObject in collidingObjects)
            {
                switch (collidingObject.Tag)
                {
                    case "Ground":
                    case "ConveyorGround":
                        if (resolvingVertical)
                        {
                            Vector2 currentVelocity = this.currentVelocity.Value;

                            if ((currentVelocity.y < 0f && characterFacade.GravityDirection == GravityDirection.Down) ||
                                (currentVelocity.y > 0f && characterFacade.GravityDirection == GravityDirection.Up) ||
                                (currentVelocity.x < 0f && characterFacade.GravityDirection == GravityDirection.Left) ||
                                (currentVelocity.x > 0f && characterFacade.GravityDirection == GravityDirection.Right))
                            {
                                onResetJumpState.Raise();
                                isGrounded.Value = collidedWithGround = true;
                                currentGround.Current = (IGround)collidingObject;
                            }
                        }
                        else
                        {
                            if (characterFacade.CharacterForm == CharacterForm.Alternative && collidingObject.Tag == "Ground")
                            {
                                float directionSign = Mathf.Sign(reducedVelocity);

                                switch (characterFacade.GravityDirection)
                                {
                                    case GravityDirection.Down:
                                    case GravityDirection.Up:
                                        if (directionSign > 0f && horizontalInput.Value > 0f)
                                        {
                                            attachedGround.Data = new AttachedGroundData((IGround)collidingObject, AttachedGroundDirection.Right);
                                        }
                                        else if (directionSign < 0f && horizontalInput.Value < 0f)
                                        {
                                            attachedGround.Data = new AttachedGroundData((IGround)collidingObject, AttachedGroundDirection.Left);
                                        }
                                        break;
                                    case GravityDirection.Left:
                                    case GravityDirection.Right:
                                        if (directionSign > 0f && verticalInput.Value > 0f)
                                        {
                                            attachedGround.Data = new AttachedGroundData((IGround)collidingObject, AttachedGroundDirection.Above);
                                        }
                                        else if (directionSign < 0f && verticalInput.Value < 0f)
                                        {
                                            attachedGround.Data = new AttachedGroundData((IGround)collidingObject, AttachedGroundDirection.Below);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        break;
                    case "JumpPad":
                        if (resolvingVertical)
                        {
                            var pad = (JumpPadFacade)collidingObject;

                            GravityDirection gravityDirection = this.characterFacade.GravityDirection;
                            GravityDirection padGravityDirection = pad.GetComponent<MagicController>().GravityDirection;

                            if ((gravityDirection == GravityDirection.Up || gravityDirection == GravityDirection.Down) &&
                                (padGravityDirection != GravityDirection.Up && padGravityDirection != GravityDirection.Down))
                            {
                                return;
                            }

                            if ((gravityDirection == GravityDirection.Left || gravityDirection == GravityDirection.Right) &&
                                (padGravityDirection != GravityDirection.Left && padGravityDirection != GravityDirection.Right))
                            {
                                return;
                            }

                            usedJumpPad.Pad = pad;
                            endResolution = true;
                        }
                        break;
                    case "DashPad":
                        if (usedDashPad.DashPad != null)
                        {
                            continue;
                        }

                        var dashPad = (DashPadFacade)collidingObject;
                        usedDashPad.DashPad = dashPad;
                        break;
                    case "AccelerationZone":
                        usedAccelerationZone.Zone = (AccelerationZoneFacade)collidingObject;
                        break;
                    case "Trap":
                    case "Projectile":
                        Debug.Log("OUCH!");
                        break;
                    default:
                        break;
                }

                if (resolvingVertical)
                {
                    if (collidingObject.StopWhenCollidingWithThisObject && !alreadyReducedVelocity)
                    {
                        currentVelocity.Value = new Vector2(currentVelocity.Value.x, reducedVelocity / Time.deltaTime);
                        alreadyReducedVelocity = true;
                    }
                }
                else
                {
                    if (collidingObject.StopWhenCollidingWithThisObject && !alreadyReducedVelocity)
                    {                    
                        currentVelocity.Value = new Vector2(reducedVelocity / Time.deltaTime, currentVelocity.Value.y);
                        alreadyReducedVelocity = true;
                    }
                }

                if (endResolution)
                {
                    break;
                }
            }

            if (resolvingVertical)
            {
                if (!collidedWithGround)
                {
                    onResetGround.Raise();
                }
            }           
        }

        #endregion
    }
}