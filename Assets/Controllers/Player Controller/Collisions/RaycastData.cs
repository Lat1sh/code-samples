﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// A struct that holds raycast data used by <see cref="CollisionDetector"/>.
    /// </summary>
#if UNITY_EDITOR
    [System.Serializable]
#endif
    public struct RaycastData
    {
        #region Properties

        /// <summary>
        /// Velocity (vertical or horizontal component, depending on context) to use when raycasting.
        /// </summary>
        public float Velocity { get; set; }

        /// <summary>
        /// Direction to use for raycasting.
        /// </summary>
        public Vector2 RaycastDirection { get; }

        /// <summary>
        /// Position to use as origin for raycasting.
        /// </summary>
        public Vector2 RayOrigin { get; }

        /// <summary>
        /// Direction to offset raycast origin.
        /// </summary>
        public Vector2 RaycastStepDirection { get; }

        /// <summary>
        /// Space between raycasts.
        /// </summary>
        public float RaycastSpacing { get; }

        #endregion

        #region Fields

#if UNITY_EDITOR

        [SerializeField]
        private float velocity;
        [SerializeField]
        private Vector2 raycastDirection;
        [SerializeField]
        private Vector2 rayOrigin;
        [SerializeField]
        private Vector2 raycastStepDirection;
        [SerializeField]
        private float raycastSpacing;
#endif

#endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="velocity">Vertical velocity to use when raycasting.</param>
        /// <param name="raycastDirection">Direction to use for raycasting.</param>
        /// <param name="rayOrigin">Position to use as origin for raycasting.</param>
        /// <param name="raycastStepDirection"> Direction to offset raycast origin.</param>
        /// <param name="raycastSpacing">Space between raycasts.</param>     
        public RaycastData (float velocity, Vector2 raycastDirection, Vector2 rayOrigin, 
                            Vector2 raycastStepDirection, float raycastSpacing)
        {
            Velocity = velocity;
            RaycastDirection = raycastDirection;
            RayOrigin = rayOrigin;
            RaycastStepDirection = raycastStepDirection;
            RaycastSpacing = raycastSpacing;

#if UNITY_EDITOR
            this.velocity = velocity;
            this.raycastDirection = raycastDirection;
            this.rayOrigin = rayOrigin;
            this.raycastStepDirection = raycastStepDirection;
            this.raycastSpacing = raycastSpacing;
#endif
        }

        #endregion
    }
}