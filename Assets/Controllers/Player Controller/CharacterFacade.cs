﻿using MetraReborn.ScriptableObjects;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Component that serves as a facade for the playable character game object.
    /// </summary>
    public class CharacterFacade : MonoBehaviour, ICollidable, IGroundable, IMagicTarget, IPolymorphableObject
    {
        #region Properties

        /// <summary>
        /// Character's tag.
        /// </summary>
        /// <value>
        /// Gets root game object's tag.
        /// </value>
        public string Tag => tag;

        /// <summary>
        /// Character's root game object.
        /// </summary>
        /// <value>
        /// Gets the value of gameObject property.
        /// </value>
        public GameObject GameObject => gameObject;

        /// <summary>
        /// Should other collidable objects stop when colliding with this object?
        /// </summary>
        /// <value>
        /// Gets a boolean.
        /// </value>
        public bool StopWhenCollidingWithThisObject => false;

        /// <summary>
        /// Current ground the character is standing on.
        /// </summary>
        /// <value>
        /// Gets and sets an <see cref="IGround"/> object.
        /// </value>
        public IGround CurrentGround => currentGround.Current;

        /// <summary>
        /// Current direction of gravity for this character.
        /// </summary>
        /// <value>
        /// Gets the value of the <see cref="MagicController.GravityDirection"/> property on the <see cref="MagicController"/>.
        /// </value>
        public GravityDirection GravityDirection => MagicController.GravityDirection;

        /// <summary>
        /// Is this object's transform currently locked? True when object is controlled by something else like a ground object that is rotating.
        /// When unlocking transform, also sets it rotation to avoid issues after parenting/unparenting.
        /// </summary>
        /// <value>
        /// Gets and sets the value of the boolean field lockTransform.
        /// </value>        
        public bool LockTransform
        {
            get => lockTransform;

            set
            {
                lockTransform = value;

                if (!value)
                {
                    switch (GravityDirection)
                    {
                        case GravityDirection.Down:
                            transform.localRotation = Quaternion.Euler(Vector3.zero);
                            break;
                        case GravityDirection.Up:
                            transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 180f));
                            break;
                        case GravityDirection.Left:
                            transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, -90f));
                            break;
                        case GravityDirection.Right:
                            transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    if (GravityDirection == GravityDirection.Down || GravityDirection == GravityDirection.Up)
                    {
                        CurrentVelocity.Value = new Vector2(CurrentVelocity.Value.x, 0f);
                    }
                    else
                    {
                        CurrentVelocity.Value = new Vector2(0f, CurrentVelocity.Value.y);
                    }
                }
            }
        }

        /// <summary>
        /// Is character currently grounded?
        /// </summary>
        /// <value>
        /// Gets the value of the scriptable object that holds the grounde flag.
        /// </value>
        public bool IsGrounded => isGrounded.Value;

        /// <summary>
        /// Is character currently facing right?
        /// </summary>
        /// <value>
        /// Gets the value of <see cref="CharacterFlipController.IsFacingRight"/> 
        /// property on <see cref="flipController"/>.
        /// </value>
        public bool IsFacingRight => flipController.IsFacingRight;

        /// <summary>
        /// Transform to use for attaching to a ground object that is applying
        /// gravity magic to this object. For objects that are polymorphable,
        /// root transform has to be used so that the object that represents
        /// alternate polymorph form can be rotated too.
        /// </summary>
        public Transform GroundRotationPivot => transform;

        /// <summary>
        /// Character's current non-scaled velocity.
        /// </summary>
        /// <value>
        /// Gets the value of the Vector2 field currentVelocity.
        /// </value>
        public Vector2Variable CurrentVelocity { get; private set; }

        /// <summary>
        /// Component that controls outline drawing.
        /// </summary>
        public OutlineController OutlineController { get; private set; }

        /// <summary>
        /// Component that handles magic effects.
        /// </summary>
        public MagicController MagicController { get; private set; }

        /// <summary>
        /// Is this interactable in spectral state?
        /// </summary>
        /// <value>
        /// Gets the value of the <see cref="IsSpectral"/> property on the <see cref="MagicController"/>.
        /// </value>
        public bool IsSpectral => MagicController.IsSpectral;

        /// <summary>
        /// Character's form.
        /// </summary>
        public CharacterForm CharacterForm { get; private set; }

        /// <summary>
        /// Polymorphable object's type.
        /// </summary>
        public PolymorphableObjectType PolymorphableObjectType => PolymorphableObjectType.Player;

        #endregion

        #region Fields

        /// <summary>
        /// Scriptable object that holds the is grounded flag.
        /// </summary>
        [Inject(Id = "IsGrounded")]
        private BoolVariable isGrounded;

        /// <summary>
        /// Scriptable object that holds reference to the ground object that the player is currently standing on.
        /// </summary>
        private CurrentGround currentGround;

        private bool lockTransform = false;

        /// <summary>
        /// Component that flips character object.
        /// </summary>
        private FlipController flipController;

        #endregion     

        #region Init

        /// <summary>
        /// Constructs this object by injecting the dependencies.
        /// </summary>
        /// <param name="magicController">Component that handles magic effects.</param>
        /// <param name="flipController">Component that flips character object.</param>
        /// <param name="currentGround">Scriptable object that holds reference to the ground object 
        /// that the player is currently standing on.</param>
        /// <param name="currentVelocity">Scriptable object that holds character's current velocity.</param>
        /// <param name="outlineController">Component that handles showing/hiding and changing the color 
        /// of an object's outline.</param>
        /// <param name="characterForm">Character's form.</param>
        [Inject]
        public void Construct(MagicController magicController, FlipController flipController,
                              CurrentGround currentGround, Vector2Variable currentVelocity,
                              OutlineController outlineController, CharacterForm characterForm)
        {
            MagicController = magicController;
            this.flipController = flipController;
            this.currentGround = currentGround;
            CurrentVelocity = currentVelocity;
            OutlineController = outlineController;
            CharacterForm = characterForm;
        }    

        #endregion

        #region Components

        /// <summary>
        /// Gets a component of specified type. Required to be able to get components 
        /// from objects that are referenced as <see cref="ICollidable"/> 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Component GetCollidableComponent(System.Type type) => GetComponent(type);

        #endregion

        #region Magic

        /// <summary>
        /// Tells the <see cref="MagicController"/> to use gravity magic.
        /// </summary>
        public void UseGravityMagic() => MagicController.UseGravityMagic();

        /// <summary>
        /// Tells the <see cref="MagicController"/> to use spectral magic.
        /// </summary>
        public void UseSpectralMagic() => MagicController.UseSpectralMagic();

        /// <summary>
        /// Tells the <see cref="MagicController"/> to use polymorph magic.
        /// </summary>
        public void UsePolymorphMagic() => MagicController.UsePolymorphMagic();

        /// <summary>
        /// Tells the <see cref="MagicController"/> to use immunity magic.
        /// </summary>
        public void UseImmunityMagic() => MagicController.UseImmunityMagic();

        /// <summary>
        /// Initializes character's state when it is polymorphed.
        /// </summary>
        /// <param name="polymorphableState">Other character form's state on polymorph.</param>
        public void InitPolymorphableState(PolymorphableState polymorphableState)
        {
            if (((CharacterPolymorphableState)polymorphableState).WasFacingRight != flipController.IsFacingRight)
            {
                flipController.FlipObject();
            }

            MagicController.InitState(polymorphableState.MagicControllerState);
        }

        /// <summary>
        /// Gets polymorphable state when character is about to polymorph.
        /// </summary>
        /// <returns>
        /// A <see cref="CharacterPolymorphableState"/>.
        /// </returns>
        public PolymorphableState GetPolymorphableState()
        {
            return new CharacterPolymorphableState(flipController.IsFacingRight,
                       new MagicControllerState(GravityDirection, IsSpectral, MagicController.IsImmuneToMagic));
        }

        #endregion

        #region Highlight

        /// <summary>
        /// Shows a highlight around the character when  a certain kind of magic effect targets them.
        /// </summary>
        /// <param name="spellType">Type of the outline to show.</param>
        public void Highlight(SpellType spellType)
        {
            if (MagicController.IsImmuneToMagic)
            {
                return;
            }

            OutlineController.ShowOutline(spellType);
        }

        /// <summary>
        /// Removes highlight from the character.
        /// </summary>
        public void RemoveHighlight()
        {
            if (MagicController.IsImmuneToMagic)
            {
                return;
            }

            OutlineController.HideOutline();
        }

        #endregion        
    }
}