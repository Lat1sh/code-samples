﻿using MetraReborn.Events;
using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// A scriptable object that holds a reference to the dash pad that the player is currently using, if any.
    /// </summary>
    [CreateAssetMenu(fileName = "UsedDashPad", menuName = "Scriptable Objects/Character/Used Dash Pad", order = 3)]
    public class UsedDashPad : ScriptableObject
    {        
        #region Properties

        /// <summary>
        /// Dash pad character is currently using.
        /// </summary>
        public DashPadFacade DashPad
        {
            get => dashPad;

            set
            {
                dashPad = value;

                if (value != null)
                {
                    onStartedUsingDashPad.Raise();
                }
                else
                {
                    onStoppedUsingDashPad.Raise();
                }
            }
        }

        #endregion

        #region Fields

        /// <summary>
        /// Event that is raised when player starts using a dash pad.
        /// </summary>
        [Tooltip("Event that is raised when player starts using a dash pad.")]
        [SerializeField]
        private GameEvent onStartedUsingDashPad;

        /// <summary>
        /// Event that is raised when player stops using a dash pad.
        /// </summary>
        [Tooltip("Event that is raised when player stops using a dash pad.")]
        [SerializeField]
        private GameEvent onStoppedUsingDashPad;

#if UNITY_EDITOR
        [SerializeField]
        [ReadOnly]
#endif
        private DashPadFacade dashPad;

        #endregion

        #region Enable

        private void OnEnable()
        {
            DashPad = null;
        }

        #endregion
    }
}