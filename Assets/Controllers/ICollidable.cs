﻿using MetraReborn.ScriptableObjects;
using System;
using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Interface for all objects that can collide with each other: player, monsters, projectiles, traps, collectibles, etc.
    /// </summary>
    public interface ICollidable
    {
        string Tag { get; }
        GameObject GameObject { get; }
        bool StopWhenCollidingWithThisObject { get; }
        
        bool LockTransform { get; set; }

        Component GetCollidableComponent(Type type);
    }
}