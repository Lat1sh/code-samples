﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Rotates a transform clockwise or counterclockwise.
    /// </summary>
    public class TransformRotator
    {
        #region Rotate

        /// <summary>
        /// Rotates a transform based on passed conditions.
        /// </summary>
        /// <param name="transformToRotate">Transform to rotate.</param>
        /// <param name="rotateClockwise">Should transform be rotated clockwise?</param>
        /// <param name="rotateAroundSpriteCenter">Should sprite center be used instead of game object pivot for rotation?</param>
        /// <param name="spriteRenderer">Sprite renderer to use as rotation pivot in case <paramref name="rotateAroundSpriteCenter"/> is true.</param>
        public void Rotate(Transform transformToRotate, bool rotateClockwise, Vector3 customPivot = new Vector3())
        {
            float rotationAngle = rotateClockwise ? 90f : -90f;

            if (customPivot != Vector3.zero)
            {
                transformToRotate.RotateAround(customPivot, Vector3.forward, -rotationAngle);
            }
            else
            {
                transformToRotate.Rotate(Vector3.forward, rotationAngle);
            }
        }

        #endregion
    }
}