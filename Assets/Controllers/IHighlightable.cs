﻿namespace MetraReborn
{
    /// <summary>
    /// Interface for objects that can be highlighted when they are targeted by magic effects.
    /// </summary>
    public interface IHighlightable
    {
        OutlineController OutlineController { get; }
        void Highlight(SpellType outlineType);
        void RemoveHighlight();
    }
}