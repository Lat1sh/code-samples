﻿using GoogleMobileAds.Api;
using UnityEngine;

namespace CodeSamples.Monetization
{
    /// <summary>
    /// Shows an add banner at the bottom of the screen. Disables it if the player purchases the NoAds non-consumable.
    /// </summary>
    public class BannerAd : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Reference to the object that represents the banner view.
        /// </summary>
        private BannerView bannerView;

        #endregion

        #region Constants

        /// <summary>
        /// Banner ad unit ID in AdMob dashboard.
        /// </summary>
        private const string BannerAdUnitID = "ca-app-pub-redacted:(";

        /// <summary>
        /// Test ad unit ID to be used for banner ads.
        /// </summary>
        private const string BannerAdUnitTestID = "ca-app-pub-redacted:(";

        #endregion

        #region Init

        /// <summary>
        /// Initializes the banner after the purchasing system is initialized.
        /// </summary>
        public void InitializeBanner()
        {
            bannerView = CreateBannerView();
            LoadAd(bannerView);
        }

        #endregion

        #region Disable

        /// <summary>
        /// Callback for when the banner ad is closed.
        /// </summary>
        private void OnBannerClosed() => Disable();

        /// <summary>
        /// Creates a new banner view, sets up its callbacks and requests an ad to be loaded.
        /// </summary>
        /// <returns>
        /// A banner view.
        /// </returns>
        private BannerView CreateBannerView()
        {
            BannerView bannerView = new BannerView(BannerAdUnitTestID, AdSize.Banner, AdPosition.Bottom);

            bannerView.OnAdLoaded += BannerView_OnAdLoaded;
            bannerView.OnAdClosed += OnBannerClosed;
            bannerView.OnAdLeavingApplication += OnBannerClosed;
            bannerView.OnAdFailedToLoad += OnBannerFailedToLoad;

            return bannerView;
        }

        /// <summary>
        /// Loads an ad into the created banner view.
        /// </summary>
        /// <param name="bannerView">Banner view to use to load the ad.</param>
        private void LoadAd(BannerView bannerView)
        {
            AdRequest request = new AdRequest.Builder().Build();
            bannerView.LoadAd(request);
        }

        private void BannerView_OnAdLoaded(object sender, System.EventArgs e)
        {
            Debug.Log("Created banner view");
        }

        /// <summary>
        /// Callback for when banner fails to load. Logs an error message and disables the banner.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBannerFailedToLoad(object sender, AdFailedToLoadEventArgs e)
        {
            Debug.LogError("Failed to load banner ad! Reason: " + e.Message);
            Disable();
        }

        /// <summary>
        /// Callback for when the banner view is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBannerClosed(object sender, System.EventArgs e)
        {
            Disable();
            InitializeBanner();
        }

        /// <summary>
        /// Hides the banner when NoAdsPurchased event is raised.
        /// </summary>
        public void Disable()
        {
            if (bannerView != null)
            {
                bannerView.Destroy();
            }            
        }

        #endregion

    }
}