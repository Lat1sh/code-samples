﻿using GoogleMobileAds.Api;
using CodeSamples.Events;
using System.Collections;
using UnityEngine;

namespace CodeSamples.Monetization
{
    /// <summary>
    /// Shows an interstitial skippable video when a certain number of missions is played.
    /// </summary>
    public class InterstitialAdManager : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Event that is raised when player presses the claim bounty button.
        /// </summary>
        [Tooltip("Event that is raised when player presses the claim bounty button.")]
        [SerializeField]
        private GameEvent onBountyClaimed;

        /// <summary>
        /// Object that holds ad playback state.
        /// </summary>
        private AdShowState adShowState;

        /// <summary>
        /// Reference to the object that holds the interstitial add.
        /// </summary>
        private InterstitialAd interstitialAd;

        #endregion

        #region Constants

        /// <summary>
        /// Interstitial ad unity ID in AdMob dashboard.
        /// </summary>
        private const string InterstitialAdUnitID = "ca-app-pub-redacted:(";

        /// <summary>
        /// Test ad unit ID for interstitial ads.
        /// </summary>
        private const string InterstitialAdUnitTestID = "ca-app-pub-redacted:(";

        #endregion

        #region Init

        private void Start()
        {
            interstitialAd = CreateInterstitial();
            LoadAd(interstitialAd);
        }

        #endregion

        #region Ad

        /// <summary>
        /// Shows an intestitial ad. Uses a coroutine to make sure the add is ready.
        /// </summary>
        public void ShowAd()
        {
            PlayerPrefs.Save();

            if (adShowState.AdShowResult == AdShowResult.Error)
            {
                HandleAdShowResult();
                return;
            }

            StartCoroutine(WaitForAdResult());
            StartCoroutine(ShowAdWhenReady());
        }

        /// <summary>
        /// Creates a new interstitial, assigns its callbacks and requests an add to be loaded.
        /// </summary>
        /// <returns>
        /// An intersitial add.
        /// </returns>
        private InterstitialAd CreateInterstitial()
        {
            adShowState = new AdShowState();

            var interstitial = new InterstitialAd(InterstitialAdUnitTestID);

            interstitial.OnAdLoaded += OnAddLoaded;
            interstitial.OnAdFailedToLoad += OnAdFailedToLoad;
            interstitial.OnAdOpening += OnAddStarted;
            interstitial.OnAdClosed += OnAdFinished;
            interstitial.OnAdLeavingApplication += OnClickedInterstitialAd; 

            return interstitial;
        }

        /// <summary>
        /// Loads an ad into the passed interstitial object.
        /// </summary>
        /// <param name="interstitial">Interstitial ad object.</param>
        private void LoadAd(InterstitialAd interstitial)
        {
            AdRequest request = new AdRequest.Builder().Build();
            interstitial.LoadAd(request);
        }

        /// <summary>
        /// Callback for when the ad is successfully loaded. 
        /// Sets the <see cref="AdShowState.AdShowResult"/> to <see cref="AdShowResult.Loaded"/> on <see cref="adShowState"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAddLoaded(object sender, System.EventArgs e)
        {
            adShowState.AdShowResult = AdShowResult.Loaded;
        }

        /// <summary>
        /// Callback for when the interstitial ad failes to load. Loads a message to the console
        /// and sets the value of the <see cref="AdShowState.AdShowResult"/> property on
        /// <see cref="adShowState"/> object to <see cref="AdShowResult.Error"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
        {
            adShowState.AdShowResult = AdShowResult.Error;
        }

        /// <summary>
        /// Callback for when interstitial ad starts. Sets the <see cref="AdShowState.AdShowResult"/> to <see cref="AdShowResult.Playing"/>
        /// on <see cref="adShowState"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAddStarted(object sender, System.EventArgs e)
        {
            adShowState.AdShowResult = AdShowResult.Playing;
        }

        /// <summary>
        /// Callback for when the ad finishes either by skipping or reaching the end of the video.
        /// Sets the <see cref="AdShowState.AdShowResult"/> to <see cref="AdShowResult.Finished"/> on <see cref="adShowState"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAdFinished(object sender, System.EventArgs e)
        {
            adShowState.AdShowResult = AdShowResult.Finished;
        }

        /// <summary>
        /// Callback for when user clicks the ad and brings the app to the background.
        /// Sets the <see cref="AdShowState.AdShowResult"/> to <see cref="AdShowResult.Finished"/> on <see cref="adShowState"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickedInterstitialAd(object sender, System.EventArgs e)
        {
            adShowState.AdShowResult = AdShowResult.Finished;
        }

        /// <summary>
        /// Coroutine that waits until unity ad playback finishes or is interrupred.
        /// Required because ads seem to play on a worker thread that can't acces Unity API.
        /// </summary>
        /// <returns>
        /// An IEnumerator.
        /// </returns>
        private IEnumerator WaitForAdResult()
        {
            while (!adShowState.HasFinished)
            {
                yield return null;
            }
            
            HandleAdShowResult();
        }

        /// <summary>
        /// Shows an add when it is ready.
        /// </summary>
        /// <returns>
        /// An IEnumerator.
        /// </returns>
        private IEnumerator ShowAdWhenReady()
        {
            while (!interstitialAd.IsLoaded())
            {
                yield return null;
            }
         
            interstitialAd.Show();
        }

        /// <summary>
        /// Raises the OnBountyClaimed event when ad finishes, is skipped or throws an error.
        /// </summary>
        private void HandleAdShowResult()
        {
            onBountyClaimed.Raise();

            interstitialAd.Destroy();
            interstitialAd = CreateInterstitial();
            LoadAd(interstitialAd);
        }

        #endregion
    }
}