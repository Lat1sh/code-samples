﻿namespace CodeSamples.Monetization
{
    /// <summary>
    /// Result of playing an ad.
    /// </summary>
    public enum AdShowResult
    {        
        Loading,
        Loaded,
        Playing,
        Finished,
        Skipped,
        Error
    }
}