﻿namespace CodeSamples.Monetization
{
    /// <summary>
    /// Contains current state of an ad that is being played: whether it has finished, its result, any error messages.
    /// </summary>
    public class AdShowState
    {
        #region Properties

        /// <summary>
        /// True when ad playback finished, was interrupted by the player or threw an error, False otherwise.
        /// </summary>
        public bool HasFinished => AdShowResult == AdShowResult.Finished || AdShowResult == AdShowResult.Skipped ||
                                   AdShowResult == AdShowResult.Error;

        /// <summary>
        /// Playback result.
        /// </summary>
        public AdShowResult AdShowResult { get; set; }

        public string ErrorMessage { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AdShowState()
        {
            AdShowResult = AdShowResult.Loading;
            ErrorMessage = string.Empty;
        }

        #endregion

    }
}