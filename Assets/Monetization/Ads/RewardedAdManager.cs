﻿using GoogleMobileAds.Api;
using CodeSamples.Events;
using CodeSamples.ScriptableObjects;
using System.Collections;
using UnityEngine;

namespace CodeSamples.Monetization
{
    /// <summary>
    /// Shows a rewarded ad when player presses the respective button in the mission end screen.
    /// </summary>
    public class RewardedAdManager : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Reference to the scriptable object that holds current mission income.
        /// </summary>
        [Space]
        [Header("Assets")]
        [Tooltip("Reference to the scriptable object that holds current mission income.")]
        [SerializeField]
        private IntVariable missionBounty;

        /// <summary>
        /// Reference to the scriptable object that holds the amount of offline income generated since last session.
        /// </summary>
        [Tooltip("Reference to the scriptable object that holds the amount of offline income generated since last session.")]
        [SerializeField]
        private IntVariable offlineIncome;

        /// <summary>
        /// Event that is raised when player presses the claim bounty button.
        /// </summary>
        [Space]
        [Header("Events")]
        [Tooltip("Event that is raised when player presses the claim bounty button.")]
        [SerializeField]
        private GameEvent onBountyClaimed;

        /// <summary>
        /// Event that is raised when player presses the claim offline income button.
        /// </summary>
        [Tooltip("Event that is raised when player presses the claim offline income button.")]
        [SerializeField]
        private GameEvent onOfflineIncomeClaimed;

        /// <summary>
        /// Object that holds ad playback state.
        /// </summary>
        private AdShowState adShowState;

        /// <summary>
        /// Reference to the object that holds the rewarded add.
        /// </summary>
        private RewardBasedVideoAd rewardedAd;

        #endregion

        #region Constants

        /// <summary>
        /// Rewarded ad unity ID in AdMob dashboard.
        /// </summary>
        private const string RewardedAdUnitID = "ca-app-pub-redacted:(";

        /// <summary>
        /// Test ad unit ID for Rewarded ads.
        /// </summary>
        private const string RewardedAdUnitTestID = "ca-app-pub-redacted:(";

        #endregion

        #region Init

        private void Start()
        {
            rewardedAd = RewardBasedVideoAd.Instance;
          
            rewardedAd.OnAdLoaded += OnRewardedAdLoaded;
            rewardedAd.OnAdFailedToLoad += OnRewardedAdFailedToLoad;
            rewardedAd.OnAdOpening += OnRewardedAdStarted;
            rewardedAd.OnAdClosed += OnRewardedAdSkipped;
            rewardedAd.OnAdCompleted += OnRewardedAdCompleted;
            rewardedAd.OnAdLeavingApplication += OnRewardedAdClicked;

            RequestANewVideo();
        }

        /// <summary>
        /// Requests a new video to be loaded into the rewarded ad.
        /// </summary>
        private void RequestANewVideo()
        {
            adShowState = new AdShowState();

            AdRequest request = new AdRequest.Builder().Build();

            rewardedAd.LoadAd(request, RewardedAdUnitTestID);
        }

        /// <summary>
        /// Callback for when the ad is successfully loaded. 
        /// Sets the <see cref="AdShowState.AdShowResult"/> to <see cref="AdShowResult.Loaded"/> on <see cref="adShowState"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRewardedAdLoaded(object sender, System.EventArgs e)
        {
            adShowState.AdShowResult = AdShowResult.Loaded;
        }

        /// <summary>
        /// Callback for when the rewarded ad failes to load. Loads a message to the console
        /// and sets the value of the <see cref="AdShowState.AdShowResult"/> property on
        /// <see cref="adShowState"/> object to <see cref="AdShowResult.Error"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRewardedAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
        {
            adShowState.AdShowResult = AdShowResult.Error;
        }

        /// <summary>
        /// Callback for when rewarded ad starts. Sets the <see cref="AdShowState.AdShowResult"/> to <see cref="AdShowResult.Playing"/>
        /// on <see cref="adShowState"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRewardedAdStarted(object sender, System.EventArgs e)
        {
            adShowState.AdShowResult = AdShowResult.Playing;
        }

        /// <summary>
        /// Callback for when user skips the rewarded ad. 
        /// Sets the <see cref="AdShowState.AdShowResult"/> to <see cref="AdShowResult.Skipped"/> on <see cref="adShowState"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRewardedAdSkipped(object sender, System.EventArgs e)
        {
            if (adShowState.AdShowResult != AdShowResult.Finished)
            {
                adShowState.AdShowResult = AdShowResult.Skipped;
            }
        }

        /// <summary>
        /// Callback for when the entire ad is watched.
        /// Sets the <see cref="AdShowState.AdShowResult"/> to <see cref="AdShowResult.Finished"/> on <see cref="adShowState"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRewardedAdCompleted(object sender, System.EventArgs e)
        {
            adShowState.AdShowResult = AdShowResult.Finished;
        }

        /// <summary>
        /// Callback for when user clicks the rewarded ad and brings the app into background. 
        /// Sets the <see cref="AdShowState.AdShowResult"/> to <see cref="AdShowResult.Finished"/> on <see cref="adShowState"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRewardedAdClicked(object sender, System.EventArgs e)
        {
            adShowState.AdShowResult = AdShowResult.Finished;
        }

        #endregion

        #region Ad

        /// <summary>
        /// Shows ad when the 2x button is clicked in the mission bounty screen.
        /// Starts a coroutine that makes sure the add is ready.
        /// </summary>
        /// <param name="claimingOfflineIncome">Is player claiming offline income or mission bounty?</param>
        public void ShowAd(bool claimingOfflineIncome)
        {
            PlayerPrefs.Save();
        
            StartCoroutine(WaitForAdResult(claimingOfflineIncome));
            StartCoroutine(ShowAdWhenReady());
        }        

        /// <summary>
        /// Shows an add when it is ready.
        /// </summary>
        /// <returns>
        /// An IEnumerator.
        /// </returns>
        private IEnumerator ShowAdWhenReady()
        {
            while (!rewardedAd.IsLoaded())
            {
                yield return null;
            }

            rewardedAd.Show();
        }

        /// <summary>
        /// Coroutine that waits until unity ad playback finishes or is interrupred.        /// 
        /// Chooses between <see cref="HandleAdShowResultBounty"/> and <see cref="HandleAdShowResultOfflineIncome"/>
        /// based on passed parameter and requests a new video to start loading into the rewarded ad objectafter bounty is handled.
        /// </summary>
        /// <remarks>
        /// Implemented as a courutine because ads seem to play on a worker thread that can't acces Unity API.
        /// </remarks>
        /// <param name="claimingOfflineIncome">Is player claiming offline income or mission bounty?</param>
        /// <returns>
        /// An IEnumerator.
        /// </returns>
        private IEnumerator WaitForAdResult(bool claimingOfflineIncome)
        {
            while (!adShowState.HasFinished)
            {
                yield return null;
            }

            if (claimingOfflineIncome)
            {
                HandleAdShowResultOfflineIncome();
            }
            else
            {
                HandleAdShowResultBounty();
            }

            RequestANewVideo();

            StopAllCoroutines();
        }

        /// <summary>
        /// Handles ad playback result when mission bounty is claimed. Rewards the player is the video was watched to the end.
        /// </summary>
        private void HandleAdShowResultBounty()
        {
            if (adShowState.AdShowResult == AdShowResult.Finished)
            {
                missionBounty.Value *= 2;
            }
            else if (adShowState.AdShowResult == AdShowResult.Error)
            {
                if (string.IsNullOrEmpty(adShowState.ErrorMessage))
                {
                    Debug.LogError("Failed to play rewarded ad when claiming mission bounty!");
                }
                else
                {
                    Debug.LogErrorFormat("Error when trying to play rewarded ad when claiming mission bounty: {0}", adShowState.ErrorMessage);
                }
            }

            onBountyClaimed.Raise();
        }

        /// <summary>
        /// Handles ad playback result when offline income is clained. Rewards the player is the video was watched to the end.
        /// </summary>
        private void HandleAdShowResultOfflineIncome()
        {
            if (adShowState.AdShowResult == AdShowResult.Finished)
            {
                offlineIncome.Value *= 2;
            }
            else if (adShowState.AdShowResult == AdShowResult.Error)
            {
                if (string.IsNullOrEmpty(adShowState.ErrorMessage))
                {
                    Debug.LogError("Failed to play rewarded ad when claiming offline income!");
                }
                else
                {
                    Debug.LogErrorFormat("Error when trying to play rewarded ad when claiming offline income: {0}", adShowState.ErrorMessage);
                }
            }

            onOfflineIncomeClaimed.Raise();
        }

        #endregion
    }
}
