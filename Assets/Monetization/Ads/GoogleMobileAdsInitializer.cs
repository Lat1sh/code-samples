﻿using GoogleMobileAds.Api;
using CodeSamples.Events;
using UnityEngine;

namespace CodeSamples
{
    /// <summary>
    /// Initializes Google mobile ads and raises an event when initialization finishes 
    /// (regardless of whether it succeeded or not).
    /// </summary>
    public class GoogleMobileAdsInitializer : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// Event that is raised when Google mobile ads finishes initialization, successfully or not.
        /// </summary>
        [SerializeField]
        private GameEvent onGoogleMobileAdsInitialized;

        #endregion

        #region Constants

        private const string AdMobAppID = "ca-app-pub-redacted:(";

        #endregion

        #region Init

        private void Start()
        {
            MobileAds.Initialize(AdMobAppID);
            onGoogleMobileAdsInitialized.Raise();
        }

        #endregion
    }
}