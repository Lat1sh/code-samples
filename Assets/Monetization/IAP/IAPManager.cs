﻿using Firebase.Analytics;
using CodeSamples.Events;
using System;
using UnityEngine;
using UnityEngine.Purchasing;

namespace CodeSamples.Monetization
{
    /// <summary>
    /// Handles Google Play store purchases.
    /// </summary>
    public class IAPManager : MonoBehaviour, IStoreListener
    {
        #region Properties

        /// <summary>
        /// Returns True if both purchasing entities are not null, False otherwise.
        /// </summary>
        public static bool IsInitialized => StoreController != null && storeExtensionProvider != null;

        #endregion

        #region Fields

        /// <summary>
        /// Event that is raised when the purchasing system is initialized.
        /// </summary>
        [Tooltip("Event that is raised when the store is initialized")]
        [SerializeField]
        private GameEvent onStoreInitialized;

        /// <summary>
        /// Event that is raised when the no ads non-consumable is purchased.
        /// </summary>
        [Tooltip("Event that is raised when the no ads non-consumable is purchased.")]
        [SerializeField]
        private GameEvent onNoAdsPurchased;        

        /// <summary>
        /// Reference to the store controller.
        /// </summary>
        public static IStoreController StoreController;

        /// <summary>
        /// Reference to the store extension provider.
        /// </summary>
        private static IExtensionProvider storeExtensionProvider;

        #endregion

        #region Constants

        /// <summary>
        /// No ands in-app purchase name as set in the Google Play Console.
        /// </summary>
        private const string NoAdsNonConsumable = "noads";

        #endregion

        #region Init

        void Start()
        {
            if (StoreController == null)
            {
                InitializePurchasing();
            }
        }

        /// <summary>
        /// Starts the initialization process if it hasn't been completed already.
        /// </summary>
        public void InitializePurchasing()
        {
            if (IsInitialized)
            {
                return;
            }

            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            builder.AddProduct(NoAdsNonConsumable, ProductType.NonConsumable);
            UnityPurchasing.Initialize(this, builder);
        }

        /// <summary>
        /// Called when purchasing system is initialized successfuly.
        /// </summary>
        /// <param name="controller">Store controller.</param>
        /// <param name="extensions">Store extension provider.</param>
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            onStoreInitialized.Raise();

            Debug.Log("OnInitialized: PASS");

            StoreController = controller;
            storeExtensionProvider = extensions;

            if (StoreController.products.WithID(NoAdsNonConsumable).hasReceipt)
            {
                onNoAdsPurchased.Raise();
            }
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
            onStoreInitialized.Raise();
        }

        #endregion

        #region Purchase

        /// <summary>
        /// Buys the no ads non-consumable.
        /// </summary>
        public void BuyNoAdsNonConsumable() => BuyProductID(NoAdsNonConsumable);

        /// <summary>
        /// Buys an items with the provided ID.
        /// </summary>
        /// <param name="productId">Item ID.</param>
        private void BuyProductID(string productId)
        {
            if (IsInitialized)
            {
                Product product = StoreController.products.WithID(productId);
              
                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    StoreController.InitiatePurchase(product);
                }
                else
                {
                    FirebaseAnalytics.LogEvent("purchase_failed_not_present_or_available");
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                FirebaseAnalytics.LogEvent("purchase_failed_store_not_initialized");
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            // A consumable product has been purchased by this user.
            if (string.Equals(args.purchasedProduct.definition.id, NoAdsNonConsumable, StringComparison.Ordinal))
            {
                // TO-DO: This is temporary for beta builds, remove
                PlayerPrefs.SetInt("AdsRemoved", 1);
                PlayerPrefs.Save();
                onNoAdsPurchased.Raise();
                FirebaseAnalytics.LogEvent("purchase_succeeded");
            }
            else
            {
                FirebaseAnalytics.LogEvent("purchase_failed_unrecognized_product");
                Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            }

            return PurchaseProcessingResult.Complete;
        }

        /// <summary>
        /// Called when purchase fails. Supplies the product and failure reason.
        /// </summary>
        /// <param name="product">Purchased product.</param>
        /// <param name="failureReason">Purchase failure reason.</param>
        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        }

        #endregion
    }
}