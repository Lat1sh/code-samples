﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Stores prefab data: a reference to a prefab and the number of prefabs to instantiate.
    /// </summary>
    [System.Serializable]
    public class PrefabData
    {
        #region Properties

        /// <summary>
        /// Prefab corresponding with specific sheep type.
        /// </summary>
        /// <value>
        /// Gets the value of the game object field prefab.
        /// </value>
        public GameObject Prefab => prefab;

        /// <summary>
        /// Number of game objects of this sheep type to instantiate and store in the pool.
        /// </summary>
        /// <value>
        /// Gets the value of the int field numberToInstantiate.
        /// </value>
        public int NumberToInstantiate => numberToInstantiate;

        #endregion

        #region Fields

        [SerializeField]
        private GameObject prefab;

        [SerializeField]
        private int numberToInstantiate;

        #endregion
    }
}