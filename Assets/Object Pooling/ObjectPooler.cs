﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Base class for object poolers.
    /// </summary>
    /// <typeparam name="T">Pooled object facade type.</typeparam>
    /// <typeparam name="TEnum">Pooled object type.</typeparam>
    public abstract class ObjectPooler<T, TEnum> where T : ObjectFacade<TEnum> where TEnum : System.Enum
    {
        #region Fields

        /// <summary>
        /// Reference to the pool settings.
        /// </summary>
        protected PoolSettings poolSettings;

        /// <summary>
        /// Object that serves as a holder for pooled objects.
        /// </summary>
        protected Transform pooledObjectHolder;

        /// <summary>
        /// Pooled objects.
        /// </summary>
        protected List<T> pool;

        /// <summary>
        /// Reference to Zenject's container.
        /// </summary>
        private DiContainer container;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="container">Zenject container.</param>
        public ObjectPooler(DiContainer container) => this.container = container;

        #endregion

        #region Init

        /// <summary>
        /// Initializes this object.
        /// </summary>
        /// <param name="poolSettings">Pool settings.</param>
        /// <param name="pooledObjectHolder">Object that serves as a holder for pooled objects.</param>
        public void Init(PoolSettings poolSettings, Transform pooledObjectHolder)
        {
            this.poolSettings = poolSettings;
            this.pooledObjectHolder = pooledObjectHolder;
        }

        /// <summary>
        /// Initializes sheep pool based on the settings in the <see cref="sheepPoolSettings"/> field.
        /// </summary>
        public virtual void InitializePool()
        {
            pool = new List<T>();

            List<PrefabData> prefabData = poolSettings.PrefabData;            

            for (int i = 0; i < prefabData.Count; i++)
            {
                for (int count = 0; count < prefabData[i].NumberToInstantiate; count++)
                {
                    ObjectFacade<TEnum> newFacade = container.InstantiatePrefab(prefabData[i].Prefab).GetComponentInChildren<ObjectFacade<TEnum>>();

                    Transform pivotTransform = newFacade.transform.parent;

                    if (pivotTransform == null)
                    {
                        pivotTransform = newFacade.transform;
                    }

                    pivotTransform.SetParent(pooledObjectHolder);
                    pivotTransform.gameObject.SetActive(false);
                    pivotTransform.name += count;

                    pool.Add((T)newFacade);
                }
            }
        }

        #endregion

        #region Pool

        /// <summary>
        /// Find a non-active object of supplied type and returns it. Returns null if pool is empty.
        /// </summary>
        /// <param name="type">Type of object  to get from the pool.</param>
        /// <returns>
        /// An <see cref="ObjectFacade{TEnum}"/>.
        /// </returns>
        public T Get(TEnum type)
        {
            foreach (T objectFacade in pool)
            {
                if (EqualityComparer<TEnum>.Default.Equals(objectFacade.Type, type) && !objectFacade.gameObject.activeInHierarchy)
                {
                    return objectFacade;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a random inactive facade from the pool.
        /// </summary>
        /// <returns>
        /// An <see cref="ObjectFacade{TEnum}"/>.
        /// </returns>
        public T GetRandom()
        {
            List<T> facades = new List<T>(pool);

            while (facades.Count > 0)
            {
                int index = Random.Range(0, facades.Count);

                if (!facades[index].IsActive)
                {
                    return pool[index];
                }
                else
                {
                    facades.RemoveAt(index);
                }
            }

            return null;
        }

        #endregion
    }
}