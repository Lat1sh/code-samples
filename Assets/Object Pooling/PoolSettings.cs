﻿using System.Collections.Generic;
using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Stores settings for pooling a certain type of objects.
    /// </summary>
    [System.Serializable]
    public class PoolSettings
    {
        #region Properties

        /// <summary>
        /// List of objects that store prefab data
        /// </summary>
        /// <value>
        /// Gets and sets the value of the field prefabData.
        /// </value>
        public List<PrefabData> PrefabData { get => prefabData; set => prefabData = value; }

        #endregion

        #region Fields

        [Tooltip("List of objects that store prefab data.")]
        [SerializeField]
        private List<PrefabData> prefabData;

        #endregion
    }
}