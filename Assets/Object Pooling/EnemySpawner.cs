﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Spawns and despawns enemies.
    /// </summary>
    public class EnemySpawner : MonoBehaviour
    {
        #region Properties

        /// <summary>
        /// List of currently spawned enemy facades.
        /// </summary>
        public List<EnemyFacade> SpawnedEnemyFacades { get; private set; }

        #endregion

        #region Fields

        /// <summary>
        /// Object that instantiates and manages a pool of enemies.
        /// </summary>
        private EnemyPooler enemyPooler;

        /// <summary>
        /// Transform of the game object that servers as a holder for the spawned enemies.
        /// </summary>
        /// <remarks>
        /// Spawned enemies, as opposed to pooled ones, are active in the scene.
        /// </remarks>
        [Inject(Id = "SpawnedEnemyHolder")]
        private Transform spawnedEnemyHolder;

        /// <summary>
        /// Transform component on the game object that serves as a holder for pooled enemies.
        /// </summary>
        [Inject(Id = "PooledEnemyHolder")]
        private Transform pooledObjectHolder;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting necesssary dependencies.
        /// </summary>
        /// <param name="enemyPooler">Object that instantiates and manages a pool of enemies.</param>
        [Inject]
        public void Construct(EnemyPooler enemyPooler) => this.enemyPooler = enemyPooler;

        private void Start() => SpawnedEnemyFacades = new List<EnemyFacade>();

        #endregion

        #region Spawn

        /// <summary>
        /// Spawns an enemy of specified type at the specified position.
        /// </summary>
        /// <param name="enemyType">Type of enemy to spawn.</param>
        /// <param name="spawnPosition">Position to place the object on spawn.</param>
        /// <param name="rotation">Rotation of the spawned object.</param>
        /// <param name="activateImmediately">Should the spawned object be set active immediately on spawn?</param>
        /// /// <returns>
        /// The <see cref="EnemyFacade"/> component on the spawned object.
        /// </returns>
        public EnemyFacade Spawn(EnemyType enemyType, Vector3 spawnPosition, Quaternion rotation, bool activateImmediately = true)
        {
            EnemyFacade enemyFacade = enemyPooler.Get(type: enemyType);

            if (enemyFacade == null)
            {
                Debug.LogError("Received a null reference from enemy pooler when trying to get an enemy!" +
                               "Consider increasing the total size of the pool or change spawning frequency.");

                return null;
            }

            SpawnedEnemyFacades.Add(enemyFacade);

            Transform enemyTransform = enemyFacade.transform;

            enemyTransform.position = spawnPosition;
            enemyTransform.rotation = rotation;
            enemyTransform.SetParent(spawnedEnemyHolder);

            enemyFacade.gameObject.SetActive(activateImmediately);

            return enemyFacade;
        }

        /// <summary>
        /// Despawns a passed enemy facade.
        /// </summary>
        /// <param name="facadeToDespawn">Enemy facade to despawn.</param>
        public void DespawnFacade(EnemyFacade facadeToDespawn)
        {
            facadeToDespawn.transform.SetParent(pooledObjectHolder);
            _ = SpawnedEnemyFacades.Remove(facadeToDespawn);
        }

        #endregion
    }
}
