﻿using UnityEngine;
using Zenject;

namespace MetraReborn
{
    /// <summary>
    /// Initializes and manages a pool of enemy objects.
    /// </summary>
    public class EnemyPooler : ObjectPooler<EnemyFacade, EnemyType>
    {
        #region Fields

        /// <summary>
        /// Transform component on the game object that serves as a holder for pooled enemies.
        /// </summary>
        [Inject(Id = "PooledEnemyHolder")]
        protected new Transform pooledObjectHolder;

        /// <summary>
        /// Pool settings.
        /// </summary>
        [Inject(Id = "EnemyPoolSettings")]
        protected new PoolSettings poolSettings;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="container">Zenject container.</param>
        public EnemyPooler(DiContainer container) : base(container) { }

        #endregion

        #region Init

        /// <summary>
        /// Initializes the object after all dependencies are injected. 
        /// Calls <see cref="ObjectPooler{T, TEnum}.InitializePool"/> on the base object.
        /// </summary>
        [Inject]
        public override void InitializePool()
        {
            Init(poolSettings, pooledObjectHolder);
            base.InitializePool();
        }

        #endregion
    }
}