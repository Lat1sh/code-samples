﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Component that serves as a base facade for various enemy objects.
    /// </summary>
    public abstract class EnemyFacade : ObjectFacade<EnemyType>, ICollidable, IMagicTarget, IPolymorphableObject
    {
        #region Properties

        /// <summary>
        /// Character's tag.
        /// </summary>
        /// <value>
        /// Gets root game object's tag.
        /// </value>
        public string Tag => tag;

        /// <summary>
        /// Character's root game object.
        /// </summary>
        /// <value>
        /// Gets the value of gameObject property.
        /// </value>
        public GameObject GameObject => gameObject;

        /// <summary>
        /// Character's current non-scaled velocity.
        /// </summary>
        /// <value>
        /// Gets the value of the Vector2 field currentVelocity.
        /// </value>
        public Vector2 CurrentVelocity => currentVelocity;

        /// <summary>
        /// Should other collidable objects stop when colliding with this object?
        /// </summary>
        /// <value>
        /// Gets a boolean.
        /// </value>
        public bool StopWhenCollidingWithThisObject => false;     

        /// <summary>
        /// Is this object currently grounded?
        /// </summary>
        /// <value>
        /// Gets and sets a boolean.
        /// </value>
        public bool LockTransform { get; set; } = false;

        /// <summary>
        /// Is this object currently grounded?
        /// </summary>
        /// <value>
        /// Gets and sets a boolean.
        /// </value>
        public bool IsGrounded { get; private set; } = true;   

        /// <summary>
        /// Current ground the character is standing on.
        /// </summary>
        /// <value>
        /// Gets and sets an <see cref="IGround"/> object.
        /// </value>
        public IGround CurrentGround { get; private set; }

        /// <summary>
        /// Transform to use for attaching to a ground object that is applying
        /// gravity magic to this object. For objects that are polymorphable,
        /// root transform has to be used so that the object that represents
        /// alternate polymorph form can be rotated too.
        /// </summary>
        public Transform GroundRotationPivot
        {
            get
            {
                return transform;
            }
        }

        /// <summary>
        /// Component that handles casting magical effects on this object.
        /// </summary>
        public MagicController MagicController { get; protected set; }

        /// <summary>
        /// Component that controls outline drawing.
        /// </summary>
        public OutlineController OutlineController { get; private set; }

        /// <summary>
        /// Is this interactable in spectral state?
        /// </summary>
        /// <value>
        /// Gets the value of the <see cref="IsSpectral"/> property on the <see cref="MagicController"/>.
        /// </value>
        public bool IsSpectral => MagicController.IsSpectral;

        /// <summary>
        /// Is this enemy currently facing righr?
        /// </summary>
        /// <value>
        /// Gets the value of the <see cref="FlipController.IsFacingRight"/> property on <see cref="flipController"/>.
        /// </value>
        public bool IsFacingRight => flipController.IsFacingRight;

        /// <summary>
        /// Enemy's current gravity direction.
        /// </summary>
        public GravityDirection GravityDirection => MagicController.GravityDirection;

        /// <summary>
        /// Polymorphable object's type.
        /// </summary>
        public PolymorphableObjectType PolymorphableObjectType => PolymorphableObjectType.Enemy;

        #endregion

        #region Fields 

        private Vector2 currentVelocity;

        /// <summary>
        /// Component that flips this object.
        /// </summary>
        protected FlipController flipController;

        #endregion

        #region Init

        /// <summary>
        /// Constructs this object by injecting dependencies.
        /// </summary>
        /// <param name="flipController">Component that flips this object.</param>
        /// <param name="magicController">Component that handles magic effects.</param>
        [Zenject.Inject]
        public void Construct(FlipController flipController, MagicController magicController)
        {
            MagicController = magicController;
            this.flipController = flipController;
        }

        #endregion

        #region Components

        /// <summary>
        /// Gets a component of specified type. Required to be able to get components 
        /// from objects that are referenced as <see cref="ICollidable"/> 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Component GetCollidableComponent(System.Type type) => GetComponent(type);

        #endregion

        #region Magic

        /// <summary>
        /// Tells the <see cref="MagicController"/> to use gravity magic.
        /// </summary>
        public void UseGravityMagic() => MagicController.UseGravityMagic();

        /// <summary>
        /// Tells the <see cref="MagicController"/> to use spectral magic.
        /// </summary>
        public void UseSpectralMagic() => MagicController.UseSpectralMagic();

        /// <summary>
        /// Tells the <see cref="MagicController"/> to use polymorph magic.
        /// </summary>
        public void UsePolymorphMagic() => MagicController.UsePolymorphMagic();

        /// <summary>
        /// Tells the <see cref="MagicController"/> to use immunity magic.
        /// </summary>
        public void UseImmunityMagic() => MagicController.UseImmunityMagic();

        /// <summary>
        /// Initializes enemy's state when it is polymorphed to.
        /// </summary>
        /// <param name="polymorphableState">State of the enemy polymorphed from.</param>
        public virtual void InitPolymorphableState(PolymorphableState polymorphableState)
        {
            if (((EnemyPolymorphableState)polymorphableState).WasFacingRight != flipController.IsFacingRight)
            {
                flipController.FlipObject();
            }

            MagicController.InitState(polymorphableState.MagicControllerState);
        }

        /// <summary>
        /// Gets polymorphable state when enemy is about to polymorph.
        /// </summary>
        /// <returns>
        /// A <see cref="PolymorphableState"/>.
        /// </returns>
        public virtual PolymorphableState GetPolymorphableState()
        {
            return new EnemyPolymorphableState(flipController.IsFacingRight,
                       new MagicControllerState(GravityDirection, IsSpectral, MagicController.IsImmuneToMagic));
        }

        #endregion

        #region Highlight

        /// <summary>
        /// Shows a highlight around the character when  a certain kind of magic effect targets them.
        /// </summary>
        /// <param name="spellType">Type of the outline to show.</param>
        public void Highlight(SpellType spellType)
        {
            if (MagicController.IsImmuneToMagic)
            {
                return;
            }

            OutlineController.ShowOutline(spellType);
        }

        /// <summary>
        /// Removes highlight from the character.
        /// </summary>
        public void RemoveHighlight()
        {
            if (MagicController.IsImmuneToMagic)
            {
                return;
            }

            OutlineController.HideOutline();
        }

        #endregion
    }
}