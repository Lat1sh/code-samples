﻿namespace MetraReborn
{
    /// <summary>
    /// Type of enemy character.
    /// </summary>
    public enum EnemyType
    {
        Slime,
        Elemental,
        FloatingSkull
    }
}