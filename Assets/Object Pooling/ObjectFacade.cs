﻿using UnityEngine;

namespace MetraReborn
{
    /// <summary>
    /// Base class for all facade objects.
    /// </summary>
    public abstract class ObjectFacade<TEnum> : MonoBehaviour where TEnum :  System.Enum
    {
        #region Properties

        /// <summary>
        /// Facade's type.
        /// </summary>
        /// <value>
        /// Gets the value of the field type.
        /// </value>
        public TEnum Type => type;

        /// <summary>
        /// Is this facade currently active?
        /// </summary>
        public bool IsActive => gameObject.activeInHierarchy;

        #endregion

        #region Fields

        [SerializeField]
        private TEnum type;

        #endregion

        #region Abstract
        public abstract void Disable();

        #endregion
    }
}