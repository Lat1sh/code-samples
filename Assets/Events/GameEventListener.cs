﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace CodeSamples.Events
{
    /// <summary>
    /// Stores a list of events to listen to and respective responses.
    /// </summary>
    [System.Serializable]
    public class GameEventListener : MonoBehaviour
    {
        /// <summary>
        /// An array of listeners.
        /// </summary>
        [SerializeField]
        private EventResponsePair[] listeners;

        #region Enable/disable

        private void OnEnable()
        {
            if (listeners == null)
            {
                return;
            }                

            foreach (EventResponsePair e in listeners)
            {
                e.Event?.RegisterListener(this);
            }
        }

        private void OnDisable()
        {
            if (listeners == null)
            {
                return;
            }

            foreach (EventResponsePair e in listeners)
            {
                e.Event?.UnregisterListener(this);
            }
        }

        #endregion

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (listeners == null)
            {
                return;
            }                

            if (!gameObject.activeInHierarchy)
            {
                return;
            }                

            if (EditorApplication.isPlayingOrWillChangePlaymode)
            {
                return;
            }                

            foreach (EventResponsePair e in listeners)
            {
                e.Event?.RegisterListener(this);
            }
        }
#endif
        

        /// <summary>
        /// Invokes listeners if their event's name matches the name of the passed argument.
        /// </summary>
        /// <param name="e">Target event.</param>
        public void OnEventRaised(GameEvent e)
        {
            foreach (EventResponsePair eventResponsePair in listeners)
            {
                if (e.Equals(eventResponsePair.Event))
                {
                    
#if UNITY_EDITOR
                    if (eventResponsePair.Event.LogToConsole)
                    {
                        Debug.Log(string.Format("<b>{0}</b> is invoking <i>{1}</i> in response to <i>{2}</i> event", eventResponsePair.Response.GetPersistentTarget(0).name,
                                                                                                                    eventResponsePair.Response.GetPersistentMethodName(0),
                                                                                                                    eventResponsePair.Event.name));
                    }
#endif
                    eventResponsePair.Response.Invoke();
                }                
            }
        }
    }
}