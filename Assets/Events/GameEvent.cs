﻿using System.Collections.Generic;
using UnityEngine;

namespace CodeSamples.Events
{ 
#if UNITY_EDITOR
    [CreateAssetMenu(fileName = "New Event", menuName = "Scriptable Objects/Events/New Event", order = 2)]
#endif
    /// <summary>
    /// A scriptable object that holds a unique game event type.
    /// </summary>
    public class GameEvent : ScriptableObject
    {
        #region Properties

        /// <summary>
        /// Event name.
        /// </summary>
        public string Name => name;

        /// <summary>
        /// Should this event be logged to console?
        /// </summary>
        /// <value>
        /// Gets the value of the bool field logToConsole.
        /// </value>
        public bool LogToConsole => logToConsole;

        #endregion

        #region Fields
       
        [Tooltip("Should this event be logged to console?")]
        [SerializeField]
        private bool logToConsole = true;

        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        [SerializeField]
        private List<GameEventListener> eventListeners;

        #endregion

        #region Raise

        /// <summary>
        /// Raises the event, notifying all listeners. Raises an optional DoozyUI event if <paramref name="raiseDoozyUIEvent"/>
        /// is True.
        /// </summary>
        /// <param name="raiseDoozyUIEvent">Should a Doozy UI event be raised as well?</param>
        public void Raise(bool raiseDoozyUIEvent = false)
        {
#if UNITY_EDITOR
            //Debug.LogFormat("Raising {0} with Doozy event: {1} and listener count: {2}", name, raiseDoozyUIEvent, EventListeners.Count);
#endif

            var index = eventListeners.Count - 1;
            
            for (; index >= 0; index--)
            {
                int numberOfListenersBeforeNotifyingCurrent = eventListeners.Count;
                GameEventListener currentListener = eventListeners[index];

                currentListener.OnEventRaised(this);                    

                if (eventListeners.Count != numberOfListenersBeforeNotifyingCurrent)
                {
                    if (!eventListeners.Contains(currentListener))
                    {
                        index = eventListeners.Count;
                    }
                    else
                    {
                        index = eventListeners.Count - 1;
                    }
                }
            }

            if (raiseDoozyUIEvent)
            {
                Doozy.Engine.Message.Send(new Doozy.Engine.GameEventMessage(name));
            }
        }

        #endregion

        #region Listeners

        /// <summary>
        /// Registers a new listeners.
        /// </summary>
        /// <param name="listener">Listener to register.</param>
        public void RegisterListener(GameEventListener listener)
        {
            if (eventListeners == null)
            {
                eventListeners = new List<GameEventListener>();
            }

            if (!eventListeners.Contains(listener))
            {
                eventListeners.Add(listener);               
            }                
        }

        /// <summary>
        /// Unregisters an existing listeners.
        /// </summary>
        /// <param name="listener">Listener to unregister.</param>
        public void UnregisterListener(GameEventListener listener)
        {
            if (eventListeners.Contains(listener))
            {
                eventListeners.Remove(listener);
            }                
        }

        #endregion

        private void OnEnable() => eventListeners = new List<GameEventListener>();
    }
}