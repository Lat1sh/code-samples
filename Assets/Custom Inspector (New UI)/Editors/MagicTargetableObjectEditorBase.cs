﻿using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace MetraReborn.Editor
{
    using UnityEditor;

    /// <summary>
    /// Base custom editor class for object that can be targeted by magic.
    /// </summary>
    public abstract class MagicTargetableObjectEditorBase : Editor
    {
        #region Fields

        protected VisualElement rootElement;
        protected VisualTreeAsset visualTree;

        protected VisualElement objectStateRootElement;
        protected VisualTreeAsset objectStateVisualTree;

        protected VisualElement magicControlsRootElement;
        protected VisualTreeAsset magicControlsVisualTree;

        #endregion

        #region Enable

        protected virtual void OnEnable()
        {
            var labelStyleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Custom Inspector (New UI)/CSS/CustomEditorLabelStyleSheet.uss");

            rootElement.styleSheets.Add(labelStyleSheet);

            objectStateRootElement = new VisualElement();

            objectStateVisualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Custom Inspector (New UI)/XML/ObjectStateTemplate.uxml");

            objectStateRootElement.styleSheets.Add(labelStyleSheet);
            objectStateRootElement.styleSheets.Add(AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Custom Inspector (New UI)/CSS/ObjectStateStyleSheet.uss"));

            magicControlsRootElement = new VisualElement();

            magicControlsVisualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Custom Inspector (New UI)/XML/MagicControlsTemplate.uxml");

            magicControlsRootElement.styleSheets.Add(labelStyleSheet);
            magicControlsRootElement.styleSheets.Add(AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Custom Inspector (New UI)/CSS/MagicControlsStyleSheet.uss"));
        }

        #endregion

        #region Inspector

        /// <summary>
        /// Creates GUI for object targetable by magic: spectral and immunity status, gravity direction, etc.
        /// </summary>
        /// <returns>
        /// A VisualElement.
        /// </returns>
        protected VisualElement CreateObjectStateEditor()
        {
            var objectStateRoot = objectStateRootElement;
            objectStateRootElement.Clear();

            objectStateVisualTree.CloneTree(objectStateRoot);

            objectStateRoot.Q(className: "label").Add(new Label("STATE"));

            var magicController = (serializedObject.targetObject as ICollidable).GameObject.GetComponent<MagicController>();

            // Spectral flag

            var spectralFlagLabel = new Label("Is Spectral");
            spectralFlagLabel.AddToClassList("spectral-flag-label-color");
            objectStateRoot.Q(className: "spectral-flag-label").Add(spectralFlagLabel);

            var spectralFlagToggle = new Toggle
            {
                value = magicController.IsSpectral
            };

            spectralFlagToggle.AddToClassList("spectral-toggle");
            spectralFlagToggle.SetEnabled(false);
            objectStateRoot.Q(className: "spectral-flag-toggle").Add(spectralFlagToggle);

            // Immunity flag

            var immunityFlagLabel = new Label("Is Immune to Magic");
            immunityFlagLabel.AddToClassList("immunity-flag-label-color");
            objectStateRoot.Q(className: "immunity-flag-label").Add(immunityFlagLabel);

            var immunityFlagToggle = new Toggle()
            {
                value = magicController.IsImmuneToMagic
            };
            immunityFlagToggle.AddToClassList("immunity-toggle");
            immunityFlagToggle.SetEnabled(false);
            objectStateRoot.Q(className: "immunity-flag-toggle").Add(immunityFlagToggle);

            // Gravity direction popup

            VisualElement gravityDirectionLabelElement = objectStateRoot.Q(className: "gravity-direction-label");
            var gravityDirectionLabel = new Label("Gravity Direction");
            gravityDirectionLabel.AddToClassList("gravity-direction-label-color");
            gravityDirectionLabelElement.Add(gravityDirectionLabel);
      
            var choicesList = new List<GravityDirection>((GravityDirection[])System.Enum.GetValues(typeof(GravityDirection)));

            var gravityDirectionPopup = new PopupField<GravityDirection>(choices: choicesList,
                                        magicController.GravityDirection);

            gravityDirectionPopup.SetEnabled(false);
            gravityDirectionPopup.AddToClassList("gravity-popup");
            objectStateRoot.Q(className: "gravity-direction-popup").Add(gravityDirectionPopup);

            objectStateRootElement = objectStateRoot;

            return objectStateRootElement;
        }

        /// <summary>
        /// Creates buttons that cast magic effects on object in the editor.
        /// </summary>
        /// <param name="disableGravityButton">Should gravity magic button be disabled for this object?</param>
        /// <param name="disableSpectralButton">Should spectral magic button be disabled for this object?</param>
        /// <param name="disablePolymorphButton">Should polymorph magic button be disabled for this object?</param>
        /// <param name="disableImmunityButton">Should immunity magic button be disabled for this object?</param>
        /// <returns>
        /// A VisualElement.
        /// </returns>
        protected VisualElement CreateMagicControlEditor(bool disableGravityButton = false, bool disableSpectralButton = false,
                                                         bool disablePolymorphButton = false, bool disableImmunityButton = false)
        {
            ICollidable collidableObject = serializedObject.targetObject as ICollidable;

            var magicControlsRoot = magicControlsRootElement;
            magicControlsRoot.Clear();

            magicControlsVisualTree.CloneTree(magicControlsRoot);

            var magicControllers = new MagicController[serializedObject.targetObjects.Length];

            // Use target objects to be able to apply the same magical effect to multiple selected instances.

            for (int i = 0, controllerCount = magicControllers.Length; i < controllerCount; i++)
            {
                magicControllers[i] = ((ICollidable)serializedObject.targetObjects[i]).GameObject.GetComponent<MagicController>();
            }

            var selectedMagicController = ((ICollidable)serializedObject.targetObject).GameObject.GetComponent<MagicController>();

            // Gravity            

            Button gravityButton = (Button) magicControlsRoot.Q(className: "gravity");
            var gravityDirectionPopup = (PopupField<GravityDirection>)objectStateRootElement.Q(className: "gravity-popup");

            gravityButton.SetEnabled(!disableGravityButton);

            if (gravityButton.enabledInHierarchy)
            {
                gravityButton.clickable.clicked += () =>
                {
                    foreach (MagicController magicController in magicControllers)
                    {
                        magicController.UseGravityMagic();

                        if (magicController == selectedMagicController)
                        {
                            if (selectedMagicController.IsImmuneToMagic)
                            {
                                rootElement.Q(className: "immunity-toggle").GetFirstOfType<Toggle>().value = false;
                            }                            

                            UpdateSerializedObject();
                            gravityDirectionPopup.value = magicController.GravityDirection;
                        }
                    }
                };

            }
            // Spectral

            Button spectralButton = (Button) magicControlsRoot.Q(className: "spectral");

            spectralButton.SetEnabled(!disableSpectralButton);

            if (spectralButton.enabledInHierarchy)
            {
                spectralButton.clickable.clicked += () =>
                {
                    foreach (MagicController magicController in magicControllers)
                    {
                        magicController.UseSpectralMagic();

                        if (magicController == selectedMagicController)
                        {
                            if (selectedMagicController.IsImmuneToMagic)
                            {
                                rootElement.Q(className: "immunity-toggle").GetFirstOfType<Toggle>().value = false;
                            }

                            UpdateSerializedObject();
                            rootElement.Q(className: "spectral-toggle").GetFirstOfType<Toggle>().value = magicController.IsSpectral;
                        }
                    }
                };
            }

            // Polymorph

            Button polymorphButton = (Button) magicControlsRoot.Q(className: "polymorph");

            polymorphButton.SetEnabled(!disablePolymorphButton);

            if (polymorphButton.enabledInHierarchy)
            {
                polymorphButton.clickable.clicked += () =>
                {
                    foreach (MagicController magicController in magicControllers)
                    {
                        magicController.UsePolymorphMagic();

                        if (magicController == selectedMagicController)
                        {
                            if (selectedMagicController.IsImmuneToMagic)
                            {
                                rootElement.Q(className: "immunity-toggle").GetFirstOfType<Toggle>().value = false;
                            }
                        }
                    }
                };
            }

            // Immunity

            Button immunityButton = (Button) magicControlsRoot.Q(className: "immunity");

            immunityButton.SetEnabled(!disableImmunityButton);

            if (immunityButton.enabledInHierarchy)
            {
                immunityButton.clickable.clicked += () =>
                {
                    foreach (MagicController magicController in magicControllers)
                    {
                        magicController.UseImmunityMagic();

                        if (magicController == selectedMagicController)
                        {
                            rootElement.Q(className: "immunity-toggle").GetFirstOfType<Toggle>().value = magicController.IsImmuneToMagic;
                            UpdateSerializedObject();
                        }
                    }
                };
            }

            return magicControlsRoot;
        }

        private void UpdateSerializedObject()
        {
            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
        }

        #endregion
    }
}