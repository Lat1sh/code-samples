﻿using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace MetraReborn.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(FanFacade))]
    public class FanFacadeEditor : MagicTargetableObjectEditorBase
    {
        #region Enable

        protected override void OnEnable()
        {
            rootElement = new VisualElement();

            visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Custom Inspector (New UI)/XML/FanFacadeTemplate.uxml");
            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Custom Inspector (New UI)/CSS/FanStyleSheet.uss");
            rootElement.styleSheets.Add(styleSheet);

            base.OnEnable();
        }

        #endregion

        #region Inspector

        public override VisualElement CreateInspectorGUI()
        {
            //////// 1. General

            var root = rootElement;
            root.Clear();

            visualTree.CloneTree(root);

            VisualElement activatorInteractableTypeElement = root.Q(className: "interactable-type");
            SerializedProperty activatorInteractableTypeProperty = serializedObject.FindProperty("type");
            activatorInteractableTypeElement.Add(new PropertyField(activatorInteractableTypeProperty));

            VisualElement velocityDirectionElement = root.Q(className: "velocity-direction");
            velocityDirectionElement.Add(new PropertyField(serializedObject.FindProperty("velocityZoneDirection")));

            VisualElement scaleElement = root.Q(className: "scale");
            scaleElement.Add(new PropertyField(serializedObject.FindProperty("scale")));

            VisualElement factorElement = root.Q(className: "factor");
            factorElement.Add(new PropertyField(serializedObject.FindProperty("characterVelocityFactor")));

            //////// 2. Object state            

            root.Add(CreateObjectStateEditor());

            //////// 3. Magic controls            

            root.Add(CreateMagicControlEditor(disableGravityButton: true));

            return root;
        }

        #endregion
    }
}
