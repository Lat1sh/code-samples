﻿using MetraReborn.Events;
using MetraReborn.ScriptableObjects;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;
using static MetraReborn.GameControls;

namespace MetraReborn
{
    /// <summary>
    /// Handles player input. Passes input values and raises input events when necessary.
    /// </summary>
    public class InputManager : MonoBehaviour, IMovementActions, IInteractionsActions, IDashActions
    {
        #region Fields

        /// <summary>
        /// Scriptable object that holds current horizontal input.
        /// </summary>
        [Inject(Id = "HorizontalInput")]
        private FloatVariable horizontalInput;

        /// <summary>
        /// Scriptable object that holds current vertical input.
        /// </summary>
        [Inject(Id = "VerticalInput")]
        private FloatVariable verticalInput;

        /// <summary>
        /// Event that is raised when player presses the jump button.
        /// </summary>
        [Inject(Id = "OnJumpButtonPressed")]
        private GameEvent onJumpButtonPressed;

        /// <summary>
        /// Event that is raised when player releases the jump button.
        /// </summary>
        [Inject(Id = "OnJumpButtonReleased")]
        private GameEvent onJumpButtonReleased;

        /// <summary>
        /// Event that is raised when player presses the move right button.
        /// </summary>
        [Inject(Id = "OnMoveRightButtonPressed")]
        private GameEvent onMoveRightButtonPressed;

        /// <summary>
        /// Event that is raised when player presses the move left button.
        /// </summary>
        [Inject(Id = "OnMoveLeftButtonPressed")]
        private GameEvent onMoveLeftButtonPressed;

        /// <summary>
        /// Event that is raised when player presses the move up button.
        /// </summary>
        [Inject(Id = "OnMoveUpButtonPressed")]
        private GameEvent onMoveUpButtonPressed;

        /// <summary>
        /// Event that is raised when player presses the move down button.
        /// </summary>
        [Inject(Id = "OnMoveDownButtonPressed")]
        private GameEvent onMoveDownButtonPressed;

        /// <summary>
        /// Event that is raised when player presses the interact button.
        /// </summary>
        [Inject(Id = "OnUseInteractableButtonPressed")]
        private GameEvent onUseInteractableButtonPressed;

        /// <summary>
        /// Event that is raised when player presses the dash button.
        /// </summary> 
        [Inject(Id = "OnDashButtonPressed")]
        private GameEvent onDashButtonPressed;

        /// <summary>
        /// Event that is raised when the test control for changing gravity direction is used.
        /// </summary>
        [Inject(Id = "OnReverseGravityButtonPressed")]
        private GameEvent onReverseGravityButtonPressed;

        /// <summary>
        /// Event that is raised when the test control for toggling spectral state is pressed.
        /// </summary>
        [Inject(Id = "OnToggleSpectralStateButtonPressed")]
        private GameEvent onToggleSpectralStateButtonPressed;

        /// <summary>
        /// Reference to game controls.
        /// </summary>
        private GameControls gameControls;

        /// <summary>
        /// Reference to the movemement action category of game controls.
        /// </summary>
        private MovementActions movementActions;

        /// <summary>
        /// Reference to the test controls action category of game controls.
        /// </summary>
        private TestControlsActions testActions;

        /// <summary>
        /// Reference to the interactions category of game controls.
        /// </summary>
        private InteractionsActions interactionActions;

        /// <summary>
        /// Reference to the dash category of movememnt controls.
        /// </summary>
        private DashActions dashActions;

        private float stickXValueOnPreviousFrame = 0f;
        private float stickYValueOnPreviousFrame = 0f;

        #endregion

        #region Constants

        /// <summary>
        /// Minimum value of a game pad stick's dead zone. Input values below this are ignored.
        /// </summary>
        private const float StickDeadZoneMin = 0.125f;

        #endregion

        #region Init

        private void Start()
        {
            gameControls = new GameControls();
            gameControls.Enable();

            movementActions = gameControls.Movement;
            movementActions.SetCallbacks(this);
            movementActions.Enable();

            testActions = gameControls.TestControls;
            testActions.SetCallbacks(this);
            testActions.Enable();

            interactionActions = gameControls.Interactions;
            interactionActions.SetCallbacks(this);
            interactionActions.Enable();

            dashActions = gameControls.Dash;
            dashActions.SetCallbacks(this);
            dashActions.Disable();

            horizontalInput.Value = verticalInput.Value = 0f;
        }

        #endregion

        #region Movement callbacks  

        /// <summary>
        /// Input callback that is fired when "Move Right" button is pressed.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnMoveRight(InputAction.CallbackContext context)
        {
            foreach (InputControl control in movementActions.MoveLeft.controls)
            {                
                if (control.IsActuated())
                {
                    return;
                }                
            }

            horizontalInput.Value = 1f;           
            onMoveRightButtonPressed.Raise();
        }

        /// <summary>
        /// Input callback that is fired when "Move Left" button is pressed.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnMoveLeft(InputAction.CallbackContext context)
        {
            foreach (InputControl control in movementActions.MoveRight.controls)
            {               
                if (control.IsActuated())
                {
                    return;
                }                
            }

            horizontalInput.Value = -1f;
            onMoveLeftButtonPressed.Raise();
        }

        /// <summary>
        /// Input callback that is fired when "Move Up" button is pressed.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnMoveUp(InputAction.CallbackContext context)
        {
            foreach (InputControl control in movementActions.MoveDown.controls)
            {               
                if (control.IsActuated())
                {
                    return;
                }                
            }

            verticalInput.Value = 1f;
            onMoveUpButtonPressed.Raise();
        }

        /// <summary>
        /// Input callback that is fired when "Move Down" button is pressed.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnMoveDown(InputAction.CallbackContext context)
        {
            foreach (InputControl control in movementActions.MoveUp.controls)
            {                
                if (control.IsActuated())
                {
                    return;
                }                
            }

            verticalInput.Value = -1f;
            onMoveDownButtonPressed.Raise();
        }

        /// <summary>
        /// Input callback that is fired when "Move Right" button is released.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnMoveRightReleased(InputAction.CallbackContext context)
        {
            foreach (InputControl control in movementActions.MoveRight.controls)
            {               
                if (control.IsActuated())
                {
                    return;
                }                
            }

            foreach (InputControl control in movementActions.MoveLeft.controls)
            {                
                if (control.IsActuated())
                {
                    onMoveLeftButtonPressed.Raise();
                    horizontalInput.Value = -1f;
                    return;
                }                                
            }

            horizontalInput.Value = 0f;
        }

        /// <summary>
        /// Input callback that is fired when "Move Left" button is released.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnMoveLeftReleased(InputAction.CallbackContext context)
        {
            foreach (InputControl control in movementActions.MoveLeft.controls)
            {                
                if (control.IsActuated())
                {
                    return;
                }                
            }

            foreach (InputControl control in movementActions.MoveRight.controls)
            {                
                if (control.IsActuated())
                {
                    onMoveRightButtonPressed.Raise();
                    horizontalInput.Value = 1f;
                    return;
                }                
            }

            horizontalInput.Value = 0f;
        }

        /// <summary>
        /// Input callback that is fired when "Move Up" button is released.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnMoveUpReleased(InputAction.CallbackContext context)
        {
            foreach (InputControl control in movementActions.MoveUp.controls)
            {               
                if (control.IsActuated())
                {
                    return;
                }                
            }

            foreach (InputControl control in movementActions.MoveDown.controls)
            {                
                if (control.IsActuated())
                {
                    onMoveDownButtonPressed.Raise();
                    verticalInput.Value = -1f;
                    return;
                }                
            }

            verticalInput.Value = 0f;
        }

        /// <summary>
        /// Input callback that is fired when "Move Down" button is released.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnMoveDownReleased(InputAction.CallbackContext context)
        {
            foreach (InputControl control in movementActions.MoveDown.controls)
            {                
                if (control.IsActuated())
                {
                    return;
                }                
            }

            foreach (InputControl control in movementActions.MoveUp.controls)
            {                
                if (control.IsActuated())
                {
                    onMoveUpButtonPressed.Raise();
                    verticalInput.Value = 1f;
                    return;
                }                
            }

            verticalInput.Value = 0f;
        }

        

        /// <summary>
        /// Input callback that is fired when jump button is used.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnJump(InputAction.CallbackContext context)
        {
            if (context.phase == InputActionPhase.Started)
            {
                onJumpButtonPressed.Raise();
            }            
        }

        /// <summary>
        /// Input callback that is fired when jump button is released.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnJumpReleased(InputAction.CallbackContext context)
        {       

            foreach (InputControl control in movementActions.Jump.controls)
            {
                if (control.IsActuated())
                {
                    return;
                }
            }

            onJumpButtonReleased.Raise();
        }

        /// <summary>
        /// Callback for when input value from gamepad's left stick is updated.
        /// </summary>
        /// <param name="context"></param>
        public void OnLeftGamePadStick(InputAction.CallbackContext context)
        {
            Vector2 newStickValue = (Vector2)context.control.ReadValueAsObject();

            float stickXValueOnCurrentFrame = newStickValue.x;
            float stickYValueOnCurrentFrame = newStickValue.y;

            if (Mathf.Abs(stickXValueOnCurrentFrame) < StickDeadZoneMin)
            {
                stickXValueOnCurrentFrame = 0f;
            }

            if (Mathf.Abs(stickYValueOnCurrentFrame) < StickDeadZoneMin)
            {
                stickYValueOnCurrentFrame = 0f;
            }

            ProcessHorizontalStickInput(stickXValueOnCurrentFrame, context);
            ProcessVerticalStickInput(stickYValueOnCurrentFrame, context);

            stickXValueOnPreviousFrame = stickXValueOnCurrentFrame;
            stickYValueOnPreviousFrame = stickYValueOnCurrentFrame;

            if (stickXValueOnCurrentFrame == 0f)
            {
                horizontalInput.Value = 0f;
            }
            else
            {
                horizontalInput.Value = stickXValueOnCurrentFrame > 0f ? 1f : -1f;
            }

            if (stickYValueOnCurrentFrame == 0f)
            {
                verticalInput.Value = 0f;
            }
            else
            {
                verticalInput.Value = stickYValueOnCurrentFrame > 0f ? 1f : -1f;

                if (stickYValueOnCurrentFrame > 0f)
                {
                    verticalInput.Value = stickYValueOnCurrentFrame > 0.4f ? 1f : 0f;
                }
                else
                {
                    verticalInput.Value = stickYValueOnCurrentFrame < -0.4f ? -1f : 0f;
                }
            }
        }

        /// <summary>
        /// Processes horizontal component of gamepad stick's input.
        /// </summary>
        /// <param name="stickXValueOnCurrentFrame">X value of the stick input this frame.</param>
        /// <param name="context">Input callback context.</param>
        private void ProcessHorizontalStickInput(float stickXValueOnCurrentFrame, InputAction.CallbackContext context)
        {
            if (stickXValueOnCurrentFrame == 0f)
            {
                if (stickXValueOnPreviousFrame < 0f)
                {
                    OnMoveLeftReleased(context);
                }
                else if (stickXValueOnPreviousFrame > 0f)
                {
                    OnMoveRightReleased(context);
                }
            }
            else
            {
                if (stickXValueOnPreviousFrame == 0f)
                {
                    if (stickXValueOnCurrentFrame > 0f)
                    {
                        OnMoveRight(context);
                    }
                    else
                    {
                        OnMoveLeft(context);
                    }
                }
                else
                {
                    if (Mathf.Sign(stickXValueOnCurrentFrame) != Mathf.Sign(stickXValueOnPreviousFrame))
                    {
                        if (stickXValueOnCurrentFrame > 0f)
                        {
                            OnMoveRight(context);
                        }
                        else
                        {
                            OnMoveLeft(context);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Processes vertical component of gamepad stick's input.
        /// </summary>
        /// <param name="stickYValueOnCurrentFrame">X value of the stick input this frame.</param>
        /// <param name="context">Input callback context.</param>
        private void ProcessVerticalStickInput(float stickYValueOnCurrentFrame, InputAction.CallbackContext context)
        {
            if (stickYValueOnCurrentFrame == 0f)
            {
                if (stickYValueOnPreviousFrame < 0f)
                {
                    OnMoveDownReleased(context);
                }
                else if (stickYValueOnPreviousFrame > 0f)
                {
                    OnMoveUpReleased(context);
                }
            }
            else
            {
                if (stickYValueOnPreviousFrame == 0f)
                {
                    if (stickYValueOnCurrentFrame > 0f)
                    {
                        OnMoveUp(context);
                    }
                    else
                    {
                        OnMoveDown(context);
                    }
                }
                else
                {
                    if (Mathf.Sign(stickYValueOnCurrentFrame) != Mathf.Sign(stickXValueOnPreviousFrame))
                    {
                        if (stickYValueOnCurrentFrame > 0f)
                        {
                            OnMoveUp(context);
                        }
                        else
                        {
                            OnMoveDown(context);
                        }
                    }
                }
            }
        }

        #endregion

        #region Dash callbacks

        /// <summary>
        /// Input callback that is fired when dash button is pressed.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnDashPressed(InputAction.CallbackContext context)
        {
            if (context.phase != InputActionPhase.Started)
            {
                return;
            }

            // TO-DO: Add active device tracking so we don't have to check both keyboard and gamepad.

            // Keyboard and game pad arrows

            var rightControlValue = 0f;

            foreach (InputControl control in movementActions.MoveRight.controls)
            {
                rightControlValue = (float)control.ReadValueAsObject();

                if (rightControlValue != 0f)
                {
                    break;
                }
            }

            var leftControlValue = 0f;

            foreach (InputControl control in movementActions.MoveLeft.controls)
            {
                leftControlValue = (float)control.ReadValueAsObject();

                if (leftControlValue != 0f)
                {
                    break;
                }
            }

            var upControlValue = 0f;

            foreach (InputControl control in movementActions.MoveUp.controls)
            {
                upControlValue = (float)control.ReadValueAsObject();

                if (upControlValue != 0f)
                {
                    break;
                }
            }

            var downControlValue = 0f;

            foreach (InputControl control in movementActions.MoveDown.controls)
            {
                downControlValue = (float)control.ReadValueAsObject();

                if (downControlValue != 0f)
                {
                    break;
                }
            }

            // Gamepad stick

            if (Gamepad.current != null)
            {
                Vector2 stickValue = Gamepad.current.leftStick.ReadValue();

                bool horizontalInputIsOutsideDeadZone = Mathf.Abs(stickValue.x) > StickDeadZoneMin;

                if (rightControlValue == 0f && horizontalInputIsOutsideDeadZone && stickValue.x > 0f)
                {
                    rightControlValue = 1f;
                }

                if (leftControlValue == 0f && horizontalInputIsOutsideDeadZone && stickValue.x < 0f)
                {
                    leftControlValue = -1f;
                }

                bool verticalInputIsAboveThreshold = Mathf.Abs(stickValue.y) > 0.4f;

                if (upControlValue == 0f && verticalInputIsAboveThreshold && stickValue.y > 0f)
                {
                    upControlValue = 1f;
                }

                if (downControlValue == 0f && verticalInputIsAboveThreshold && stickValue.y < 0f)
                {
                    downControlValue = -1f;
                } 
            }

            if (rightControlValue != 0f && leftControlValue != 0f)
            {
                horizontalInput.Value = 0f;
            }
            else if (rightControlValue != 0f)
            {
                horizontalInput.Value = 1f;
            }
            else if (leftControlValue != 0f)
            {
                horizontalInput.Value = -1f;
            }

            if (downControlValue != 0f && upControlValue != 0f)
            {
                verticalInput.Value = 0f;
            }
            else if (downControlValue != 0f)
            {
                verticalInput.Value = -1f;
            }
            else if (upControlValue != 0f)
            {
                verticalInput.Value = 1f;
            }

            onDashButtonPressed.Raise();
        }

        #endregion

        #region Interaction callbacks

        /// <summary>
        /// Input callback that is fired when interact button is pressed.
        /// </summary>
        /// <param name="context">Callback context.</param>
        public void OnUseInteractable(InputAction.CallbackContext context)
        {
            if (context.phase == InputActionPhase.Started)
            {
                onUseInteractableButtonPressed.Raise();
            }            
        }

        #endregion

        #region Events

        /// <summary>
        /// Enables movement actions.
        /// </summary>
        public void EnableMovementActions() => movementActions.Enable();

        /// <summary>
        /// Disables movement actions.
        /// </summary>
        public void DisableMovementActions() => movementActions.Disable();

        /// <summary>
        /// Enables interaction actions.
        /// </summary>
        public void EnableInteractionActions() => interactionActions.Enable();

        /// <summary>
        /// Disables interaction actions.
        /// </summary>
        public void DisableInteractionActions() => interactionActions.Disable();

        /// <summary>
        /// Enables dash actions.
        /// </summary>
        public void EnableDashActions() => dashActions.Enable();

        /// <summary>
        /// Disables dash actions.
        /// </summary>
        public void DisableDashActions() => dashActions.Disable();

        #endregion
    }
}