// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Runtime/Input/GameControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace MetraReborn
{
    public class @GameControls : IInputActionCollection, IDisposable
    {
        private InputActionAsset asset;
        public @GameControls()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""GameControls"",
    ""maps"": [
        {
            ""name"": ""Movement"",
            ""id"": ""b15ae5a7-c588-45f8-a379-7cf733813fe2"",
            ""actions"": [
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""a5d3cd37-6983-4a79-863b-a39f661e4b3c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""JumpReleased"",
                    ""type"": ""Button"",
                    ""id"": ""3fb14fdd-4f20-46b2-baf1-be65380e0c27"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""MoveRight"",
                    ""type"": ""Value"",
                    ""id"": ""43565e3f-4206-418d-8a4e-a3c46e0b00b2"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""MoveRightReleased"",
                    ""type"": ""Value"",
                    ""id"": ""03a875b1-edf1-4ee3-8ccb-6c1ecba94427"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""MoveLeft"",
                    ""type"": ""Value"",
                    ""id"": ""85e47aef-4fa7-463a-82ed-908766c025b4"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""MoveLeftReleased"",
                    ""type"": ""Button"",
                    ""id"": ""554ba21b-bb6e-48b1-af90-dbdd1898c45b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""MoveUp"",
                    ""type"": ""Value"",
                    ""id"": ""4a31824c-ef03-4b0b-9590-52d44ce0759f"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""MoveUpReleased"",
                    ""type"": ""Value"",
                    ""id"": ""1ca5a14d-61f6-45db-bcfb-9f39cd32685d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""MoveDown"",
                    ""type"": ""Value"",
                    ""id"": ""da3a0e3d-1b67-40f8-98f1-6c1294a66d5e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""MoveDownReleased"",
                    ""type"": ""Value"",
                    ""id"": ""272825a2-0e53-45a2-94af-7f679d5214b5"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""LeftGamePadStick"",
                    ""type"": ""Value"",
                    ""id"": ""d3b6ac1e-8a09-4b44-ab0b-6a4fec62291d"",
                    ""expectedControlType"": ""Stick"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""dda001ff-9a3f-4a3e-8b08-c075fa0d5523"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b3f2fd4f-7c7f-4ed4-b611-143f9e065445"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""056536c9-3a1c-46c3-bc91-d42c99ed83b6"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0ff028f5-3953-4d82-a8b7-bcf8499ba0e2"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8222146c-4e62-4a85-8f6f-d3fd1d312bf8"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""20bd0fbe-32b6-4727-bed1-a59365ca15d8"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5dcc5d29-2300-45ac-9248-2d35321c07f1"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4ee4ac4a-3cf1-44fa-88c0-7eb43361b864"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""57f20955-2386-4848-a69b-d213893f19af"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6b33b9a9-4071-46f8-b118-ca1c52140859"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a2cf209a-54c6-4e25-8b95-d443474f8fa0"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8ca7d183-dc6a-41d4-92fd-812210c7b289"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b0105f92-8153-4f13-a15b-d987679c24d1"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9e27944a-ca87-4b3d-9f63-f03e8736fa08"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""869e0489-4450-4f9f-9b27-af2527090d05"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveRightReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""535b6ad9-a07f-42f4-b5be-bb0533978c77"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveRightReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""579c7e47-6da1-43aa-b25a-5da4ab8e350f"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveRightReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b8344d97-2fff-49a7-a120-4d8dd33447d4"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveLeftReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""adc2e942-6faa-4118-b695-e2caf99cd35e"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveLeftReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""66e189e2-9e07-4764-9aa4-96c2cfc92369"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveUpReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bf7ed1e7-6a95-4c41-b2e4-2929bc57b668"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveUpReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""80f11350-6f09-46c6-9506-e5ead441aea3"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveUpReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6014a6a0-c518-433a-81f5-09fd13778d6a"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveDownReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3f5ebe6e-8f4f-4efa-a3e3-c96de854bfea"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveDownReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""af7f2fc0-a637-4884-b1f5-c49ec6fa3b64"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveDownReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c81e8de1-9587-4e96-9c07-503c6ad2640f"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""JumpReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ac7de9be-c914-4cea-9f6c-dba860282956"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""JumpReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4ef87e2d-8dc5-485b-93a5-d17015ad7137"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveLeftReleased"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5274ff67-679a-46c3-a71c-6f82b0a17baf"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftGamePadStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""TestControls"",
            ""id"": ""16aac155-3a78-4999-8bc9-74f8c5256be6"",
            ""actions"": [
                {
                    ""name"": ""EnableTestControls"",
                    ""type"": ""Button"",
                    ""id"": ""a00f8099-d2e5-4db4-987b-de5885579f8e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""939a37ac-d013-40a2-bf63-45963782b9f5"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""EnableTestControls"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5251fecf-3291-4efa-8b7e-e383bdd77429"",
                    ""path"": ""<Keyboard>/rightShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""EnableTestControls"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Interactions"",
            ""id"": ""f2359c6d-ab63-4d84-b0ec-d130d0a3b7fe"",
            ""actions"": [
                {
                    ""name"": ""UseInteractable"",
                    ""type"": ""Button"",
                    ""id"": ""3b6884d5-fe3d-4be0-a309-75f851c56f93"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""8172de6f-2072-4061-8401-7d22a5ca8760"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UseInteractable"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""40a15ceb-f796-47dd-ad60-48a889afe9ee"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UseInteractable"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Dash"",
            ""id"": ""2d0fa80a-663b-47fc-9ff3-73e41e4db5d1"",
            ""actions"": [
                {
                    ""name"": ""DashPressed"",
                    ""type"": ""Button"",
                    ""id"": ""3d7ac258-f2cf-420c-89db-374bfd01f126"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""c6ae46ce-b975-4cbf-b8e3-e0417748ed4a"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DashPressed"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e5ebeed9-2159-4299-b2dd-9c4b00e22b54"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DashPressed"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Movement
            m_Movement = asset.FindActionMap("Movement", throwIfNotFound: true);
            m_Movement_Jump = m_Movement.FindAction("Jump", throwIfNotFound: true);
            m_Movement_JumpReleased = m_Movement.FindAction("JumpReleased", throwIfNotFound: true);
            m_Movement_MoveRight = m_Movement.FindAction("MoveRight", throwIfNotFound: true);
            m_Movement_MoveRightReleased = m_Movement.FindAction("MoveRightReleased", throwIfNotFound: true);
            m_Movement_MoveLeft = m_Movement.FindAction("MoveLeft", throwIfNotFound: true);
            m_Movement_MoveLeftReleased = m_Movement.FindAction("MoveLeftReleased", throwIfNotFound: true);
            m_Movement_MoveUp = m_Movement.FindAction("MoveUp", throwIfNotFound: true);
            m_Movement_MoveUpReleased = m_Movement.FindAction("MoveUpReleased", throwIfNotFound: true);
            m_Movement_MoveDown = m_Movement.FindAction("MoveDown", throwIfNotFound: true);
            m_Movement_MoveDownReleased = m_Movement.FindAction("MoveDownReleased", throwIfNotFound: true);
            m_Movement_LeftGamePadStick = m_Movement.FindAction("LeftGamePadStick", throwIfNotFound: true);
            // TestControls
            m_TestControls = asset.FindActionMap("TestControls", throwIfNotFound: true);
            m_TestControls_EnableTestControls = m_TestControls.FindAction("EnableTestControls", throwIfNotFound: true);
            // Interactions
            m_Interactions = asset.FindActionMap("Interactions", throwIfNotFound: true);
            m_Interactions_UseInteractable = m_Interactions.FindAction("UseInteractable", throwIfNotFound: true);
            // Dash
            m_Dash = asset.FindActionMap("Dash", throwIfNotFound: true);
            m_Dash_DashPressed = m_Dash.FindAction("DashPressed", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Movement
        private readonly InputActionMap m_Movement;
        private IMovementActions m_MovementActionsCallbackInterface;
        private readonly InputAction m_Movement_Jump;
        private readonly InputAction m_Movement_JumpReleased;
        private readonly InputAction m_Movement_MoveRight;
        private readonly InputAction m_Movement_MoveRightReleased;
        private readonly InputAction m_Movement_MoveLeft;
        private readonly InputAction m_Movement_MoveLeftReleased;
        private readonly InputAction m_Movement_MoveUp;
        private readonly InputAction m_Movement_MoveUpReleased;
        private readonly InputAction m_Movement_MoveDown;
        private readonly InputAction m_Movement_MoveDownReleased;
        private readonly InputAction m_Movement_LeftGamePadStick;
        public struct MovementActions
        {
            private @GameControls m_Wrapper;
            public MovementActions(@GameControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @Jump => m_Wrapper.m_Movement_Jump;
            public InputAction @JumpReleased => m_Wrapper.m_Movement_JumpReleased;
            public InputAction @MoveRight => m_Wrapper.m_Movement_MoveRight;
            public InputAction @MoveRightReleased => m_Wrapper.m_Movement_MoveRightReleased;
            public InputAction @MoveLeft => m_Wrapper.m_Movement_MoveLeft;
            public InputAction @MoveLeftReleased => m_Wrapper.m_Movement_MoveLeftReleased;
            public InputAction @MoveUp => m_Wrapper.m_Movement_MoveUp;
            public InputAction @MoveUpReleased => m_Wrapper.m_Movement_MoveUpReleased;
            public InputAction @MoveDown => m_Wrapper.m_Movement_MoveDown;
            public InputAction @MoveDownReleased => m_Wrapper.m_Movement_MoveDownReleased;
            public InputAction @LeftGamePadStick => m_Wrapper.m_Movement_LeftGamePadStick;
            public InputActionMap Get() { return m_Wrapper.m_Movement; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(MovementActions set) { return set.Get(); }
            public void SetCallbacks(IMovementActions instance)
            {
                if (m_Wrapper.m_MovementActionsCallbackInterface != null)
                {
                    @Jump.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnJump;
                    @Jump.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnJump;
                    @Jump.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnJump;
                    @JumpReleased.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnJumpReleased;
                    @JumpReleased.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnJumpReleased;
                    @JumpReleased.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnJumpReleased;
                    @MoveRight.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveRight;
                    @MoveRight.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveRight;
                    @MoveRight.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveRight;
                    @MoveRightReleased.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveRightReleased;
                    @MoveRightReleased.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveRightReleased;
                    @MoveRightReleased.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveRightReleased;
                    @MoveLeft.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveLeft;
                    @MoveLeft.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveLeft;
                    @MoveLeft.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveLeft;
                    @MoveLeftReleased.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveLeftReleased;
                    @MoveLeftReleased.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveLeftReleased;
                    @MoveLeftReleased.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveLeftReleased;
                    @MoveUp.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveUp;
                    @MoveUp.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveUp;
                    @MoveUp.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveUp;
                    @MoveUpReleased.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveUpReleased;
                    @MoveUpReleased.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveUpReleased;
                    @MoveUpReleased.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveUpReleased;
                    @MoveDown.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveDown;
                    @MoveDown.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveDown;
                    @MoveDown.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveDown;
                    @MoveDownReleased.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveDownReleased;
                    @MoveDownReleased.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveDownReleased;
                    @MoveDownReleased.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMoveDownReleased;
                    @LeftGamePadStick.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnLeftGamePadStick;
                    @LeftGamePadStick.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnLeftGamePadStick;
                    @LeftGamePadStick.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnLeftGamePadStick;
                }
                m_Wrapper.m_MovementActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Jump.started += instance.OnJump;
                    @Jump.performed += instance.OnJump;
                    @Jump.canceled += instance.OnJump;
                    @JumpReleased.started += instance.OnJumpReleased;
                    @JumpReleased.performed += instance.OnJumpReleased;
                    @JumpReleased.canceled += instance.OnJumpReleased;
                    @MoveRight.started += instance.OnMoveRight;
                    @MoveRight.performed += instance.OnMoveRight;
                    @MoveRight.canceled += instance.OnMoveRight;
                    @MoveRightReleased.started += instance.OnMoveRightReleased;
                    @MoveRightReleased.performed += instance.OnMoveRightReleased;
                    @MoveRightReleased.canceled += instance.OnMoveRightReleased;
                    @MoveLeft.started += instance.OnMoveLeft;
                    @MoveLeft.performed += instance.OnMoveLeft;
                    @MoveLeft.canceled += instance.OnMoveLeft;
                    @MoveLeftReleased.started += instance.OnMoveLeftReleased;
                    @MoveLeftReleased.performed += instance.OnMoveLeftReleased;
                    @MoveLeftReleased.canceled += instance.OnMoveLeftReleased;
                    @MoveUp.started += instance.OnMoveUp;
                    @MoveUp.performed += instance.OnMoveUp;
                    @MoveUp.canceled += instance.OnMoveUp;
                    @MoveUpReleased.started += instance.OnMoveUpReleased;
                    @MoveUpReleased.performed += instance.OnMoveUpReleased;
                    @MoveUpReleased.canceled += instance.OnMoveUpReleased;
                    @MoveDown.started += instance.OnMoveDown;
                    @MoveDown.performed += instance.OnMoveDown;
                    @MoveDown.canceled += instance.OnMoveDown;
                    @MoveDownReleased.started += instance.OnMoveDownReleased;
                    @MoveDownReleased.performed += instance.OnMoveDownReleased;
                    @MoveDownReleased.canceled += instance.OnMoveDownReleased;
                    @LeftGamePadStick.started += instance.OnLeftGamePadStick;
                    @LeftGamePadStick.performed += instance.OnLeftGamePadStick;
                    @LeftGamePadStick.canceled += instance.OnLeftGamePadStick;
                }
            }
        }
        public MovementActions @Movement => new MovementActions(this);

        // TestControls
        private readonly InputActionMap m_TestControls;
        private ITestControlsActions m_TestControlsActionsCallbackInterface;
        private readonly InputAction m_TestControls_EnableTestControls;
        public struct TestControlsActions
        {
            private @GameControls m_Wrapper;
            public TestControlsActions(@GameControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @EnableTestControls => m_Wrapper.m_TestControls_EnableTestControls;
            public InputActionMap Get() { return m_Wrapper.m_TestControls; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(TestControlsActions set) { return set.Get(); }
            public void SetCallbacks(ITestControlsActions instance)
            {
                if (m_Wrapper.m_TestControlsActionsCallbackInterface != null)
                {
                    @EnableTestControls.started -= m_Wrapper.m_TestControlsActionsCallbackInterface.OnEnableTestControls;
                    @EnableTestControls.performed -= m_Wrapper.m_TestControlsActionsCallbackInterface.OnEnableTestControls;
                    @EnableTestControls.canceled -= m_Wrapper.m_TestControlsActionsCallbackInterface.OnEnableTestControls;
                }
                m_Wrapper.m_TestControlsActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @EnableTestControls.started += instance.OnEnableTestControls;
                    @EnableTestControls.performed += instance.OnEnableTestControls;
                    @EnableTestControls.canceled += instance.OnEnableTestControls;
                }
            }
        }
        public TestControlsActions @TestControls => new TestControlsActions(this);

        // Interactions
        private readonly InputActionMap m_Interactions;
        private IInteractionsActions m_InteractionsActionsCallbackInterface;
        private readonly InputAction m_Interactions_UseInteractable;
        public struct InteractionsActions
        {
            private @GameControls m_Wrapper;
            public InteractionsActions(@GameControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @UseInteractable => m_Wrapper.m_Interactions_UseInteractable;
            public InputActionMap Get() { return m_Wrapper.m_Interactions; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(InteractionsActions set) { return set.Get(); }
            public void SetCallbacks(IInteractionsActions instance)
            {
                if (m_Wrapper.m_InteractionsActionsCallbackInterface != null)
                {
                    @UseInteractable.started -= m_Wrapper.m_InteractionsActionsCallbackInterface.OnUseInteractable;
                    @UseInteractable.performed -= m_Wrapper.m_InteractionsActionsCallbackInterface.OnUseInteractable;
                    @UseInteractable.canceled -= m_Wrapper.m_InteractionsActionsCallbackInterface.OnUseInteractable;
                }
                m_Wrapper.m_InteractionsActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @UseInteractable.started += instance.OnUseInteractable;
                    @UseInteractable.performed += instance.OnUseInteractable;
                    @UseInteractable.canceled += instance.OnUseInteractable;
                }
            }
        }
        public InteractionsActions @Interactions => new InteractionsActions(this);

        // Dash
        private readonly InputActionMap m_Dash;
        private IDashActions m_DashActionsCallbackInterface;
        private readonly InputAction m_Dash_DashPressed;
        public struct DashActions
        {
            private @GameControls m_Wrapper;
            public DashActions(@GameControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @DashPressed => m_Wrapper.m_Dash_DashPressed;
            public InputActionMap Get() { return m_Wrapper.m_Dash; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(DashActions set) { return set.Get(); }
            public void SetCallbacks(IDashActions instance)
            {
                if (m_Wrapper.m_DashActionsCallbackInterface != null)
                {
                    @DashPressed.started -= m_Wrapper.m_DashActionsCallbackInterface.OnDashPressed;
                    @DashPressed.performed -= m_Wrapper.m_DashActionsCallbackInterface.OnDashPressed;
                    @DashPressed.canceled -= m_Wrapper.m_DashActionsCallbackInterface.OnDashPressed;
                }
                m_Wrapper.m_DashActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @DashPressed.started += instance.OnDashPressed;
                    @DashPressed.performed += instance.OnDashPressed;
                    @DashPressed.canceled += instance.OnDashPressed;
                }
            }
        }
        public DashActions @Dash => new DashActions(this);
        public interface IMovementActions
        {
            void OnJump(InputAction.CallbackContext context);
            void OnJumpReleased(InputAction.CallbackContext context);
            void OnMoveRight(InputAction.CallbackContext context);
            void OnMoveRightReleased(InputAction.CallbackContext context);
            void OnMoveLeft(InputAction.CallbackContext context);
            void OnMoveLeftReleased(InputAction.CallbackContext context);
            void OnMoveUp(InputAction.CallbackContext context);
            void OnMoveUpReleased(InputAction.CallbackContext context);
            void OnMoveDown(InputAction.CallbackContext context);
            void OnMoveDownReleased(InputAction.CallbackContext context);
            void OnLeftGamePadStick(InputAction.CallbackContext context);
        }
        public interface ITestControlsActions
        {
            void OnEnableTestControls(InputAction.CallbackContext context);
        }
        public interface IInteractionsActions
        {
            void OnUseInteractable(InputAction.CallbackContext context);
        }
        public interface IDashActions
        {
            void OnDashPressed(InputAction.CallbackContext context);
        }
    }
}
