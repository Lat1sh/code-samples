This repositiry contains various code samples from my projects.

1. Controllers - contains a character controller from a 2D platform game.

2. Custom Inspector (New UI) - custom inspector made with the use of Unity's new UI system, UI Elements.

3. Events - game events and event listeners, a modified version of scriptable object events from Ryan Hipple's Unite talk.

4. Input Manager - input manager using Unity's new input system.

5. Monetization - scripts handling Google Play Store and Google Mobile Ads in a mobile project.

6. Object pooling - example of an object pooling system using generics.

7. Pathfinding - pathfinder based on A* algorithm as explained in Sebastian Lague's tutorials.

Note that samples consist of pieces of code plucked from actual projects and most of them won't compile due to missin parts.